import Geography from '@/components/cells/data/geography'
import Culture from '@/components/cells/data/culture'
import History from '@/components/cells/data/history'
import Cell from '@/components/cells/cell'
export default {
  name: 'World',
  extends: Cell,
  computed: {
    tabs: function() {
      return [
        {
          id: 0,
          title: 'Geography',
          component: Geography,
          props: {
            cell: this.cell
          }
        },
        {
          id: 1,
          title: 'Demographics',
          component: Culture,
          props: {
            cell: this.cell,
            text: this.culture
          }
        },
        {
          id: 2,
          title: 'History',
          component: History,
          props: {
            cell: this.cell
          }
        }
      ]
    },
    culture: function() {
      const dist = {
        mages: this.cell.cultures.filter(s => s.spec.mage > 2).length,
        dark: this.cell.cultures.filter(s => s.spec.dark > 2).length,
        faith: this.cell.cultures.filter(s => s.spec.faith > 2).length,
        void: this.cell.cultures.filter(s => s.spec.mage < 1).length,
        hybrids: this.cell.cultures.filter(s => s.spec.hybrids > 0).length
      }
      const cls = this.cell.cultures.reduce((sum, c) => {
        c.classes.forEach(cls => {
          !sum[cls] && (sum[cls] = 0)
          sum[cls] += 1
        })
        return sum
      }, {})
      const cultures = this.cell.cultures
      const styles = {
        Common: cultures.filter(c => c.common).length / cultures.length,
        Exotic: cultures.filter(c => c.exotic).length / cultures.length,
        Gutteral: cultures.filter(c => c.gutteral).length / cultures.length,
        Melodic: cultures.filter(c => c.melodic).length / cultures.length
      }
      return [
        `Population: ${this.cell.population.toLocaleString('en-US')}`,
        `Cultures: ${Object.entries(styles).map(([k, v]) => `${k} [${v.toFixed(2)}]`).join(', ')}`,
        `Statistics: ${Object.entries(dist).map(([k, v]) => `${k} [${v}]`).join(', ')}`,
        `Classes: ${Object.entries(cls).map(([k, v]) => `${k} [${v}]`).sort().join(', ')}`
      ]
    }
  }
}
