import Entities from '@/components/cells/data/Entities'
import Geography from '@/components/cells/data/geography'
import Culture from '@/components/cells/data/culture'
import Cell from '@/components/cells/cell'
import { mapValue } from '@/model/utilities/common'
export default {
  name: 'Location',
  extends: Cell,
  computed: {
    tabs: function() {
      return [
        {
          id: 0,
          title: 'Geography',
          component: Geography,
          props: {
            cell: this.cell,
            text: this.geography
          }
        },
        {
          id: 1,
          title: 'Demographics',
          component: Culture,
          props: {
            cell: this.cell,
            text: this.culture
          }
        },
        {
          id: 2,
          title: 'Entities',
          component: Entities,
          props: {
            cell: this.cell
          }
        }
      ]
    },
    culture: function() {
      const city = this.cell
      return [
        `Majority: ${Object.entries(city.distributions.majority)
          .sort((a, b) => b[1] - a[1])
          .map(
            ([culture, percent]) =>
              `${city.cultures[culture].name} (${percent.toFixed(2)}%)`
          )
          .join(', ')}`,
        this.cell.region.conquered ? `Foreign Presence: ${this.cell.foreign_presence.toFixed(2)}%` : false,
        `Urban Population: ${this.cell.urban.toLocaleString('en-US')}`,
        `Rural Population: ${this.cell.rural.toLocaleString('en-US')}`
      ].filter(s => s)
    },
    geography: function() {
      const city = this.cell
      const { latitude } = window.world.gps(...this.cell.loc)
      const localtime = window.world.localtime(this.cell)
      const hours = mapValue([0, 360], [0, 23], window.world.hourAngle(latitude))
      const hour = Math.floor(hours)
      const minutes = (hours % 1) * 60
      const minute = Math.floor(minutes)
      const seconds = (minutes % 1) * 60
      const second = Math.floor(seconds)
      const sunrise = new Date(localtime)
      sunrise.setHours(11 - hour)
      sunrise.setMinutes(59 - minute)
      sunrise.setSeconds(59 - second)
      const sunset = new Date(localtime)
      sunset.setHours(12 + hour)
      sunset.setMinutes(minute)
      sunset.setSeconds(second)
      const daytime = window.world.dayTime(latitude)
      const eternal = daytime >= 24 || daytime <= 0 ? '∞' : false
      const features = window.world.features
      const water = []
      if (city.water.still.length + city.water.river.length > 0) {
        water.push(
          `Water Sources${city.port ? ' (Port)' : ''}: ${city.water.still
            .map(p => features.landmarks[p])
            .concat(city.water.river.map(p => features.rivers[p]))
            .join(', ')}`
        )
      }
      const seaTrade = []
      if (city.trade.sea.length > 0) {
        seaTrade.push(
          `Sea Trade: ${city.trade.sea
            .map(idx => `${city.locations[idx].name}[${city.paths.sea[idx]}]`)
            .sort()
            .join(', ')}`
        )
      }
      const landTrade = []
      if (city.trade.land.length > 0) {
        landTrade.push(
          `Land Trade: ${city.trade.land
            .map(idx => `${city.locations[idx].name}[${city.paths.land[idx]}]`)
            .sort()
            .join(', ')}`
        )
      }
      return [
        ...landTrade,
        ...seaTrade,
        // `Economy (Imports: ${city.tradeWeight.toFixed(2)}): ${Object.entries(city.distributions.trade)
        //   .sort((a, b) => b[1] - a[1])
        //   .map(([industry, percent]) => `${industry} (${percent.toFixed(2)}%)`)
        //   .join(', ')}`,
        // `Economy (Exports): ${Object.entries(city.distributions.economy)
        //   .sort((a, b) => b[1] - a[1])
        //   .map(([industry, percent]) => `${industry} (${percent.toFixed(2)}%)`)
        //   .join(', ')}`,
        `Prices: ${Object.entries(city.priceMods)
          .sort((a, b) => b[1] - a[1])
          .map(([industry, percent]) => `${industry} (${percent.toFixed(2)}%)`)
          .join(', ')}`,
        ...water,
        `Local Time: ${localtime.toLocaleString()}`,
        `Length of Day: ${daytime.toFixed(2)} hours`,
        `Sunrise: ${eternal || sunrise.toLocaleTimeString()} | Sunset: ${eternal || sunset.toLocaleTimeString()}`
      ]
    }
  }
}
