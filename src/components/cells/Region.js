import Geography from '@/components/cells/data/geography'
import Culture from '@/components/cells/data/culture'
// import Politics from '@/components/cells/data/politics'
// import Power from '@/components/cells/data/power'
// import History from '@/components/cells/data/history'
import Cell from '@/components/cells/cell'
export default {
  name: 'Region',
  extends: Cell,
  computed: {
    tabs: function() {
      return [
        {
          id: 0,
          title: 'Geography',
          component: Geography,
          props: {
            cell: this.cell,
            text: this.geography
          }
        },
        {
          id: 1,
          title: 'Demographics',
          component: Culture,
          props: {
            cell: this.cell,
            text: this.culture
          }
        }
        // {
        //   title: 'Politics',
        //   component: Politics,
        //   props: {
        //     cell: this.cell
        //   }
        // },
        // {
        //   title: 'Power',
        //   component: Power,
        //   props: {
        //     cell: this.cell
        //   }
        // },
        // {
        //   title: 'History',
        //   component: History,
        //   props: {
        //     cell: this.cell
        //   }
        // }
      ]
    },
    culture: function() {
      return [
        `Population: ${this.cell.population.toLocaleString('en-US')}`,
        this.cell.colony ? `Colonial Presence: ${this.cell.colony.name}` : false
      ].filter(s => s)
    },
    geography: function() {
      return [
        `Average Temperature: ${this.cell.heat.toFixed(2)} (${this.cell.climate})`,
        `Ocean: ${this.cell.ocean}`,
        `Borders: ${this.cell.neighbors
          .map(region => region.name)
          .sort()
          .join(', ')}`
      ]
    }
  }
}
