import { Bubble, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default Bubble.extend({
  mixins: [reactiveProp],
  props: ['options', 'host'],
  mounted () {
    this.renderChart(this.chartData, {...this.options, ...this.defaults})
  },
  computed: {
    defaults: function() {
      return {
        legend: {
          display: false,
          labels: {
            fontFamily: 'Josefin Sans'
        }
        },
        scales: {
          fontFamily: 'Josefin Sans'
        },
        layout: {
          padding: {
            bottom: 10,
            top: 10
          }
        },
        title: {
          text: 'Nation-State Power: Military vs Economy vs Population',
          fontFamily: 'Josefin Sans',
          fontSize: 16,
          display: true
        },
        tooltips: {
          callbacks: {
            label: this.tooltipLabels
          }
        }
      }
    }
  },
  methods: {
    tooltipLabels(tooltipItems, data) {
      const label = data.datasets[tooltipItems.datasetIndex].label
      const x = data.datasets[tooltipItems.datasetIndex].data[0].x.toFixed(2)
      const y = data.datasets[tooltipItems.datasetIndex].data[0].y.toFixed(2)
      const r = data.datasets[tooltipItems.datasetIndex].data[0].i
      return `${label}: (${x}, ${y}, ${r})`
    }
  }
})
