import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default Line.extend({
  mixins: [reactiveProp],
  props: ['options', 'x', 'y'],
  mounted () {
    // this.chartData is created in the mixin
    this.renderChart(this.chartData, {...this.options, ...this.axes})
  },
  computed: {
    axes: function() {
      return {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: this.y
              }
            }
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: this.x
              }
            }
          ]
        },
        layout: {
          padding: {
            bottom: 10
          }
        }
      }
    }
  }
})
