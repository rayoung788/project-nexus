import { Radar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default Radar.extend({
  mixins: [reactiveProp],
  props: ['options', 'host'],
  mounted() {
    this.renderChart(this.chartData, { ...this.options, ...this.defaults })
  },
  computed: {
    defaults: function() {
      return {
        legend: {
          display: true,
          onClick: e => e.stopPropagation(),
          labels: {
            fontFamily: 'Josefin Sans',
            generateLabels: chart => {
              return chart.data.datasets.map(({ name, backgroundColor, borderColor }) => {
                return {
                  text: name,
                  fillStyle: backgroundColor,
                  strokeStyle: borderColor
                }
              })
            }
          }
        },
        scales: {
          fontFamily: 'Josefin Sans'
        },
        layout: {
          padding: 5
        },
        tooltips: {
          fontFamily: 'Josefin Sans',
          callbacks: {
            label: this.tooltipLabels
          }
        }
      }
    }
  },
  methods: {
    tooltipLabels(tooltipItems, data) {
      const {index, datasetIndex} = tooltipItems
      const item = data.datasets[datasetIndex]
      return `${item.name}: ${item.data[index].toFixed(2)}`
    }
  }
})
