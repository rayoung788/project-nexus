import { Pie, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default Pie.extend({
  mixins: [reactiveProp],
  props: ['options'],
  data() {
    return {
      tooltips: {
        tooltips: {
          callbacks: {
            label: this.tooltipLabels
          }
        }
      }
    }
  },
  mounted () {
    this.renderChart(this.chartData, {...this.options, ...this.tooltips})
  },
  methods: {
    tooltipLabels(tooltipItems, data) {
      let label = data.labels[tooltipItems.index]
      let value = data.datasets[0].data[tooltipItems.index]
      return `${label}: ${value}%`
    }
  }
})
