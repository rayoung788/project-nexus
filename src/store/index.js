import Vue from 'vue'
import Vuex from 'vuex'
import Unit from '@/model/entities/unit'

Vue.use(Vuex)

// initial state
const state = {
  avatar: {},
  heroes: {},
  loc: -1,
  target: { x: 0, y: 0 },
  time: {},
  id: ''
}

// mutations
const mutations = {
  shaper(state, id) {
    state.id = id
  },
  clearAvatar(state) {
    state.avatar = {}
    state.loc = -1
    state.time = {}
  },
  load(state, { avatar, loc, time, id }) {
    state.id = id
    state.avatar = avatar
    state.loc = loc
    state.time = time
  },
  setAvatar(state, { avatar, loc }) {
    state.avatar = window.world.npcgen[avatar]
    state.loc = loc
    state.time = window.world.date
    state.heroes = new Unit(state.avatar)
  },
  travel(state, { elapsed, loc }) {
    state.loc = loc
    state.time.setHours(state.time.getHours() + elapsed.hours)
    state.time.setMinutes(state.time.getMinutes() + elapsed.minutes)
    window.world.date = new Date(state.time.toString())
    state.time = window.world.date
  },
  setTarget(state, loc) {
    state.target = loc
  }
}

export default new Vuex.Store({
  state,
  mutations
})
