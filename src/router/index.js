import Vue from 'vue'
import Router from 'vue-router'
import Nexus from '@/components/Nexus'
import CityMaps from '@/components/global/CityMaps'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Nexus',
      component: Nexus
    },
    {
      path: '/city',
      name: 'CityMaps',
      component: CityMaps
    }
  ]
})
