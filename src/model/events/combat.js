class CombatEvent {
  constructor(units) {
    this.log = []
    this.idx = 0
    this.units = units
    this.units.forEach(unit => {
      unit.npcs.forEach(npc => {
        npc.allies = unit
        npc.enemies = this.units.filter(other => other !== unit)
      })
    })
    this.actors = units.reduce((group, unit) => {
      return [...group, ...unit.npcs]
    }, [])
  }

  get complete() {
    return (
      this.units.reduce((total, unit) => {
        return total + (unit.health > 0 ? 1 : 0)
      }, 0) < 2
    )
  }

  write(entry) {
    entry.name = `${this.idx}# ${entry.name}`
    this.log.unshift(entry)
    this.idx += 1
  }

  tick() {
    console.log(`TURN SEQUENCE STARTING`)
    if (!this.complete) {
      this.actors.sort((a, b) => b.initiative - a.initiative).forEach(actor => {
        if (actor.health > 0) {
          console.log(`===== IDX # ${this.idx} =====`)
          this.write(actor.ai())
        }
      })
    } else {
      this.cleanup()
    }
    console.log(`TURN SEQUENCE ENDING`)
  }

  cleanup() {
    this.units.forEach(unit => {
      unit.npcs.forEach(npc => {
        npc.allies = []
        npc.enemies = []
      })
    })
  }
}

export default CombatEvent
