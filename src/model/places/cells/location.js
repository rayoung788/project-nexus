import Cell from '@/model/places/cell'
import { distance } from '@/model/utilities/common'
// Location context; each region can have zero or more locations
class Location extends Cell {
  constructor(ref) {
    super(ref)
    // type of location (captial, city, town, etc.)
    this.type = 'location'
    // locations are by default not populated
    this.populated = false
    // used to index this.locations
    this.idx = this.locations.length
    // add this location to the locations list
    this.locations.push(this)
  }
  // get's the name of this location's region
  get regionName() {
    return this.region.name
  }
  // defaults to reference's region
  get region() {
    return this.ref.region
  }
  // defaults to reference's city
  get city() {
    return this.ref.city
  }
  // location on the map (x, y)
  get loc() {
    return this.ref.loc
  }
  // descriptive information
  get description() {
    return this.name
  }
  // computes the distance from this location to some other location
  distanceTo(loc) {
    return distance(this.loc, loc.loc, [this.dim.sh, this.dim.sw])
  }

  populate() {}
  // populates the location if applicable
  spawn() {
    if (!this.populated) {
      this.populate()
      this.populated = true
    }
  }
}

export default Location
