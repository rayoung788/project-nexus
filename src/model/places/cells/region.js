import Cell from '@/model/places/cell'
import { settlements } from '@/model/utilities/constants'

// Regional context
class Region extends Cell {
  constructor(ref, idx, capital, borders) {
    super(ref)
    if (ref) {
      // region index (used to index this.regions)
      this.idx = idx
      // bordering region indexes
      this.borders = borders || {}
      // base culture
      this._culture = -1
      // regional captial city index
      this._capital = capital
      // cached regional land cells
      this.land = this.ref.land.filter(poly => poly.region === this.idx)
      // cached regional water cells
      this.water = this.ref.water.filter(poly => poly.region === this.idx)
      // regional ocean cells ... better hope the ocean is #1
      this.ocean = this.water.filter(p => p.landmark === 1).length // + this.land.filter(p => p.ocean).length
      // relations
      this.similarity = {}
      this.relations = {}
      // history
      this.history = []
      // contacts
      this.contacts = []
      // conquered status
      this._conquered = false
      // vassal states
      this.vassals = []
      // hazard state
      this.hazard = 0
      // colony state
      this._colony = false
    }
  }
  get total_population() {
    return (
      this.vassals
        .map(v => this.regions[v].population)
        .reduce((sum, p) => sum + p, 0) + this.population
    )
  }
  // ruling agent (applies to vassal states)
  get agent() {
    return this.conquered ? this.regions[this.conquered] : this
  }
  // is this part of a complex political unit?
  get complex_unit() {
    return this.conquered || this.vassals.length > 0 || this.divided
  }
  get colony() {
    return this.agent._colony
  }
  // conquered status
  get conquered() {
    return this._conquered
  }
  // conquered status
  set conquered(value) {
    if (this._conquered) {
      // dismiss the old ruler
      const former = this.regions[this._conquered]
      former.vassals = former.vassals.filter(v => v !== this.idx)
      this.capital.type = settlements.capital
    }
    this._conquered = value
    if (value) {
      // assign the new ruler
      const ruler = this.regions[value]
      ruler.vassals.push(this.idx)
      this.vassals.forEach(v => {
        this.regions[v].conquered = value
      })
      this.capital.type = settlements.city
    }
  }
  // ruling culture
  get culture() {
    return this.cultures[this._culture]
  }
  // regional titles
  get titles() {
    return this.culture.titles
  }
  // ruling culture
  set culture(value) {
    if (this._culture !== value.idx) {
      this._culture = value.idx
      this.distributions.culture[this._culture] = 100
      this.distributions.origin[this._culture] = 100
      this.distributions.majority[this._culture] = 100
      this.name = this.namePlace('region')
      this._name = this.name
      this.cultures[this._culture].regions += 1
    }
  }
  // display border color
  get color() {
    return this.culture.display
  }
  // regional captial city
  get capital() {
    return this.locations[this._capital]
  }
  // bordering regions
  get neighbors() {
    const territories = [this.idx, ...this.vassals]
    return Array.from(
      new Set(
        territories.reduce(
          (sum, v) =>
            sum.concat(
              Object.keys(this.regions[v].borders).map(
                b => this.regions[b].agent.idx
              )
            ),
          []
        )
      )
    )
      .map(b => this.regions[b])
      .filter(b => !territories.includes(b.idx) && !b.conquered)
  }
  // gets all locations inside of this region
  get territories() {
    const territory = [this.idx, ...this.vassals]
    return this.settlements.filter(loc => territory.includes(loc.region.idx))
  }
  // gets the base regional culture distribution
  get normal() {
    return this.cultureDist
  }
  // gets the minority regional culture distribution
  get minority() {
    return this.distributions.minority
  }
  // gets the majority regional culture distribution
  get majority() {
    return this.distributions.majority
  }
  // gets the origin regional culture distribution
  get origin() {
    return this.distributions.origin
  }
  relation(idx) {
    return this.relations[idx] + this.similarity[idx]
  }
  recordEvent(event) {
    this.history.push(event)
    this.ref.history.push(event)
  }
  // total population & wealth (scored with vassals)
  get subjects() {
    return (
      this.population +
      this.vassals
        .map(v => this.regions[v])
        .reduce((sum, r) => sum + r.population, 0)
    )
  }
  get wealth() {
    return (
      this.value +
      this.vassals.map(v => this.regions[v]).reduce((sum, r) => sum + r.value, 0)
    )
  }
  // ruling culture-based traits
  get traits() {
    return this.culture.traits
  }
  get advantage() {
    return this.culture.advantage
  }
  // economic strength
  get economy() {
    const innovation = (this.traits.technology + this.traits.magic) / 4
    return this.wealth * (this.advantage.economics + innovation)
  }
  // military strength
  get military() {
    const demographics = this.population / 1000000
    const magic = 1 + this.traits.magic
    const tech = 1 + this.traits.technology
    return (demographics * magic + this.wealth * tech) * this.traits.aggression
  }

  get space() {
    const land =
      this.vassals
        .map(v => this.regions[v].land.length)
        .reduce((sum, p) => sum + p, 0) + this.land.length
    return land / this.world.land.length
  }

  get primary_biome() {
    return Object.entries(this.biomeDist).reduce((max, [biome, p]) => max[1] < p ? [biome, p] : max, ['', 0])
  }
}

export default Region
