import Cell from '@/model/places/cell'
import Star from '@/model/places/star'
import { Dice } from '@/model/utilities/random'
import biomes from '@/model/places/biomes.json'
import * as d3 from 'd3'
import NPCRegistry from '../../entities/registry'
import { degrees, radians, mapValue } from '../../utilities/common'
// World context (voronoi style)
class World extends Cell {
  constructor(seed, highres = 1) {
    super()
    // id used for save file names + seeds
    this.id = seed
    // deterministic fate
    this.dice = new Dice(this.id)
    // high resolution flag
    this.highres = highres
    // voronoi diagram
    this.diagram = {}
    // voronoi polygons
    this._polygons = {}
    // voronoi initial points
    this.sites = []
    // sea-level cutoff height
    this.coast = 0.1
    // mountain cutoff height
    this.mountains = 0.5
    // number of regions
    this.nRegions = 12 // 32
    this.nTowns = [300, 350] // 500, 550
    // river index offset (used for river ids)
    this.rivers = 0
    // feature object used to keep track of land / sea landmarks and their names
    this._features = {
      landmarks: {},
      rivers: { '-1': 'none' },
      mountains: { '-1': 'none' }
    }
    // locations list
    this._locations = []
    // region list
    this._regions = {}
    // culture list
    this._cultures = []
    // biome mapping
    this._biomes = biomes
    // unique places list
    this._places = new Set()
    // road lists
    this.routes = {
      main: [],
      small: [],
      sea: []
    }
    // display information used to render the map
    this.display = {
      rivers: [],
      routes: {
        main: [],
        small: [],
        ocean: []
      }
    }
    // npc generation factory instantiation
    this._npcgen = new NPCRegistry()
    // map dimensions
    const res = 800
    this._dim = {
      // display image height (pixels)
      h: res,
      // display image width (pixels)
      w: res,
      // voronoi cell resolution
      cells: 16000 * highres,
      // actual height (miles)
      rh: 3000,
      // actual width (miles)
      rw: 3000,
      // noise resolution
      res: 256
    }
    // city & region spacing
    this.spacing = {
      // minimum space between capital cities
      regions: 0.04,
      // minimum space between regular cities & towns
      towns: 0.00675,
      // radius for regional trade
      main: 0.2,
      // radius for local trade
      small: 0.03,
      // radius for sea trade
      ocean: 0.125
    }
    // this.dr = window.dice.norm(0, 0.1) // global precipitation
    this._population = 0
    // planet info
    this.star = new Star({ earth: true })
    this.rotation = 24 // daily period (hours)
    this.tilt = 23.5 // axial tilt (degrees)
    // temperature gradiant
    const dt = 0
    this.climate_extremes = {
      w: -60 + dt,  // polar winter
      s: -10 + dt, // polar summer
      e: 30 + dt // equatorial
    }
    const northern_hemisphere = window.dice.random > 0.5
    this.bound = {
      lat: northern_hemisphere ? [0, 80] : [-80, 0],
      long: [0, 80]
    }
    this.temp = this.temp_compute()
    // start date
    const year = Math.round(window.dice.norm(1300, 150))
    const month = window.dice.randint(0, 11)
    const day = window.dice.randint(0, 28)
    const hours = window.dice.randint(0, 23)
    const minutes = window.dice.randint(0, 59)
    this._date = new Date(year, month, day, hours, minutes)
    // historical events
    this.history = []
  }
  get dice() {
    return this._dice
  }
  set dice(value) {
    this._dice = value
    window.dice = value
  }
  // sets variables to be referenced by child cells
  get date() {
    return this._date
  }
  set date(value) {
    this._date = value
  }
  get dim() {
    return this._dim
  }
  get features() {
    return this._features
  }
  get biomes() {
    return this._biomes
  }
  set biomes(value) {
    this._biomes = value
  }
  get locations() {
    return this._locations
  }
  set locations(value) {
    this._locations = value
  }
  get cultures() {
    return this._cultures
  }
  set cultures(value) {
    this._cultures = value
  }
  get places() {
    return this._places
  }
  get polygons() {
    return this._polygons
  }
  set polygons(value) {
    this._polygons = value
  }
  get regions() {
    return this._regions
  }
  set regions(value) {
    this._regions = value
  }
  get nations() {
    return Object.values(this.regions).filter(r => !r.conquered)
  }
  get npcgen() {
    return this._npcgen
  }
  // global population
  get population() {
    return this._population
  }
  set population(value) {
    this._population = value
  }
  // gets the base regional culture distribution
  get normal() {
    return this.cultureDist
  }
  // gets world context
  get world() {
    return this
  }
  // rebuild voronoi diagram on object load
  rebuild() {
    let voronoi = d3.voronoi().extent([[0, 0], [this.dim.w, this.dim.h]])
    this.diagram = voronoi(this.sites)
  }
  // recalculate dimensions
  dimFinalize() {
    // average voronoi cell area (square miles)
    this._dim.cellArea = (this._dim.rh * this._dim.rw) / this._dim.cells
    // scaled height (used in distance calculations)
    this._dim.sh = this._dim.rh / this._dim.h
    // scaled width (used in distance calculations)
    this._dim.sw = this._dim.rw / this._dim.w
  }
  // number of days per year
  get orbital() {
    return this.star.orbital
  }
  // time calculations (degrees)
  get angularVelocity() {
    return 360 / this.rotation
  }
  // sun declination (degrees)
  sunDeclination(day) {
    const diff = this.date - new Date(this.date.getFullYear(), 0) / (1000 * 60 * 60 * this.rotation)
    const days = day || diff
    const position = (days / this.orbital) * 360
    return Math.asin(Math.sin(radians(-this.tilt)) * Math.cos(radians(position)))
  }
  // https://en.wikipedia.org/wiki/Hour_angle
  hourAngle(latitude, day) {
    const pre = -Math.tan(radians(latitude)) * Math.tan(this.sunDeclination(day))
    return degrees(Math.acos(pre > 1 ? 1 : pre < -1 ? -1 : pre))
  }
  // length of day (hours) at a given latitude
  dayTime(latitude, day) {
    return this.hourAngle(latitude, day) * 2 / this.angularVelocity
  }
  gps(x, y) {
    return {latitude: mapValue([this.dim.h, 0], this.bound.lat, y), longitude: mapValue([0, this.dim.w], this.bound.long, x)}
  }
  localtime(loc) {
    const { longitude } = this.gps(...loc.loc)
    // aprox 1 hour diff per 15 degrees
    const diff = Math.round(longitude / 15)
    const date = new Date(this.date)
    date.setHours(date.getHours() + diff)
    return date
  }
  temp_compute() {
    const w = [-90, this.climate_extremes.w] // polar winter
    const s = [90, this.climate_extremes.s] // polar summer
    const e = [0, this.climate_extremes.e] // equatorial
    const c = e[1]
    const a = (w[1] - c + (c - s[1]) / s[0] * w[0]) / (w[0] ** 2 - s[0] ** 2 / s[0] * w[0])
    const b = (w[1] - c - a * w[0] ** 2) / w[0]
    const d = b / a / 6 // monthly latitude diff
    return { a, b, c, d }
  }
  km(h) {
    return mapValue([this.coast, 1.5], [0, 6], h)
  }
  temperature(l, t, h) {
    const { a, b, c, d } = this.temp
    const x = l + t * d
    return a * x ** 2 - b * x + c - this.km(h) * 5
  }
}

export default World
