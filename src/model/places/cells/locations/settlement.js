import Location from '@/model/places/cells/location'
import { landmarks, settlements } from '@/model/utilities/constants'

import { mapValue } from '@/model/utilities/common'
import { Town } from './builders/town'
import { City, CapitalCity } from './builders/city'

const builders = {}
builders[settlements.town] = new Town()
builders[settlements.city] = new City()
builders[settlements.capital] = new CapitalCity()

class Settlement extends Location {
  constructor(ref, x, y, type, region, cell, trade, paths, port) {
    super(ref)
    if (ref) {
      // map coordinates of the settlement
      this.x = x
      this.y = y
      // settlement type (captial, city, town, etc.)
      this.type = type
      this.capital = type === settlements.capital
      // region that owns the settlement
      this._region = region
      // settlement voronoi cell location (index)
      this.site = cell
      // trade routes
      this.trade = trade
      this.trade.all = Array.from(new Set(this.trade.land.concat(this.trade.sea)))
      // distance to neighbors
      this.paths = paths
      // get all voronoi cells that are part of this settlement's territory
      this.land = this.region.land.filter(poly => poly.territory === this.idx)
      this.water = this.region.water.filter(poly => poly.territory === this.idx)
      // nearby water sources
      this.water.river = new Set()
      this.water.still = new Set()
      // iterate over voronoi cell neighbors to check for water cells (lakes and ocean)
      this.polygons[cell].n.map(p => this.polygons[p]).forEach(p => {
        p.type === landmarks.water && this.water.still.add(p.landmark)
      })
      // check to see if a  river flows through this location
      this.polygons[cell].river >= 0 && this.water.river.add(this.polygons[cell].river)
      this.water.river = Array.from(this.water.river)
      this.water.still = Array.from(this.water.still)
      // this city should have a port if it is on a sheltered coast (and on the ocean apparently - no lakes?)
      const ocean = this.water.still.some(p => this.features.landmarks[p] === landmarks.ocean)
      this.port = port && ocean
      // harbor names
      this.harbor = this.water.river.concat(this.water.still.filter(p => this.features.landmarks[p] !== landmarks.ocean)).length > 0
      // iterate through each cell in the territory and count biomes
      const biomes = new Set()
      const total = this.land.reduce((total, poly) => {
        biomes.add(poly.biome)
        return total + 1
      }, 0)
      // calculate percent distribution by dividing the biome count by the total territory count
      Array.from(biomes).forEach(biome => {
        const count = this.land.filter(poly => poly.biome === biome).length
        this.biomeDist[biome] = count / total * 100
      })
      // calculate economic advantage for each industry
      const economy = {
        minerals: 0,
        wood: 0,
        farming: 0,
        herding: 0,
        textile: 0
      }
      const biome = Object.entries(this.biomeDist)
      Object.keys(economy).forEach(industry => {
        economy[industry] = biome.reduce((total, [b, v]) => {
          return total + this.biomes[b].economics[industry] * v * 0.3
        }, 0)
      })
      economy.fishing = ocean ? -30 : 30
      this.distributions.economy = economy
      // settlement population
      this._urban = 0
      // territory population
      this.population = ~~Object.entries(this.biomeDist).reduce(
        (total, [b, percent]) => {
          return (
            total +
            this.biomes[b].population * percent / 100 * this.size * this.dim.cellArea
          )
        },
        0
      )
      // economic value
      this.resources = window.dice.norm(1, 0.15)
      // districts
      this._districts = []
      this.militaryDistrict = false
      this.magicDistrict = false
      this.templeDistrict = false
    }
  }
  // urban population
  get urban() {
    return this.population * this._urban
  }
  // rural population
  get rural() {
    return this.population * (1 - this._urban)
  }
  // settlements have a region
  get region() {
    return this.regions[this._region]
  }
  // settlements have a ruling culture
  get culture() {
    const colony = this.region.colony && [settlements.city, settlements.capital].includes(this.type)
    return colony ? this.region.colony : this.region.culture
  }
  // settlements have a location
  get loc() {
    return [this.x, this.y]
  }
  // location height
  get elev() {
    return this.polygons[this.site].h
  }
  // gets the base culture distribution (territory)
  get normal() {
    return this.cultureDist
  }
  // gets the minority culture distribution (territory)
  get minority() {
    return this.distributions.minority
  }
  // gets the majority culture distribution (territory)
  get majority() {
    return this.distributions.majority
  }
  // settlements are the cities
  get city() {
    return this
  }
  get origin() {
    return this.distributions.origin
  }
  get tradeWeight() {
    return Math.min(this.trade.all.length, 32) * 2.5 / 100
  }
  // settlement price modifiers
  get priceMods() {
    const mods = {
      minerals: 0,
      wood: 0,
      farming: 0,
      herding: 0,
      textile: 0,
      fishing: 0
    }
    // prices are weighted base on # of trading partners
    // more partners => more weight on traded goods
    const trade = this.tradeWeight
    const local = 1 - trade
    Object.keys(mods).forEach(industry => {
      mods[industry] = this.distributions.trade[industry] * trade + this.distributions.economy[industry] * local
    })
    return mods
  }
  // converts the district list to a mapping: name -> district
  get districts() {
    return this._districts.map(d => this.locations[d])
  }
  // aggregates all district entities
  get entities() {
    return this.districts.reduce((list, district) => {
      return list.concat(district.entities)
    }, [])
  }
  // number of law enforcement
  get guards() {
    return ~~(this.urban / mapValue([0, 1], [200, 75], this.region.traits.aggression))
  }
  get builder() {
    return builders[this.type]
  }
  build(population) {
    this.builder.build(this, population)
  }
  populate() {
    this.builder.populate(this)
  }
}

export default Settlement
