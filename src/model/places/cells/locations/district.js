import Location from '@/model/places/cells/location'
// District context; there is at least one district in towns and cities
class District extends Location {
  constructor(ref) {
    super(ref)
    // list of buildings in this district
    this._buildings = []
    // location type
    this.type = 'district'
  }
  // defaults to reference's population
  get population() {
    return this.ref.population
  }
  // district postfix based on regional title
  get postfix() {
    return this.region.titles.district
  }
  // converts building list to mapping by name
  get buildings() {
    return this._buildings.map(b => this.locations[b])
  }
  // gets the entities in all buildings
  get entities() {
    return this.buildings.reduce((list, building) => {
      return list.concat(building.entities)
    }, [])
  }
  // populates all buildings in the district
  populate() {
    this.buildings.forEach(building => {
      building.spawn()
    })
  }
}

export default District
