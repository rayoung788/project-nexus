import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

class CommonHouse extends Dwelling {
  constructor(ref) {
    super(ref)
    this.professions = ['Laborer', 'Merchent']
  }
  get patron() {
    return 'patron'
  }
  populate() {
    const profession = window.dice.choice(this.professions)
    // spawn a base tenent with randomly selected profession
    const tenant = this.npcgen[this.patron]({
      location: this,
      cultures: this.residents,
      profession: profession
    })
    this._entities.push(tenant)
    const odds = window.dice.random
    // tenent is married 30% of the time
    if (odds < 0.3) {
      const spouse = this.npcgen[this.patron]({
        location: this,
        profession: window.dice.choice([profession, 'Spouse']),
        relation: { relative: tenant, type: 'spouse' }
      })
      this._entities.push(spouse)
    }
    // tenent has 1-3 children 10% of the time
    if (odds < 0.1) {
      range(window.dice.randint(1, 3)).forEach(i => {
        const child = this.npcgen[this.patron]({
          location: this,
          relation: { relative: tenant, type: 'child' }
        })
        this._entities.push(child)
      })
    }
  }
}

class NobleHouse extends CommonHouse {
  constructor(ref) {
    super(ref)
    this.professions = ['Landowner', 'Aristocrat', 'Advocate']
  }
  get patron() {
    return 'noblePatron'
  }
}

class ForeignCommonHouse extends CommonHouse {
  get residents() {
    return this.minority
  }
}

export { CommonHouse, NobleHouse, ForeignCommonHouse }
