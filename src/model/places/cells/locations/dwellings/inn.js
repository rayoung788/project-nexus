import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

class Inn extends Dwelling {
  get patron() {
    return 'patron'
  }
  populate() {
    // spawn an innkeeper
    const owner = this.npcgen.owner({
      location: this,
      cultures: this.residents,
      profession: 'Innkeeper'
    })
    // spawn an chef (need multiple chefs?)
    const chef = this.npcgen.artisan({
      location: this,
      cultures: this.residents,
      profession: 'Chef'
    })
    // spawn patrons on a normal distribtuion
    const patrons = range(window.dice.norm(8, 1)).map(i =>
      this.npcgen[this.patron]({
        location: this,
        cultures: this.residents,
        profession: 'Patron'
      })
    )
    // spawn waiters on a normal distribtuion
    const waiters = range(window.dice.norm(3, 1)).map(i =>
      this.npcgen.apprentice({
        location: this,
        cultures: this.residents,
        profession: 'Waiter'
      })
    )
    // spawn servants on a normal distribtuion
    const servants = range(window.dice.norm(3, 1)).map(i =>
      this.npcgen.apprentice({
        location: this,
        cultures: this.residents,
        profession: 'Servant'
      })
    )
    this._entities = [owner, chef, ...patrons, ...waiters, ...servants]
  }
}

class NobleInn extends Inn {
  get residents() {
    return this.majority
  }
  get patron() {
    return 'noblePatron'
  }
}

class ForeignInn extends Inn {
  get residents() {
    return this.minority
  }
}

export { Inn, ForeignInn, NobleInn }
