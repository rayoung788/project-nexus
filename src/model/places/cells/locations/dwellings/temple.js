import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

class Temple extends Dwelling {
  populate() {
    // spawn 3-6 religous figures
    this._entities = range(window.dice.randint(3, 6)).map(i =>
      this.npcgen.artisan({
        location: this,
        cultures: this.residents,
        profession: this.region.titles.priest
      })
    )
  }
}

class ForeignTemple extends Temple {
  get residents() {
    return this.minority
  }
}

export { Temple, ForeignTemple }
