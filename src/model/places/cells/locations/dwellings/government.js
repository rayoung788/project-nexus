import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

import { settlements } from '@/model/utilities/constants'

class Embassy extends Dwelling {
  populate() {
    // regional diplomat title
    const title = this.region.titles.diplomat
    // spawn resident diplomat
    const diplomat = this.npcgen.artisan({
      location: this,
      cultures: this.majority,
      profession: `${title} (Home)`
    })
    // spawn foreign diplomats
    const foreigners = this.region.neighbors.map(region =>
      this.npcgen.diplomat({
        region,
        location: this,
        cultures: region.majority,
        profession: `${region.titles.diplomat} (${region.name})`
      })
    )
    this._entities = [...foreigners, diplomat]
  }
}

class Court extends Dwelling {
  get leader() {
    return 'leader'
  }
  populate() {
    // spawn leadership figure
    const leader = this.npcgen[this.leader]({
      location: this.city,
      cultures: this.origin
    })
    // spawn 1-3 advisors
    const advisors = range(window.dice.randint(1, 3)).map(i =>
      this.npcgen.advisor({ location: this, cultures: this.residents, profession: 'Advisor' })
    )
    this._entities = [...advisors, leader]
    // add a court wizard based on regional magic rating
    if (this.region.traits.magic > window.dice.random) {
      this._entities.push(
        this.npcgen.advisor({
          location: this.city,
          cultures: this.residents,
          profession: 'Court Wizard'
        })
      )
    }
  }
}

class HighCourt extends Court {
  get territories() {
    const province = this.polygons[this.city.site].province
    return this.region.territories.filter(loc => {
      return province === this.polygons[loc.site].province && loc.idx !== this.idx
    })
  }
  populate() {
    // do everything from normal courts
    super.populate()
    // plus collect councilers from towns within this city's province
    this.territories.forEach(city => {
      this._entities.push(
        this.npcgen.advisor({
          location: this.city,
          cultures: this.origin,
          profession: `Counciler (${city.name})`,
          last: city.greatHouse
        })
      )
    })
  }
}

class RoyalCourt extends HighCourt {
  get leader() {
    return 'king'
  }
  get provinces() {
    return this.region.territories.filter(loc => {
      return loc.type === settlements.city
    })
  }
  populate() {
    // do everything from higher courts
    super.populate()
    // plus collect provincial councilers from cities within the region
    if (this.region.traits.government > 0.2) {
      this.provinces.forEach(city => {
        this._entities.push(
          this.npcgen.advisor({
            location: this.city,
            cultures: this.origin,
            profession: `High Counciler (${city.name})`,
            last: city.greatHouse
          })
        )
      })
    }
    // add a religious representative based on regional religion rating
    if (this.region.traits.religion > 0.8 || this.region.traits.religion < 0.2) {
      this._entities.push(
        this.npcgen.advisor({
          location: this.city,
          cultures: this.majority,
          profession: this.region.titles.bishop
        })
      )
    }
    // TODO: ministers of finance, resources, science, etc. for managed economies
  }
}

class TownCourt extends Court {
  constructor(ref) {
    super(ref)
    this.council = 'Adviser'
    this.numCouncil = [3, 1]
  }
}

export { Embassy, RoyalCourt, HighCourt, TownCourt, Court }
