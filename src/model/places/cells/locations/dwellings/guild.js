import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

import { settlements } from '@/model/utilities/constants'

class Guild extends Dwelling {
  constructor(ref) {
    super(ref)
    this.leader = 'Grandmaster'
    this.apprentice = 'Recruit'
  }
  get members() {
    return []
  }
  populate() {
    // spawn a guild master
    const leader = this.city.type === settlements.capital ? this.leader : 'Master'
    const master = this.npcgen.master({
      location: this,
      cultures: this.residents,
      profession: `${window.dice.choice(this.members)} (${leader})`
    })
    const relatives = [{last: this.npcgen[master].last, cultures: {}}]
    relatives[0].cultures[this.npcgen[master].race.idx] = 1
    // spawn 3-5 veteren members with a chance to be related to the master
    const veterens = range(window.dice.randint(3, 5)).map(i => {
      let last = false
      let cultures = this.residents
      if (window.dice.random < 0.4) {
        const relative = window.dice.choice(relatives)
        last = relative.last
        cultures = relative.cultures
      }
      return this.npcgen.veteren({
        location: this,
        cultures,
        last,
        profession: `${window.dice.choice(this.members)} (Veteran)`
      })
    })
    veterens.forEach(v => {
      const relative = {last: this.npcgen[v].last, cultures: {}}
      relative.cultures[this.npcgen[v].race.idx] = 1
      relatives.push(relative)
    })
    // spawn 3-5 apprentice members with a chance to be related to the master or veterens
    const apprentices = range(window.dice.randint(3, 5)).map(i => {
      let last = false
      let cultures = this.residents
      if (window.dice.random < 0.4) {
        const relative = window.dice.choice(relatives)
        last = relative.last
        cultures = relative.cultures
      }
      return this.npcgen.apprentice({
        location: this,
        cultures,
        last,
        profession: `${window.dice.choice(this.members)} (${this.apprentice})`
      })
    })
    this._entities = [...apprentices, ...veterens, master]
  }
}

class MageGuild extends Guild {
  constructor(ref) {
    super(ref)
    this.leader = 'Archmage'
    this.apprentice = 'Apprentice'
  }
  get members() {
    return ['Elementalist', 'Sorcerer', 'Illusionist', 'Battlemage']
  }
}

class TemplarGuild extends Guild {
  get members() {
    return ['Priest', 'Inquisitor', 'Paladin', 'Demon Hunter']
  }
}

class EmeraldCircle extends Guild {
  get members() {
    return ['Druid', 'Shaman', 'Ranger', 'Witch Doctor']
  }
}

class MercenaryGuild extends Guild {
  get members() {
    return ['Mercenary', 'Knight', 'Berserker', 'Wanderer']
  }
}

class Syndicate extends Guild {
  constructor(ref) {
    super(ref)
    this.leader = 'Boss'
    this.name = `${window.dice.choice(['Syndicate', 'Brotherhood'])}: #${this.id}`
  }
  get members() {
    return ['NightBlade', 'Assassin', 'Thief']
  }
}

export { MageGuild, TemplarGuild, EmeraldCircle, MercenaryGuild, Syndicate }
