import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

class Shop extends Dwelling {
  constructor(ref, type) {
    super(ref)
    this.type = type
    this.name = `${type} (#${this.id})`
  }
  populate() {
    // spawn shop owner
    const owner = this.npcgen.owner({
      location: this,
      cultures: this.residents,
      profession: `${this.type} (Owner)`
    })
    const relative = { last: this.npcgen[owner].last, cultures: {} }
    relative.cultures[this.npcgen[owner].race.idx] = 1
    // spawn 0-2 apprentices with 80% to be related to the shop owner
    const apprentices = range(window.dice.randint(0, 2)).map(i => {
      let last = false
      let cultures = this.residents
      if (window.dice.random < 0.8) {
        last = relative.last
        cultures = relative.cultures
      }
      return this.npcgen.apprentice({
        location: this,
        cultures,
        profession: `${this.type} (Apprentice)`,
        last
      })
    })
    this._entities = [owner, ...apprentices]
  }
}

class ForeignShop extends Shop {
  get residents() {
    return this.minority
  }
}

export { Shop, ForeignShop }
