import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

import { mapValue } from '../../../../utilities/common'

class GuardBarracks extends Dwelling {
  populate() {
    // determine the number of gaurds to spawn based on city law enforcement count
    const count = mapValue([5, 750], [10, 50], this.city.guards)
    // spawn 15 gaurds per captain
    const numCaptains = Math.max(1, ~~(count / 15))
    // spawn npcs
    const captains = range(numCaptains).map(i =>
      this.npcgen.captain({ location: this, cultures: this.residents, profession: 'Guard Captain' })
    )
    const guards = range(count).map(i =>
      this.npcgen.soldier({ location: this, cultures: this.residents, profession: 'Guard' })
    )
    this._entities = [...captains, ...guards]
  }
}

class ArmyBarracks extends Dwelling {
  populate() {
    // spawn a general
    // TODO: spawn generals in court if no army barracks are present
    const general = this.npcgen.general({ location: this, cultures: this.majority, profession: 'General' })
    // base infantry units
    const units = ['Light Infantry', 'Light Infantry', 'Heavy Infantry']
    // ranged units based on regional technology rating
    if (this.region.traits.technology > 0.9) {
      units.push('Rifleman')
    } else if (this.region.traits.technology > 0.5) {
      units.push('Crossbowman')
    } else {
      units.push('Archer')
    }
    // siege units based on regional technology rating
    // TODO: magic siege weapons based on tech + magic? Arcane sentinals
    if (this.region.traits.technology > 0.5) {
      units.push('Siege Engineer')
    }
    // mage units based on regional magic rating
    // TODO: battlemages based on miltiary + magic?
    if (this.region.traits.magic > 0.5) {
      units.push('Warmage')
    }
    // spawn npcs
    this._entities = units.reduce((entities, unit) => {
      const officer = this.npcgen.captain({ location: this, cultures: this.residents, profession: `${unit} (Officer)` })
      const soldiers = range(window.dice.randint(3, 5)).map(i =>
        this.npcgen.soldier({ location: this, cultures: this.residents, profession: unit })
      )
      return [...entities, officer, ...soldiers]
    }, [general])
  }
}

// light/heavy infantry | archers/crossbowmen/riflemen | siege engineers

export {GuardBarracks, ArmyBarracks}
