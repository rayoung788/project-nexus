import Dwelling from '@/model/places/cells/locations/dwelling'
import { range } from '@/model/utilities/common'

class Ship extends Dwelling {
  populate() {
    // spawn ship captain
    const captain = this.npcgen.owner({
      location: this,
      cultures: this.residents,
      profession: 'Ship Captain'
    })
    // spawn sailors on a normal distribution
    const sailors = range(window.dice.norm(10, 2)).map(i =>
      this.npcgen.artisan({
        location: this,
        cultures: this.residents,
        profession: 'Sailor'
      })
    )
    this._entities = [captain, ...sailors]
  }
}

export default Ship
