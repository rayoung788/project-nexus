import District from '@/model/places/cells/locations/district'
import { shops } from '@/model/places/cells/locations/districts/trade'
import { Shop } from '@/model/places/cells/locations/dwellings/shop'
import Ship from '@/model/places/cells/locations/dwellings/ship'
import { Inn } from '@/model/places/cells/locations/dwellings/inn'
import { range } from '@/model/utilities/common'

class HarborDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = `${window.dice.choice(['Harbor', 'Dock'])} ${this.postfix}`)
    const essentials = [
      ...range(window.dice.randint(2, 3)).map(i => new Ship(this)),
      new Inn(this),
      new Shop(this, 'Fishmonger')
    ]
    this._buildings = [
      ...essentials,
      ...window.dice
        .sample(shops, window.dice.randint(2, 3))
        .map(shop => new Shop(this, shop))
    ].map(b => b.idx)
  }
}

export default HarborDistrict
