import District from '@/model/places/cells/locations/district'
import { CommonHouse } from '@/model/places/cells/locations/dwellings/residential'
import { Syndicate } from '@/model/places/cells/locations/dwellings/guild'
import { range } from '@/model/utilities/common'

import { Shop } from '@/model/places/cells/locations/dwellings/shop'
import { shops } from '@/model/places/cells/locations/districts/trade'
import { Inn } from '@/model/places/cells/locations/dwellings/inn'

class SlumDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = window.dice.choice(['Slums', 'Old Town']))
    const buildings = range(window.dice.randint(3, 5)).map(i => new CommonHouse(this))
    // TODO: need a better method for syndicate placement
    if (window.dice.random > 0.5) {
      buildings.push(new Syndicate(this))
    }
    this._buildings = buildings.map(b => b.idx)
  }
}

class MixedSlumDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = `Mixed ${this.postfix}`)
    const esentials = range(window.dice.randint(1, 3)).map(i => new CommonHouse(this))
    if (window.dice.random > 0.5) {
      esentials.push(new Inn(this))
    }
    // TODO: need a better method for syndicate placement
    if (window.dice.random > 0.5) {
      esentials.push(new Syndicate(this))
    }
    this._buildings = [
      ...esentials,
      ...window.dice.sample(shops, window.dice.randint(1, 3)).map(shop => new Shop(this, shop))
    ].map(b => b.idx)
  }
}

export { SlumDistrict, MixedSlumDistrict }
