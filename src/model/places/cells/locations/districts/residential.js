import District from '@/model/places/cells/locations/district'
import {CommonHouse, NobleHouse} from '@/model/places/cells/locations/dwellings/residential'
import { range } from '@/model/utilities/common'

import { Shop } from '@/model/places/cells/locations/dwellings/shop'
import { shops } from '@/model/places/cells/locations/districts/trade'
import { Inn } from '@/model/places/cells/locations/dwellings/inn'

class ResidentialDistrict extends District {
    constructor(ref) {
        super(ref)
        ref && (this.name = `Residential ${this.postfix}`)
        const commoners = range(window.dice.randint(5, 7)).map(i => new CommonHouse(this))
        this._buildings = [...commoners, new NobleHouse(this)].map(b => b.idx)
      }
}

class MixedResidentialDistrict extends District {
    constructor(ref) {
        super(ref)
        ref && (this.name = `Mixed ${this.postfix}`)
        const esentials = range(window.dice.randint(1, 3)).map(i => new CommonHouse(this))
        if (window.dice.random > 0.5) {
            esentials.push(new NobleHouse(this))
        }
        if (window.dice.random > 0.5) {
            esentials.push(new Inn(this))
        }
        this._buildings = [
            ...esentials,
            ...window.dice.sample(shops, window.dice.randint(1, 3)).map(shop => new Shop(this, shop))
          ].map(b => b.idx)
      }
}

export {ResidentialDistrict, MixedResidentialDistrict}
