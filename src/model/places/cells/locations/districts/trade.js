import District from '@/model/places/cells/locations/district'
import { Shop } from '@/model/places/cells/locations/dwellings/shop'
import { Inn } from '@/model/places/cells/locations/dwellings/inn'

const shops = [
  'Weaver',
  'Shoemaker',
  'Hatmaker',
  'Glovemaker',
  'Tailor',
  'Leatherworker',
  'Tanner',
  'Saddler',
  'Locksmith',
  'Woodseller',
  'Woodcarver',
  'Rugmaker',
  'Bookbinder',
  'Barber',
  'Jeweler',
  'Scabbardmaker',
  'Ropemaker',
  'Baker',
  'Wine-Seller',
  'Butcher',
  'Fishmonger',
  'Farmer',
  'Fletcher',
  'General Goods',
  'Alchemist',
  'Blacksmith',
  'Enchanter',
  'Produce Goods'
]

class TradeDistrict extends District {
  constructor(ref) {
    super(ref)
    ref &&
      (this.name = `${window.dice.choice(['Market', 'Merchant', 'Trade'])} ${
        this.postfix
      }`)
    const stores = [
      'General Goods',
      'Alchemist',
      'Blacksmith',
      'Produce Goods',
      ...window.dice.sample(shops, window.dice.randint(4, 8))
    ]
    this._buildings = [
      ...stores.map(store => new Shop(this, store)),
      new Inn(this)
    ].map(b => b.idx)
  }
}

export { shops, TradeDistrict }
