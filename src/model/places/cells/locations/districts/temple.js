import District from '@/model/places/cells/locations/district'
import { Temple } from '@/model/places/cells/locations/dwellings/temple'
import { TemplarGuild, EmeraldCircle } from '@/model/places/cells/locations/dwellings/guild'
import { Shop } from '@/model/places/cells/locations/dwellings/shop'

class TempleDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = `Temple ${this.postfix}`)
    const Guild = this.region.traits.religion > 0.5 ? TemplarGuild : EmeraldCircle
    const buildings = [Temple, Guild]
    this._buildings = [
      ...buildings.map(Building => new Building(this)),
      new Shop(this, 'Apothecary')
    ].map(b => b.idx)
  }
}

export default TempleDistrict
