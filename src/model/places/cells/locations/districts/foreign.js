import District from '@/model/places/cells/locations/district'
import { shops } from '@/model/places/cells/locations/districts/trade'
import { ForeignShop } from '@/model/places/cells/locations/dwellings/shop'
import { ForeignInn } from '@/model/places/cells/locations/dwellings/inn'
import { ForeignCommonHouse } from '@/model/places/cells/locations/dwellings/residential'
import { range } from '@/model/utilities/common'

class ForeignDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = `Foreign ${this.postfix}`)
    const essentials = [
      ...range(window.dice.randint(1, 3)).map(i => new ForeignCommonHouse(this)),
      new ForeignInn(this)
    ]
    this._buildings = [
      ...essentials,
      ...window.dice
        .sample(shops, window.dice.randint(1, 3))
        .map(shop => new ForeignShop(this, shop))
    ].map(b => b.idx)
  }
}

export default ForeignDistrict
