import District from '@/model/places/cells/locations/district'
import { NobleHouse } from '@/model/places/cells/locations/dwellings/residential'
import * as goverment from '@/model/places/cells/locations/dwellings/government'
import { GuardBarracks } from '@/model/places/cells/locations/dwellings/barracks'
import { NobleInn } from '@/model/places/cells/locations/dwellings/inn'
import { range } from '@/model/utilities/common'

import { MageGuild, MercenaryGuild, TemplarGuild, EmeraldCircle } from '@/model/places/cells/locations/dwellings/guild'

class NobleDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = `${this.title} ${this.postfix}`)
    const units = range(window.dice.randint(2, 3)).map(i => new NobleHouse(this))
    const extra = [new NobleInn(this)]
    // fill with essential buildings if the requisite districts are absent
    if (!this.city.militaryDistrict) {
      extra.push(new GuardBarracks(this))
    }
    if (!this.city.militaryDistrict && this.region.advantage.military > 0.3) {
      extra.push(new MercenaryGuild(this))
    }
    if (!this.city.magicDistrict && this.region.traits.magic > 0.3) {
      extra.push(new MageGuild(this))
    }
    if (!this.city.templeDistrict && (this.region.traits.religion > 0.7 || this.region.traits.religion < 0.3)) {
      const Guild = this.region.traits.religion > 0.5 ? TemplarGuild : EmeraldCircle
      extra.push(new Guild(this))
    }
    this._buildings = [...units, ...extra, ...this.leadership.map(Building => new Building(this))].map(b => b.idx)
  }

  get title() {
    return 'Noble'
  }

  get leadership() {
    return [goverment.Court]
  }
}

class LordsDistrict extends NobleDistrict {
  get leadership() {
    return [goverment.HighCourt]
  }
}

class RoyalDistrict extends NobleDistrict {
  get title() {
    return 'Royal'
  }

  get leadership() {
    return [goverment.RoyalCourt, goverment.Embassy]
  }
}

export { NobleDistrict, LordsDistrict, RoyalDistrict }
