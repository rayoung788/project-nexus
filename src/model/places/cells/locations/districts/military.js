import District from '@/model/places/cells/locations/district'
import {
  GuardBarracks,
  ArmyBarracks
} from '@/model/places/cells/locations/dwellings/barracks'
import { Shop } from '@/model/places/cells/locations/dwellings/shop'
import { MercenaryGuild } from '@/model/places/cells/locations/dwellings/guild'

class MilitaryDistrict extends District {
  constructor(ref) {
    super(ref)
    ref && (this.name = `${window.dice.choice(['Military', 'War'])} ${this.postfix}`)
    const structures = window.dice.sample(
      ['Armorsmith', 'Mercenary Guild', 'Weaponsmith'],
      window.dice.randint(1, 3)
    )
    this._buildings = [
      ...structures.map(
        struct =>
          struct.includes('smith')
            ? new Shop(this, struct)
            : new MercenaryGuild(this)
      ),
      new GuardBarracks(this),
      new ArmyBarracks(this)
    ].map(b => b.idx)
  }
}

export default MilitaryDistrict
