import District from '@/model/places/cells/locations/district'
import { Shop } from '@/model/places/cells/locations/dwellings/shop'
import { MageGuild } from '@/model/places/cells/locations/dwellings/guild'

class MagicDistrict extends District {
  constructor(ref) {
    super(ref)
    ref &&
      (this.name = `${window.dice.choice(['Magic', 'Mystic', 'Arcane'])} ${
        this.postfix
      }`)
    const structures = window.dice.sample(
      ['Archivist', 'Enchanter', 'Scribe', 'Teleporter'],
      window.dice.randint(1, 3)
    )
    this._buildings = [
      ...structures.map(struct => new Shop(this, struct)),
      new MageGuild(this)
    ].map(b => b.idx)
  }
}

export default MagicDistrict
