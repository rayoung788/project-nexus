import { SlumDistrict, MixedSlumDistrict } from './../districts/slum'
import HarborDistrict from './../districts/harbor'

import { TradeDistrict } from '../districts/trade'
import {
  ResidentialDistrict,
  MixedResidentialDistrict
} from './../districts/residential'
import { NobleDistrict } from '../districts/noble'
import { range } from '@/model/utilities/common'
// Towns are settlements with 1000 < population < 8000
class Town {
  // governing district style
  get Government() {
    return NobleDistrict
  }
  // residential district generator (chance for middle class or lower class based on regional corruption rating)
  residential(subject) {
    return subject.region.traits.economy > window.dice.random
      ? SlumDistrict
      : ResidentialDistrict
  }
  // mixed district generator (chance for middle class or lower class based on regional corruption rating)
  mixed(subject) {
    return subject.region.traits.economy > window.dice.random
      ? MixedSlumDistrict
      : MixedResidentialDistrict
  }

  build(subject, population) {
    // subject settlement to build
    subject._urban = Math.min(1, population / subject.population)
    // initial cultual dist
    subject.cultureDist[subject.culture.idx] = 100
    subject.distributions.majority[subject.culture.idx] = 100
    subject.distributions.origin[subject.culture.idx] = 100
    // economic value
    subject.value =
      ((subject.tradeWeight * 2 + subject.resources) *
        (subject.urban * 0.99 + subject.rural * 0.01)) /
      100000
    // governing house name
    subject.greatHouse = subject.culture.genName('last', 'great house')
    // name of the settlement
    subject.name = subject.namePlace('settlement', subject)
  }

  nonEssential(subject) {
    // compute the number of non-essential districts based on population
    const districts = ~~(subject.urban / 3000)
    // sets the excess district types
    const excess = ['Residential', TradeDistrict, 'Mixed']
    return range(districts).map(() => {
      // randomly select from the excess list
      let District = window.dice.choice(excess)
      if (District === 'Residential') {
        // randomly pick middle or lower class residences
        District = this.residential(subject)
      }
      if (District === 'Mixed') {
        // randomly pick middle or lower class residences
        District = this.mixed(subject)
      }
      return new District(subject)
    })
  }

  populate(subject) {
    // create non-essential districts
    const districts = this.nonEssential(subject)
    // create essential districts
    subject._districts = districts
      .concat([
        new TradeDistrict(subject),
        new (subject.port ? HarborDistrict : this.residential(subject))(subject),
        new this.Government(subject)
      ])
      .map(d => d.idx)
    // populate each district
    subject.districts.forEach(district => district.spawn())
    // const cultures = Object.keys(this.normal).map(k => this.cultures[k])
    // const entities = this.entities
    // call the matchmaker to finalize lineages
    // this.npcgen.matchmaker(entities, cultures)
  }
}

export { Town }
