import ForeignDistrict from './../districts/foreign'
import MagicDistrict from './../districts/magic'
import MilitaryDistrict from './../districts/military'
import TempleDistrict from './../districts/temple'

import { TradeDistrict } from '../districts/trade'
import { LordsDistrict, RoyalDistrict } from '../districts/noble'
import { range } from '@/model/utilities/common'
import { Town } from '@/model/places/cells/locations/builders/town'
// Cities are settlements with population > 8000
class City extends Town {
  get Government() {
    // includes provincial council
    return LordsDistrict
  }
  nonEssential(subject) {
    // base non-essential districts
    const nonEssential = ['Mixed', TradeDistrict]
    // compute the number of non-essential districts based on population
    const districts = ~~(subject.urban / 3000)
    // check if a military district is needed
    if (subject.region.advantage.military > 0.6) {
      nonEssential.push(MilitaryDistrict)
    }
    // check if a magic district is needed
    if (subject.region.traits.magic > 0.6) {
      nonEssential.push(MagicDistrict)
    }
    // check if a temple district is needed
    if (subject.region.traits.religion > 0.8 || subject.region.traits.religion < 0.2) {
      nonEssential.push(TempleDistrict)
    }
    // check if a foreign district is needed
    if (subject.region.traits.liberalism > 0.4) {
      nonEssential.push(ForeignDistrict)
    }
    // check if a second foreign district is needed (large cities)
    if (subject.region.traits.liberalism > 0.4 && districts > 10) {
      nonEssential.push(ForeignDistrict)
    }
    // no priorities
    window.dice.shuffle(nonEssential)
    // excess districts if all non-essentials are consumed
    const excess = ['Residential', TradeDistrict, 'Mixed']
    return range(districts).map(() => {
      let District = window.dice.choice(excess)
      if (nonEssential.length > 0) {
        District = nonEssential.shift()
      }
      if (District === 'Residential') {
        // randomly pick middle or lower class residences
        District = this.residential(subject)
      }
      if (District === 'Mixed') {
        // randomly pick middle or lower class residences
        District = this.mixed(subject)
      }
      if (District === MilitaryDistrict) {
        subject.militaryDistrict = true
      }
      if (District === MagicDistrict) {
        subject.magicDistrict = true
      }
      if (District === TempleDistrict) {
        subject.templeDistrict = true
      }
      return new District(subject)
    })
  }
}
// Capitals are the regional seat of power
class CapitalCity extends City {
  get Government() {
    // includes regional council
    return RoyalDistrict
  }
}

export { City, CapitalCity }
