import Location from '@/model/places/cells/location'

// Dwelling context; each district has many dwellings (buildings)
class Dwelling extends Location {
  constructor(ref) {
    super(ref)
    // dwelling unqiue id
    this.id = window.dice.generateId()
    // dwelling name + id
    this.name = `${this.constructor.name} (#${this.id})`
    // list of entities located in this building
    this._entities = []
    // location type
    this.type = 'dwelling'
  }
  // default resident culture distribution
  get residents() {
    return this.normal
  }
  // list of entities used by references
  get entities() {
      return this._entities
  }
  // defaults to reference's population
  get population() {
    return this.ref.population
  }
  // override to include district and city name
  get description() {
    let extra = `${this.city.name}`
    if (this.ref.type === 'district') {
      extra = `${extra} | ${this.ref.name}`
    }
    return `${extra} | ${this.name}`
  }
}

export default Dwelling
