import Shaper from './../shaper'
import { landmarks, settlements } from './../../utilities/constants'
import Region from '@/model/places/cells/region'

class RegionalShaper extends Shaper {
  get pipeline() {
    return [
      { name: 'Capital City Placement', action: this.placeCapitals },
      { name: 'Regional Spheres', action: this.regionalSpheres },
      { name: 'Finalize Regions', action: this.finalizeRegions }
    ]
  }
  baseScores() {
    // base land scores for city placement
    this.land.forEach(poly => {
      // prefer lower elevations
      let score = (1 - poly.h) * 5
      if (poly.type === landmarks.coast) {
        // bonus for sheltered harbors
        score += Math.max(0, 4 - poly.harbor)
      }
      poly.score = score
    })
  }
  placeCapitals() {
    // compute base land scores
    this.baseScores()
    const capitals = this.world.nRegions
    const spacing = (this.dim.w + this.dim.h) * this.spacing.regions
    // place the regional capitals
    this.placeCities(capitals, settlements.capital, () => spacing)
    this.land.filter(poly => poly.location).forEach(poly => {
      if (poly.harbor > 0) {
        // force capital cities to be ports if the are on the coast
        const city = this.locations[poly.location]
        city.port = true
        // move city to the coast
        const { x, y } = window.dice.choice(poly.coastal)
        city.x = x
        city.y = y
        poly.site = [city.x, city.y]
        poly.harbor = 1
      }
    })
  }
  regionalSpheres() {
    let queue = this.locations
      .filter(city => city.type === settlements.capital)
      .map(city => this.polygons[city.cell])
    // use capital cities to start the regional floodfill
    let idx = 1
    queue.forEach(poly => {
      const city = this.locations[poly.location]
      poly.region = idx
      city.region = poly.region
      // initialize temp region data
      this.regions[idx] = {
        capital: city.idx,
        idx: poly.region,
        power: window.dice.uniform(0.5, 1),
        borders: {}
      }
      idx += 1
    })
    while (queue.length > 0) {
      // grab the next item in the queue
      const poly = queue.shift()
      const region = this.regions[poly.region]
      // get the regional power
      let power = region.power
      if (poly.type === landmarks.water || poly.h >= this.mountains) {
        power /= 50 // penalty for crossing mountains & water
      }
      // skip cell if power is insufficient
      if (window.dice.random < power) {
        // otherwise process neighbors
        poly.n.map(n => this.polygons[n]).forEach(n => {
          // claim neighbor if not claimed
          if (n.region === undefined) {
            n.region = poly.region
            queue.push(n)
          } else if (n.region !== poly.region) {
            // otherwise mark neighbor as border
            !region.borders[n.region] && (region.borders[n.region] = 0)
            region.borders[n.region] += 1
            // penalize water borders
            if (poly.type === landmarks.water || n.type === landmarks.water) {
              region.borders[n.region] -= 0.5
            }
            poly.border = true
          }
        })
      } else {
        queue.push(poly)
      }
    }
  }
  finalizeRegions() {
    this.world.ridx = Object.entries(this.regions).length + 1
    // use temp region objects to create the final region objects
    this.world.regions = Object.entries(this.regions).reduce((dict, [k, v]) => {
      const { idx, capital, borders } = v
      dict[k] = new Region(this.world, idx, capital, borders)
      return dict
    }, {})
  }
}
export default RegionalShaper
