import Shaper from './../shaper'
import { range } from '@/model/utilities/common'
import { settlements } from '@/model/utilities/constants'

class LoreShaper extends Shaper {
  get pipeline() {
    return [
      {
        name: 'Diffuse Economy',
        action: this.diffuseEconomy
      },
      {
        name: 'Diffuse Cultures',
        action: this.diffuseCultures
      },
      {
        name: 'Relations',
        action: this.relations
      },
      {
        name: 'History',
        action: this.history
      }
    ]
  }
  // simulate trade amongst neighboring cities
  diffuseEconomy() {
    const base = {
      minerals: 0,
      wood: 0,
      farming: 0,
      herding: 0,
      textile: 0,
      fishing: 0
    }
    this.locations.forEach(loc => {
      const partners = loc.trade.all.map(t => this.locations[t])
      loc.distributions.trade = { ...base }
      partners.forEach(partner => {
        Object.entries(partner.distributions.economy).forEach(([k, v]) => {
          loc.distributions.trade[k] += v
        })
      })
      Object.entries(loc.distributions.trade).forEach(([k, v]) => {
        loc.distributions.trade[k] /= partners.length
      })
    })
    range(5).forEach(() => {
      this.locations.forEach(loc => {
        loc.distributions.trade = { ...base }
        const partners = loc.trade.all.map(t => this.locations[t])
        partners.forEach(partner => {
          Object.entries(partner.distributions.trade).forEach(([k, v]) => {
            loc.distributions.trade[k] += v
          })
        })
        Object.entries(loc.distributions.trade).forEach(([k, v]) => {
          loc.distributions.trade[k] /= partners.length
        })
      })
    })
  }

  localCulture(loc, localMod) {
    const occupation = loc.type === settlements.town ? window.dice.uniform(0, 20) : window.dice.uniform(20, 40)
    let [local, imperial] = loc.region.conquered && !loc.region.colony ? [100 - occupation, occupation] : [100, 0]
    loc.distributions.culture[loc.culture.idx] += local * localMod
    loc.foreign_presence = occupation
    if (imperial > 0) {
      loc.distributions.culture[loc.region.agent._culture] += imperial * localMod
    }
  }

  diffuseCultures() {
    const localMod = 2
    // init all cultures to zero
    const cultures = {}
    Object.keys(this.cultures).forEach(k => {
      cultures[k] = 0
    })
    this.locations.forEach(loc => {
      const partners = loc.trade.all.map(t => this.locations[t])
      loc.distributions.culture = {
        ...cultures
      }
      this.localCulture(loc, localMod)
      partners.forEach(partner => {
        loc.distributions.culture[partner.culture.idx] += 100
      })
      Object.entries(loc.distributions.culture).forEach(([k, v]) => {
        loc.distributions.culture[k] /= partners.length + 2
      })
    })
    range(5).forEach(() => {
      this.locations.forEach(loc => {
        loc.distributions.culture = {
          ...cultures
        }
        this.localCulture(loc, localMod)
        const partners = loc.trade.all.map(t => this.locations[t])
        partners.forEach(partner => {
          Object.entries(partner.distributions.culture).forEach(([k, v]) => {
            loc.distributions.culture[k] += v
          })
        })
        Object.entries(loc.distributions.culture).forEach(([k]) => {
          loc.distributions.culture[k] /= partners.length + localMod
        })
      })
    })
    // aggregate for regional cultures
    Object.values(this.regions).forEach(region => {
      // calculate territory population based on biome modifiers
      region.territories.forEach(terr => {
        Object.entries(terr.distributions.culture).forEach(([k, v]) => {
          terr.cultureDist[k] = v
        })
        // finalize display caching
        this.finalize(terr)
      })
      // finalize display caching
      this.finalize(region)
    })
  }

  // culture distributions
  majority(loc) {
    // sort cultures by percent
    const sorted = Object.entries(loc.normal).sort((a, b) => {
      return b[1] - a[1]
    })
    const majors = []
    // Majority criterion:
    // there is less than 15% difference between the highest culture
    // the prospect has at least 5% contribution
    sorted.forEach(([culture, p]) => {
      if (sorted[0][1] - p < 15 && p > 5) {
        majors.push([culture, p])
      }
    })
    // assign majority
    loc.distributions.majority = majors.reduce((res, [culture, p]) => {
      res[culture] = p
      return res
    }, {})
  }

  minority(loc) {
    const majority = loc.distributions.majority
    const majoritySet = Object.keys(majority)
    // all cultures not in the majority are minority
    const minority = Object.keys(loc.normal).filter(culture => {
      return !majoritySet.includes(culture)
    })
    // assign minority; if there is no minority - assign majority
    loc.distributions.minority =
      minority.length > 0
        ? minority.reduce((res, culture) => {
            res[culture] = loc.normal[culture]
            return res
          }, {})
        : majority
  }

  finalize(cell) {
    // find majority cultures
    this.majority(cell)
    // find minority cultures
    this.minority(cell)
  }
  relations() {
    Object.values(this.regions).forEach(region => {
      const borders = region.land
        .concat(region.water)
        .filter(p => {
          return p.border && p.path > 0
        })
        .reduce((sum, p) => {
          return sum.concat(
            p.n
              .map(n => this.polygons[n])
              .filter(n => {
                return n.region !== p.region && n.path > 0
              })
              .map(n => n.region)
          )
        }, [])
      region.contacts = Array.from(new Set(borders)).map(r => this.regions[r])
      region.contacts.forEach(neighbor => {
        this.similarity(region, neighbor)
      })
    })
  }
  similarity(r1, r2) {
    if (!r1.similarity[r2.idx]) {
      // compute past history (border conflicts & past alliances)
      const history = window.dice.norm(0, 0.25)
      r1.relations[r2.idx] = history
      r2.relations[r1.idx] = history
      // compute how 'similar' these regions are
      // similar regions are more likely to get along
      r1.similarity[r2.idx] = 0
      r2.similarity[r1.idx] = 0
      Object.keys(r1.traits).forEach(k => {
        const diff = Math.abs(r1.traits[k] - r2.traits[k])
        // reward similarities and punish disimilarites
        const boost = 0.05
        const sim = diff < 0.1 ? boost : diff < 0.2 ? 0 : -boost
        r1.similarity[r2.idx] += sim
        r2.similarity[r1.idx] += sim
      })
    }
  }
  placeholder(r, time) {
    r.recordEvent([new Date(time), r.idx, `Event has occured in ${r.name}`])
    return ~~(window.dice.random * 120)
  }
  union(r, time) {
    const prospects = r.neighbors.filter(n => r.military > 2 * n.military)
    if (prospects.length > 0) {
      const consumed = prospects[0]
      const invader = r.military
      const defender = consumed.military
      consumed.conquered = r.idx
      r.recordEvent([new Date(time), `${r.name} has conquered ${consumed.name} (${r.vassals.length}) :: (${invader.toFixed(2)} > ${defender.toFixed(2)})`])
    }
    return ~~(window.dice.random * 120 + 200)
  }
  divide(r, time) {
    // find the furthest vassal from host
    const home = r.capital
    const idx = r.vassals.reduce(
      (furthest, idx) => {
        const distance = home.distanceTo(this.regions[idx].capital)
        return distance > furthest.distance ? { distance, idx } : furthest
      },
      { distance: 0, idx: 0 }
    ).idx
    if (idx !== 0) {
      const rebel = this.regions[idx]
      rebel.conquered = false
      r.recordEvent([new Date(time), `${rebel.name} has declared independence from ${r.name} (${r.vassals.length})`])
    }
    return ~~(window.dice.random * 120 + 200)
  }
  darkages(r, time) {
    r.vassals.map(v => this.regions[v]).forEach(v => {
      v.conquered = false
    })
    r.recordEvent([new Date(time), `The Great Empire of ${r.name} has fallen! (${r.vassals.length})`])
    return ~~(window.dice.random * 120 + 200)
  }
  event(r, time) {
    const hegemony = r.neighbors.length < 1
    if (hegemony) {
      return this.darkages(r, time)
    }
    return window.dice.random > 0.5 ? this.union(r, time) : this.divide(r, time)
  }
  history() {
    // small chance of events happening every x months
    /**
     * War: x invades y
     */
    const time = this.world.date
    range(10).forEach(() => {
      Object.values(this.regions)
        .filter(() => window.dice.random > 0.95)
        .forEach(r => {
          if (this.regions[r.idx] && !r.conquered) {
            const step = this.placeholder(r, time)
            time.setDate(time.getDate() + step)
          }
        })
      // const step = ~~(window.dice.random * 600) // number of days passed
      // time.setDate(time.getDate() + step)
    })
  }
}
export default LoreShaper
