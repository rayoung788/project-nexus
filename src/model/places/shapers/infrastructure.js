import Shaper from './../shaper'
import PriorityQueue from 'js-priority-queue'
import * as d3 from 'd3'
import { landmarks, settlements } from './../../utilities/constants'

class InfrastructureShaper extends Shaper {
  constructor(world) {
    super(world)
    this.ntowns = window.dice.randint(...this.world.nTowns) // NEEDS SCALING
    this.tspacing = (this.dim.w + this.dim.h) * this.spacing.towns
    const highres = this.world.highres ? 5 : 0
    this.pathing = {
      sea: 20 + highres,
      main: 35 + highres,
      small: 35 + highres
    }
  }
  get pipeline() {
    return [
      { name: 'Main Roads', action: this.mainRoads },
      { name: 'Town Placement', action: this.placeTowns },
      { name: 'Major City Placement', action: this.selectMajorCities },
      { name: 'City Influence', action: this.territoryLines },
      { name: 'Minor Roads', action: this.smallRoads },
      { name: 'Sea Routes', action: this.seaRoutes }
    ]
  }
  mainRoads() {
    const locs = this.land.filter(poly => {
      return poly.location !== undefined
    })
    const blacklist = locs.reduce((org, city) => {
      org[city.location] = []
      return org
    }, {})
    const type = 'main'
    // iterate through all islands
    d3.range(...this.features.land).forEach(i => {
      // get all captials on the island
      const capitals = locs.filter(poly => poly.landmark === i)
      if (capitals.length > 1) {
        for (let j = 1; j < capitals.length; j++) {
          for (let k = 0; k < j; k++) {
            // find the shortest path to each capital on the island
            const src = this.locations[capitals[j].location]
            const dst = this.locations[capitals[k].location]
            const close =
              Math.hypot(src.x - dst.x, src.y - dst.y) <
              this.dim.w * this.spacing.main
            // record the land trade route
            if (
              close &&
              !src.trade.land.includes(dst.idx) &&
              !blacklist[src.idx].includes(dst.idx)
            ) {
              const dist = this.shortestPath(capitals[j].idx, capitals[k].idx, type)
              this.addPaths(src, dst, dist, blacklist, type)
            }
          }
        }
      }
    })
  }
  geographyScores() {
    this.land.forEach(poly => {
      // prefer lower elevations
      let score = (1 - poly.h) * 5
      if (poly.type === landmarks.coast) {
        // bonus for sheltered harbors
        score += Math.max(0, 4 - poly.harbor)
        // bonus for estuaries
        poly.river >= 0 && poly.harbor === 1 && (score += 3)
      }
      // river bonus
      poly.flux > 1 && (score += poly.flux / 5)
      // confluence bonus
      poly.confluence && (score += 2)
      const biome = this.biomes[poly.biome]
      // biome penalty
      poly.score = score * biome.mod
      // bonus for building along roads
      if (poly.path) {
        poly.score += Math.max(poly.path / 10, 1)
      }
    })
  }
  placeTowns() {
    const num = this.ntowns
    const spacing = this.tspacing
    // compute geography scores
    this.geographyScores()
    // place towns based on cell scores
    this.placeCities(
      num,
      settlements.town,
      () => spacing * (window.dice.random + 0.5)
    )
    // make sure there are no isolated island cities
    this.checkIslands()
  }
  selectMajorCities() {
    // TODO: medium roads connecting cities
    // selects towns to become cities
    const spacing = this.tspacing
    // iterate through all regions
    Object.values(this.regions).forEach(region => {
      // get all the towns in the region
      const towns = this.locations.filter(
        city => city.region === region.idx && city.type === settlements.town
      )
      // sort towns by score
      let cells = towns.map(town => this.polygons[town.cell]).sort(this.compareScore)
      // TODO: use quad tree
      const majors = [region.capital]
      // 1 city per 10 towns
      const mcnt = ~~(towns.length / 10)
      d3.range(mcnt).forEach(() => {
        // find towns that are not too close to other cities
        cells = cells.filter(cell => {
          const city = this.locations[cell.location]
          return majors.every(major => {
            return Math.hypot(city.x - major.x, city.y - major.y) > spacing * 2
          })
        })
        // add them as major cities
        if (cells.length > 0) {
          const city = this.locations[cells[0].location]
          majors.push(city)
          city.type = settlements.city
          if (cells[0].harbor > 0) {
            // force major cities to be ports if the are on the coast
            city.port = true
            // move city to the coast
            const { x, y } = window.dice.choice(cells[0].coastal)
            city.x = x
            city.y = y
            cells[0].site = [city.x, city.y]
            cells[0].harbor = 1
          }
        }
      })
    })
  }
  territoryLines() {
    // city territories
    // each city will be responsible for a "territory" (collections of land cells)
    // fill queue with all locations to start
    let queue = this.locations.map(city => {
      this.polygons[city.cell].territory = city.idx
      return this.polygons[city.cell]
    })
    while (queue.length > 0) {
      // grab the next item in the queue
      const poly = queue.shift()
      poly.n
        .map(n => this.polygons[n])
        .forEach(n => {
          // expand the location's territory if unclaimed
          if (n.territory === undefined && n.region === poly.region) {
            n.territory = poly.territory
            queue.push(n)
          }
        })
    }
    // regional provinces
    // each major city will be responsible for a "province" (collections of territories)
    // fill queue with all cities to start
    // TODO: use a quad tree to find closest city to each town instead of floodfill
    queue = this.locations
      .filter(city => [settlements.city, settlements.capital].includes(city.type))
      .map(city => {
        this.polygons[city.cell].province = city.idx
        return this.polygons[city.cell]
      })
    while (queue.length > 0) {
      // grab the next item in the queue
      const poly = queue.shift()
      poly.n
        .map(n => this.polygons[n])
        .forEach(n => {
          // expand the city's province if unclaimed
          if (n.province === undefined && n.region === poly.region) {
            n.province = poly.province
            queue.push(n)
          }
        })
    }
  }
  smallRoads() {
    const locs = this.land.filter(poly => {
      return poly.location !== undefined
    })
    const blacklist = locs.reduce((org, town) => {
      org[town.location] = []
      return org
    }, {})
    const type = 'small'
    // iterate through all islands
    d3.range(...this.features.land).forEach(i => {
      // get all locations on the island
      const cities = locs.filter(poly => poly.landmark === i)
      // get all towns on the island
      const towns = cities.filter(
        poly => this.locations[poly.location].type !== settlements.capital
      )
      if (cities.length > 1) {
        for (let j = 0; j < towns.length; j++) {
          const start = cities[j]
          const src = this.locations[start.location]
          // find all nearby locations
          cities
            .filter(city => {
              const dist = Math.hypot(
                start.site[0] - city.site[0],
                start.site[1] - city.site[1]
              )
              return dist < this.dim.w * this.spacing.small && city.idx !== start.idx
            })
            .forEach(city => {
              // find shortest path from current town to nearby location
              const dst = this.locations[city.location]
              if (
                !src.trade.land.includes(dst.idx) &&
                !blacklist[src.idx].includes(dst.idx)
              ) {
                const dist = this.shortestPath(start.idx, city.idx, type)
                this.addPaths(src, dst, dist, blacklist, type)
              }
            })
        }
        // check for towns that have no connecting routes
        towns.forEach(town => {
          if (!town.path) {
            console.log('edge town')
            const src = this.locations[town.location]
            // find the closest town
            const closest = cities.reduce(
              (res, n) => {
                let dist = Math.hypot(
                  town.site[0] - n.site[0],
                  town.site[1] - n.site[1]
                )
                n.location === town.location && (dist = Infinity)
                const prospect = {
                  dist,
                  town: n
                }
                return res.dist > dist ? prospect : res
              },
              {
                dist: Infinity
              }
            )
            if (closest.town) {
              // build a road to connect the isolated town
              const dist = this.shortestPath(town.idx, closest.town.idx, 'small')
              const dst = this.locations[closest.town.location]
              this.addPaths(src, dst, dist, blacklist, 'land')
            }
          }
        })
      }
    })
  }
  checkIslands() {
    // iterate through all islands
    d3.range(...this.features.land).forEach(i => {
      // get all cities located on the island
      const cities = this.land.filter(
        poly => poly.landmark === i && poly.location !== undefined
      )
      if (cities.length > 0) {
        const ports = cities.filter(c => this.locations[c.location].port)
        // make sure there is at least one port on each island where there are cities
        if (ports.length < 1) {
          // if there isn't, force a coastal city to be a port
          const harbors = cities.filter(c => c.harbor && c.type === landmarks.coast)
          if (harbors.length > 0) {
            const poly = harbors[0]
            const city = this.locations[poly.location]
            poly.harbor = 1
            city.port = true
            const { x, y } = window.dice.choice(poly.coastal)
            city.x = x
            city.y = y
            poly.site = [city.x, city.y]
          } else {
            console.log('Inaccessible island detected.')
          }
        }
      }
    })
  }
  seaRoutes() {
    // iterate through all water bodies
    const all_ports = this.land.filter(poly => {
      return poly.location !== undefined && this.locations[poly.location].port
    })
    const blacklist = all_ports.reduce((org, port) => {
      org[port.location] = []
      return org
    }, {})
    const type = 'sea'
    d3.range(...this.features.water).forEach(i => {
      // get all ports on the water body
      const ports = all_ports.filter(poly => {
        return poly.n.some(nidx => {
          return this.polygons[nidx].landmark === i
        })
      })
      if (ports.length > 1) {
        const sight = this.dim.w * this.spacing.ocean
        for (let j = 0; j < ports.length; j++) {
          const start = ports[j]
          const src = this.locations[start.location]
          // find all nearby ports
          ports
            .filter(city => {
              const dist = Math.hypot(
                start.site[0] - city.site[0],
                start.site[1] - city.site[1]
              )
              return dist < sight && city.idx !== start.idx
            })
            .forEach(city => {
              // generate ocean routes between nearby ports
              const dst = this.locations[city.location]
              if (
                !src.trade.sea.includes(dst.idx) &&
                !blacklist[src.idx].includes(dst.idx)
              ) {
                const dist = this.shortestPath(
                  start.idx,
                  city.idx,
                  type,
                  h => h < this.coast
                )
                this.addPaths(src, dst, dist, blacklist, type)
              }
            })
        }
      }
    })
  }
  addPaths(src, dst, dist, blacklist, type) {
    const trade = type === 'sea' ? 'sea' : 'land'
    if (dist) {
      src.trade[trade].push(dst.idx)
      src.paths[trade][dst.idx] = dist
      dst.trade[trade].push(src.idx)
      dst.paths[trade][src.idx] = dist
    } else {
      blacklist[src.idx].push(dst.idx)
      blacklist[dst.idx].push(src.idx)
    }
  }
  distance([x1, x2], [y1, y2]) {
    return Math.hypot(x1 - x2, y1 - y2)
    // return Math.abs(x1 - x2) + Math.abs(y1 - y2)
  }
  shortestPath(start, end, type, f = h => h >= this.coast) {
    // initialize the priority queue to compare cell priorities
    const queue = new PriorityQueue({
      comparator: function(a, b) {
        return a.p - b.p
      }
    })
    queue.queue({
      idx: start,
      p: 0,
      d: 0
    })
    const destination = this.polygons[end]
    const totals = {}
    // visited cell list marking the next cell in the path
    const visited = []
    // total cost from a cell
    totals[start] = 0
    let prev = start
    let len = 0
    while (queue.length > 0 && prev !== end && len < this.pathing[type]) {
      // get the next item in the queue
      const { idx, d } = queue.dequeue()
      len = d + 1
      prev = idx
      const cell = this.polygons[prev]
      // consider all neighbors that pass the validation conditon (f)
      cell.n.forEach(next => {
        const neighbor = this.polygons[next]
        if (f(neighbor.h) || next === end) {
          // only consider neighbors that haven't already been visited
          if (visited[next] === undefined) {
            const [nx, ny] = [neighbor.site[0], neighbor.site[1]]
            const [cx, cy] = [cell.site[0], cell.site[1]]
            const [dx, dy] = [destination.site[0], destination.site[1]]
            // start the cost at the neighbor height and the distance between cells
            let cost = neighbor.h * 2 + this.distance([nx, cx], [ny, cy]) / 30 // NEEDS SCALING
            let mod = 1
            // prioritize roads that pass through cities
            neighbor.location === undefined && (cost += 0.1)
            // dont go through hazard states
            neighbor.hazard > 0 && (cost += 1)
            // prioritize roads that don't pass through rivers
            neighbor.river < 0 && (cost -= 0.1)
            // prioritize coastal roads
            neighbor.type === landmarks.coast && (mod *= 0.75)
            // prioritize already built roads
            neighbor.path && (mod *= 0.3)
            // finalize the cost by adding it to the total cost to get to the previous cell
            const prospect = cost * mod + totals[prev]
            totals[next] = prospect
            visited[next] = prev
            // add cell w/ priorty to the queue
            queue.queue({
              idx: next,
              p: prospect + (this.distance([nx, dx], [ny, dy]) / 15) * mod, // NEEDS SCALING
              d: len
            })
          }
        }
      })
    }
    const success = prev === end
    // get path using the visited list
    success && this.restorePath(start, end, type, visited)
    return success ? len : false
  }
  restorePath(start, end, type, visited) {
    let path = []
    let current = end
    let prev = this.polygons[current]
    // mark the path addition to the current cell
    prev.path += 1
    path.push(prev.idx)
    // go from source to end
    while (current !== start) {
      // use the visited list to get the next cell in the path
      current = visited[current]
      const curr = this.polygons[current]
      if (current === undefined) {
        console.log(
          `BAD ROUTE: ${type} | start: ${start} | end: ${end} | current: ${current}`
        )
        break
      }
      // add index to the path
      path.push(curr.idx)
      // mark the path addition to the current cell
      curr.path += 1
      prev = curr
    }
    // add path to the roads list
    path.length > 1 && this.routes[type].push(path)
  }
}
export default InfrastructureShaper
