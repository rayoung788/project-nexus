import * as d3 from 'd3'
import Shaper from './../shaper'
import NoiseGenerator from './../../utilities/noise'
import { mapValue } from '@/model/utilities/common'
import { landmarks } from './../../utilities/constants'

class ContinentShaper extends Shaper {
  constructor(world, type, climate) {
    super(world)
    // noise params
    this.params = {
      noise: NoiseGenerator.diamondSquare,
      res: 256,
      weights: [0.05, 1, 2.5, 2, 1.5]
    }
    const templates = {
      Archipelago: this.archipelago,
      Continental: this.continental
    }
    const climates = {
      Arctic: [0.1, 0.3],
      Temperate: [0.45, 0.55],
      Tropical: [0.8, 1]
    }
    this.climate = climates[climate]
    this.template = templates[type]
  }
  get pipeline() {
    return [
      {
        name: 'Template',
        action: this.setTemplate
      },
      {
        name: 'Voronoi Setup',
        action: this.setup
      },
      {
        name: 'Adjacenct Polygons',
        action: this.buildAdjacency
      },
      {
        name: 'Height Generation',
        action: this.heightgen
      },
      {
        name: 'Resolve Depressions',
        action: this.resolveDepressions
      },
      {
        name: 'Land Markers',
        action: this.landmarks
      },
      {
        name: 'Coastline',
        action: this.coastline
      }
    ]
  }
  // voronoi setup
  setup() {
    // create inital points
    let sites = d3.range(this.dim.cells).map(d => {
      return [window.dice.random * this.dim.w, window.dice.random * this.dim.h]
    })
    // create voronoi object clipped by the image width & height
    let voronoi = d3.voronoi().extent([[0, 0], [this.dim.w, this.dim.h]])
    // perform loyd relaxation to smooth voronoi cells
    let relaxedSites = voronoi(sites)
      .polygons()
      .map(d3.polygonCentroid)
    // d3.range(5).forEach(() => {
    //   relaxedSites = voronoi(relaxedSites)
    //   .polygons()
    //   .map(d3.polygonCentroid)
    // })
    this.world.sites = relaxedSites
    // finalize voronoi diagram
    this.diagram = voronoi(relaxedSites)
  }
  // get adjacent cell info
  buildAdjacency() {
    let diagram = this.diagram
    // get voronoi polygon data
    this.polygons = diagram.polygons().map((i, idx) => {
      return {
        idx: idx,
        data: i,
        site: diagram.cells[idx].site.slice(0, 2),
        h: 0,
        rain: 0,
        hazard: 0,
        flux: 0,
        path: 0,
        river: -1,
        mountain: -1,
        // find neighbors using cell edges
        n: diagram.cells[idx].halfedges
          .filter(e => {
            let edge = diagram.edges[e]
            return edge.left && edge.right
          })
          .map(e => {
            let edge = diagram.edges[e]
            let left = edge.left.index
            return left === idx ? edge.right.index : left
          })
      }
    })
  }
  setTemplate() {
    this.template()
    this.world.dimFinalize()
  }
  archipelago() {
    const dim = window.dice.choice(['h', 'w'])
    this.world._dim[dim] += 200
    this.world._dim[`r${dim}`] = 4000
    this.world._dim.rh /= 5
    this.world._dim.rw /= 5
    this.world.nRegions = 8
    this.world.nTowns = [200, 250]
    this.world.coast = 0.185
    this.world.spacing.regions = 0.05
    this.world.spacing.ocean = 0.2
    this.params = {
      noise: NoiseGenerator.simplex,
      res: 300,
      f: {
        octaves: 8,
        frequency: 0.004,
        persistence: 0.75
      },
      weights: [0.05, 1, 2.5, 2.0, 1.5],
      dist: Math.max
    }
  }
  continental() {
    const regions = 56 // 28 * this.dim.cells / 8000
    this.world.nRegions = regions
    this.world.nTowns = [regions * 16, regions * 18]
    const f = 2.5
    this.params = {
      noise: NoiseGenerator.simplex,
      res: 300,
      f: {
        octaves: 8,
        frequency: window.dice.uniform(0.0025, 0.0055),
        persistence: window.dice.uniform(0.7, 0.75)
      },
      weights: [0.05, 1, 2.5, f, f - 0.5],
      dist: Math.hypot
    }
  }
  // base heightmap generation
  heightgen() {
    // start from fractal noise
    const seed = this.world.id
    const res = this.params.res
    const elev = this.params.noise(res, this.params.f, seed)
    console.log('Noise Parms:', this.params.f)
    // convert from noise resolution to final resolution
    const convert = res / Math.max(this.dim.h, this.dim.w)
    const bound = res - 1
    // a is a flat height increase
    // b weights the distance drop
    const [a, b, c, f, g] = this.params.weights
    this.polygons.forEach(poly => {
      // manhatten distance from center
      const control = 0.5 // poly.site[1] > this.dim.h / 2 ? 0.75 : 0.25
      const d =
        2.1 *
        this.params.dist(
          Math.abs(this.dim.w * 0.5 - poly.site[0]) / this.dim.w,
          Math.abs(this.dim.h * control - poly.site[1]) / this.dim.h
        )
      // average polygon height using vertices as data points
      const e =
        poly.data.reduce((total, pt) => {
          const [y, x] = pt.map(p => Math.min(Math.round(p * convert), bound))
          return total + elev[x][y]
        }, 0) / poly.data.length
      // final height (edges should be below sea-level producing islands)
      poly.h = (1 - b * d ** c) * (a + e ** f) * g // (1 - b * d ** c) * (a + e ** 2.25) * 2.25
    })
  }
  // make sure all land cells have at least one neighbor that is lower elevation (used for rivers)
  resolveDepressions() {
    let polygons = this.polygons
    polygons.filter(p => p.h < this.coast).forEach(p => {
      p.h = this.coast - 0.0001
    })
    this.world.land = this.polygons.filter(e => {
      return e.h >= this.coast
    })
    let land = this.land
    let depression = 1
    let minCell = Infinity
    let minHigh = Infinity
    while (depression > 0) {
      depression = 0
      // process highest points first
      land.sort(this.compareHeight)
      for (var i = 0; i < land.length; i++) {
        // arbitrary impossibly high point to start
        minHigh = 10
        // check all neighbors and record the lowest neighbor
        land[i].n.forEach(e => {
          if (polygons[e].h < minHigh) {
            minHigh = polygons[e].h
            minCell = e
          }
        })
        // check if the current cell is lower than its lowest neighbor
        if (land[i].h <= polygons[minCell].h) {
          // mark the depression
          depression += 1
          // make it slightly higher than its lowest neighbor
          land[i].h = polygons[minCell].h + 0.01
        }
      }
    }
    // redistribute heights to create ~15% mountains / total land
    let mountains = this.land.filter(p => p.h > this.mountains)
    const total = this.land.length
    let percent = mountains.length / total
    let max = mountains.reduce((m, { h }) => (h > m ? h : m), 0)
    let newMax = max + window.dice.uniform(0.1, 0.2)
    while (percent < 0.15) {
      land.forEach(p => {
        p.h = mapValue([this.coast, max], [this.coast, newMax], p.h)
      })
      mountains = this.land.filter(p => p.h > this.mountains)
      max = land.reduce((m, { h }) => (h > m ? h : m), 0)
      percent = mountains.length / total
      newMax += window.dice.uniform(0.1, 0.2)
      console.log(`mountains: ${(percent * 100).toFixed(2)}%, max: ${max}`)
    }
    let min = mountains.reduce((m, { h }) => (h < m ? h : m), Infinity)
    mountains.forEach(p => {
      p.h = mapValue([min, max], [min, 1.5], p.h)
    })
  }
  // mark landscape features
  landmarks() {
    let idx = 1
    // mark water cells
    // make sure to that index #1 points to the ocean
    const top = this.diagram.find(0, 0).index
    // get all water cells
    this.world.water = this.polygons.filter(e => {
      return e.h < this.coast
    })
    let water = [this.polygons[top]]
    water = water.concat(
      this.polygons.filter(p => p.h < this.coast && p.idx !== top)
    )
    // iterate through all bodies of water
    while (water.length > 0) {
      let queue = [water[0].idx]
      // floodfill all connecting water cells to mark a body of water
      while (queue.length > 0) {
        // grab the next item in the queue
        let current = this.polygons[queue.shift()]
        // mark it with the current water feature index
        current.landmark = idx
        // mark it as a water cell
        current.type = landmarks.water
        // add neighboring water cells to the queue
        queue = queue.concat(
          current.n.filter(
            p =>
              this.polygons[p].h < this.coast &&
              !this.polygons[p].landmark &&
              !queue.includes(p)
          )
        )
      }
      // only consider cells that haven't been marked
      water = water.filter(poly => !poly.landmark)
      // increment the water feature index after a completed floodfill
      idx += 1
    }
    // mark water feature indices
    this.features.water = [1, idx]
    // mark land cells
    let land = [...this.land]
    // iterate through all islands
    while (land.length > 0) {
      let queue = [land[0].idx]
      // floodfill all connecting land cells to mark an island
      while (queue.length > 0) {
        // grab the next item in the queue
        let current = this.polygons[queue.shift()]
        // mark it with the current land feature index
        current.landmark = idx
        // count how many neighbors are water cells (used for harbor placement)
        current.harbor = current.n.filter(
          p => this.polygons[p].h < this.coast
        ).length
        // mark as coast if at least one neighbor is water, otherwise its land
        current.type = current.harbor > 0 ? landmarks.coast : landmarks.land
        // add neighboring land cells to the queue
        queue = queue.concat(
          current.n.filter(
            p =>
              this.polygons[p].h >= this.coast &&
              !this.polygons[p].landmark &&
              !queue.includes(p)
          )
        )
      }
      // only consider cells that haven't been marked
      land = land.filter(poly => !poly.landmark)
      // increment the land feature index after a completed floodfill
      idx += 1
    }
    // mark land feature indices
    this.features.land = [this.features.water[1], idx]
    // mark mountains
    // find all cells above the mountain cutoff
    let mountains = this.land.filter(p => p.h > this.mountains)
    let midx = 0
    // iterate through all mountain ranges
    while (mountains.length > 0) {
      let queue = [mountains[0].idx]
      // floodfill all connecting mountain cells to mark a mountain range
      while (queue.length > 0) {
        // grab the next item in the queue
        let current = this.polygons[queue.shift()]
        // mark it with the current mountain feature index
        current.mountain = midx
        // add neighboring mountain cells to the queue
        queue = queue.concat(
          current.n.filter(
            p =>
              this.polygons[p].h > this.mountains &&
              this.polygons[p].mountain < 0 &&
              !queue.includes(p)
          )
        )
      }
      // only consider cells that haven't been marked
      mountains = mountains.filter(poly => poly.mountain < 0)
      // increment the mountain feature index after a completed floodfill
      midx += 1
    }
    this.features.mountain = midx
  }
  // marks coastline edges
  coastline() {
    // iterate through all coastal polygons
    this.polygons.filter(p => p.type === landmarks.coast).forEach(p => {
      p.coastal = []
      // iterate through edges
      this.diagram.cells[p.idx].halfedges
        .map(e => this.diagram.edges[e])
        .filter(edge => edge.left && edge.right)
        .forEach(edge => {
          const n = edge.right.index === p.idx ? edge.left.index : edge.right.index
          const neighbor = this.polygons[n]
          // find edges with water sides
          if (neighbor.h < this.coast) {
            neighbor.shallow = true
            // mark ocean facing coasts
            p.ocean = p.ocean || neighbor.landmark === 1
            // mark edge as coastal
            edge.coast = true
            // record the water edge
            edge.water = neighbor.landmark
            // record the island edge
            edge.land = p.landmark
            // get coastal edge coordinates
            let [cx, cy] = [
              (edge[0][0] + edge[1][0]) / 2,
              (edge[0][1] + edge[1][1]) / 2
            ]
            const [dx, dy] = [cx - p.site[0], cy - p.site[1]]
            const sx = dx < 0 ? 1 : -1
            const sy = dy < 0 ? 1 : -1
            const dist = 0.5
            // add them to the coastal polygon coordinates list (used for city placement)
            p.coastal.push({
              x: cx + sx * (window.dice.random / 2 + dist),
              y: cy + sy * (window.dice.random / 2 + dist)
            })
          }
        })
    })
  }
}
export default ContinentShaper
