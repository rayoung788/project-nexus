import * as d3 from 'd3'
import Shaper from './../shaper'
import { Humanoid } from '../../entities/attributes/race'
import { landmarks, settlements } from '@/model/utilities/constants'
import Settlement from '@/model/places/cells/locations/settlement'
import nations from '@/model/entities/nations'

class CulturalShaper extends Shaper {
  get pipeline() {
    return [
      {
        name: 'Cultural Spheres',
        action: this.culturalSpheres
      },
      {
        name: 'Name Places',
        action: this.namePlaces
      },
      {
        name: 'Imperialism',
        action: this.imperialism
      },
      {
        name: 'Colonialism',
        action: this.colonialism
      },
      {
        name: 'Tribalism',
        action: this.tribalism
      },
      {
        name: 'Revolution',
        action: this.revolution
      },
      {
        name: 'Name Cities',
        action: this.defineCities
      }
    ]
  }
  culturalSpheres() {
    // use capital cities to start the culture floodfill
    const capitals = this.locations
      .filter(city => city.type === settlements.capital)
      .map(city => this.polygons[city.cell])
    // categorize by climate to get even distribution
    const count = 2
    let arctics = capitals.filter(c => {
      const region = this.regions[c.region]
      return region.climate === 'Arctic'
    })
    arctics = arctics.slice(0, arctics.length / count)
    let temperates = capitals.filter(c => {
      const region = this.regions[c.region]
      return region.climate === 'Temperate'
    })
    temperates = temperates.slice(0, temperates.length / count)
    let tropics = capitals.filter(c => {
      const region = this.regions[c.region]
      return region.climate === 'Tropical'
    })
    tropics = tropics.slice(0, tropics.length / count)
    let centers = [...arctics, ...temperates, ...tropics]
    centers.forEach(center => {
      const origin = center.region
      const region = this.regions[origin]
      // create the culture
      const culture = new Humanoid(this.cultures.length, origin, region.climate)
      this.cultures.push(culture)
      region.culture = culture
    })
    centers = centers.map(center => center.idx)
    // find regions that aren't cultural centers
    const satellites = capitals.filter(c => !centers.includes(c.idx))
    // spread origin cultures to satellite regions
    satellites.forEach(src => {
      const climate = this.regions[src.region].climate
      // find the closest center to associate with
      // penalize for entering different climate spheres
      const closest = centers.reduce(
        (res, center) => {
          const dst = this.polygons[center]
          let dist = Math.hypot(src.site[0] - dst.site[0], src.site[1] - dst.site[1])
          this.regions[dst.region].climate !== climate && (dist *= 5)
          return dist < res.min
            ? {
                cell: dst,
                min: dist
              }
            : res
        },
        {
          min: Infinity
        }
      )
      if (closest.cell) {
        const region = this.regions[src.region]
        region.culture = this.regions[closest.cell.region].culture
        // region.conquered = this.regions[closest.cell.region].idx
      }
    })
    this.cultures_groups = Object.values(this.regions).reduce((sum, r) => {
      const key = r._culture
      !sum[key] && (sum[key] = [])
      sum[key].push(r)
      return sum
    }, {})
  }
  namePlaces() {
    // all names are based on the major culture
    const land = this.land
    const total = this.polygons.length
    // islands
    d3.range(...this.features.land).forEach(i => {
      const island = land.filter(poly => poly.landmark === i)
      this.features.landmarks[i] = this.findInfluence(island, i).genName(
        'island',
        island.length / total
      )
    })
    // lakes
    const water = this.polygons.filter(poly => poly.h < this.coast)
    d3.range(...this.features.water).forEach(i => {
      const body = water.filter(poly => poly.landmark === i)
      this.features.landmarks[i] = landmarks.ocean
      if (body.length / total < 0.3) {
        this.features.landmarks[i] = this.findInfluence(body, i).genName('lake')
      }
    })
    // rivers
    d3.range(this.rivers).forEach(i => {
      const river = land.filter(poly => poly.river === i)
      if (river.length > 0) {
        const name = this.findInfluence(river, i).genName('river')
        this.features.rivers[i] = name
      }
    })
    // mountains
    d3.range(this.features.mountain).forEach(i => {
      const mountain = land.filter(poly => poly.mountain === i)
      const name = this.findInfluence(mountain, i).genName(
        'mountain',
        mountain.length
      )
      this.features.mountains[i] = name
    })
  }
  findInfluence(land, i) {
    // determine the greatest cultural influence on a group of cells
    const cultures = this.cultures.map(() => 0)
    // count the number of cells under the influence of each culture
    land.forEach(poly => {
      const culture = this.regions[poly.region]._culture
      cultures[culture] += 1
    })
    // find the culture with the highest cell count
    const influence = cultures.reduce(
      (top, c, i) => {
        return c > top.value
          ? {
              idx: i,
              value: c
            }
          : top
      },
      {
        idx: 0,
        value: -1
      }
    )
    return this.cultures[influence.idx]
  }
  imperialism() {
    // build the settlments
    const locations = this.world.locations
    this.world.locations = []
    locations.forEach(city => this.buildSettlement(city))
    // build nations
    const regions = window.dice.shuffle(Object.values(this.regions))
    let empires = ~~(regions.length * 0.07)
    let i = 0
    let powers = window.dice.shuffle(['Empire', 'Republic', 'Dynasty', 'Horde'])
    const used = []
    while (empires > 0 && i < regions.length) {
      const empire = regions[i]
      const neighbors = empire.neighbors
        .filter(n => !n.complex_unit && n.territories.length > 1)
        .sort((a, b) => {
          const amod = a._culture === empire._culture ? 0.5 : 1
          const adist = a.capital.distanceTo(empire.capital) * amod
          const bmod = b._culture === empire._culture ? 0.5 : 1
          const bdist = b.capital.distanceTo(empire.capital) * bmod
          return adist - bdist
        })
      if (
        !empire.complex_unit &&
        neighbors.length > 2 &&
        empire.territories.length > 2 &&
        !used.includes(empire._culture)
      ) {
        const strength = Math.min(window.dice.randint(3, 6), neighbors.length)
        neighbors.slice(0, strength).forEach(n => {
          n.conquered = empire.idx
        })
        this.cultures_groups[empire._culture]
          .filter(r => !r.complex_unit && r.idx !== empire.idx)
          .forEach(n => {
            n.conquered = empire.idx
          })
        const nation = powers.pop()
        if (powers.length < 1) powers = window.dice.shuffle(['Empire', 'Republic', 'Dynasty', 'Horde'])
        nations(nation, empire.culture)
        const name = empire.namePlace('region')
        empire.nation = `${name} ${nation}`
        used.push(empire._culture)
        empires--
      }
      i++
    }
  }
  center(regions) {
    return regions.reduce(
      (max, c) => {
        const dist = regions.reduce(
          (sum, r) => sum + c.capital.distanceTo(r.capital),
          0
        )
        return max[1] > dist ? [c, dist] : max
      },
      [regions[0], Infinity]
    )[0]
  }
  tribalism() {
    const tribal = ['Wilds', 'Clans', 'Tribes']
    let candidates = window.dice.shuffle(
      Object.values(this.cultures_groups).filter(
        c =>
          c.length > 1 && c.every(r => !r.complex_unit && r.territories.length > 1)
      )
    )
    candidates
      .sort((a, b) => b.length - a.length)
      .slice(0, tribal.length)
      .forEach((candidate, i) => {
        let center = this.center(candidate)
        const name = center.namePlace('region')
        center.nation = `${name} ${tribal[i]}`
        nations('Tribal', center.culture)
        candidate
          .filter(n => n.idx !== center.idx)
          .forEach(n => {
            n.conquered = center.idx
          })
      })
  }
  colonialism() {
    let candidates = window.dice.shuffle(
      Object.values(this.cultures_groups).filter(
        c => c.length > 1 && c.every(r => !r.complex_unit)
      )
    )
    candidates.forEach(c => {
      c.ocean = c.reduce((sum, r) => sum + r.ocean, 0)
    })
    candidates.sort((a, b) => b.ocean - a.ocean)
    if (candidates.length > 0) {
      const candidate = candidates[0]
      let center = this.center(candidate)
      // create the colonial culture; TODO: change origin because this is a foreign power
      center._colony = new Humanoid(this.cultures.length, center, center.climate)
      this.cultures.push(center._colony)
      const name = center.colony.genName('region')
      center.nation = `${name} Colonies`
      nations('Tribal', center.culture)
      nations('Colonial', center.colony)
      candidate
        .filter(n => n.idx !== center.idx)
        .forEach(n => {
          n.conquered = center.idx
        })
    }
  }
  revolution() {
    // geographic revolutionary split
    const candidates = window.dice.shuffle(
      Object.values(this.cultures_groups).filter(
        c =>
          c.length === 2 && c.every(r => !r.complex_unit && r.territories.length > 1)
      )
    )
    if (candidates.length > 0) {
      const [primary, secondary] = candidates[0]
      const hdiff = Math.abs(primary.capital.x - secondary.capital.x)
      const vdiff = Math.abs(primary.capital.y - secondary.capital.y)
      const [d, d1, d2] =
        vdiff > hdiff ? ['y', 'South', 'North'] : ['x', 'East', 'West']
      const name = primary.name
      const [r1, r2] =
        primary.capital[d] > secondary.capital[d]
          ? [primary, secondary]
          : [secondary, primary]
      r1.name = `${d1} ${name}`
      r2.name = `${d2} ${name}`
      r1._name = `${d1} ${name}`
      r2._name = `${d2} ${name}`
      r1.divided = true
      r2.divided = true
    }
    // all other cultures are kingdoms WE NEED TO MOVE THIS
    this.cultures
      .filter(culture => !culture.gov)
      .forEach(culture => nations('Kingdom', culture))
  }
  defineCities() {
    // go through each region and finalize the cities
    Object.values(this.regions)
      .filter(r => !r.conquered)
      .forEach(region => {
        this.centralized(region)
        if (region.territories.length < 2 && !region.conquered) {
          region.capital.type = settlements.city
          region.capital.name = `Free City of ${region._name}`
          region.capital.free = true
        }
      })
  }
  centralized(region) {
    // find all settlements in the region
    const cities = region.territories
    // find the regional captial
    const capital = region.capital
    // find all cities in the region
    const major = cities.filter(
      city => city.type === settlements.city && city.capital
    )
    const minor = cities.filter(
      city => city.type === settlements.city && !city.capital
    )
    // find all towns in the region
    const towns = cities.filter(city => city.type === settlements.town)
    // set the capital's population
    let population = region.total_population ** 0.5 * window.dice.norm(15, 1.5)
    capital.build(~~population)
    major.concat(minor).forEach(city => {
      // make each city's population some fraction of the previous city's population
      population = Math.round(population * window.dice.uniform(0.7, 0.8))
      // require that city's have at least 8000 residents
      city.build(population > 8000 ? ~~population : window.dice.randint(8000, 8500))
    })
    towns.forEach(town => {
      // randomly assign town populations
      town.build(window.dice.randint(500, 7999))
    })
  }
  buildSettlement = city => {
    city.ref = this.regions[city.region]
    // extract relavent attributes
    const { ref, x, y, type, region, cell, trade, port, paths } = city
    // build the settlement
    return new Settlement(ref, x, y, type, region, cell, trade, paths, port)
  }
}
export default CulturalShaper
