import * as d3 from 'd3'
import Shaper from './../shaper'
import { landmarks } from './../../utilities/constants'
import { mapValue, range } from '../../utilities/common'

class WeatherShaper extends Shaper {
  // order in which to display rivers
  get riverOrder() {
    return this.world.riverOrder
  }
  set riverOrder(value) {
    this.world.riverOrder = value
  }
  // river path data (display)
  get riverData() {
    return this.world.riverData
  }
  set riverData(value) {
    this.world.riverData = value
  }
  get pipeline() {
    return [
      { name: 'Temperature Generation', action: this.heatgen },
      { name: 'Wind Model', action: this.raingen },
      { name: 'Flux', action: this.flux },
      { name: 'Normal Biomes', action: this.biomeClassification },
      { name: 'Hazard Biomes', action: this.hazards },
      { name: 'World Health', action: this.health }
    ]
  }
  // latitude based temperature
  heatgen() {
    this.polygons.forEach(poly => {
      const { latitude } = this.world.gps(...poly.site)
      poly.heat = (this.world.temperature(latitude, 0, poly.h) + this.world.temperature(latitude, 6, poly.h)) / 2
    })
  }
  // randomly distribute precipitation by region
  raingen() {
    // tunes humidity generated from lakes
    const lakeEffect = 0.15
    // tunes humidity generated from the ocean
    const coastalRain = 0.2
    // iterate through all regions
    Object.values(this.regions).forEach(region => {
      // determine if region has coasts neighboring the ocean
      region.coastal = region.land
        .filter(p => p.type === landmarks.coast)
        .some(p => {
          return this.diagram.cells[p.idx].halfedges
            .map(e => this.diagram.edges[e])
            .some(e => e.water === 1)
        })
      // get all land cells in the region
      const land = this.polygons.filter(poly => poly.region === region.idx)
      // get the average regional temperature to mark climate (used for cultures)
      const heat = d3.mean(region.land.map(poly => poly.heat))
      region.heat = heat
      region.climate =
        heat < this.climates.arctics ? 'Arctic' : heat > this.climates.tropics ? 'Tropical' : 'Temperate'
      // create base regional precipitation
      let bound = [0.8, 0]
      if (heat > this.climates.tropics) {
        bound = [1, 0]
      } else if (heat >= this.climates.arctics) {
        bound = [0.8, 0.1]
      }
      const rain = window.dice.uniform(...bound)
      // distribute rain to all regional cells
      land.forEach(poly => {
        poly.rain = rain
        // always add coastal rain if applicable
        if (poly.h < this.coast) {
          poly.rain += coastalRain
          // set lake effect if precipitation is below threshold for lakes
          if (poly.landmark !== 1) {
            poly.rain += lakeEffect
          }
        }
      })
    })
    // smooth precipitation values
    d3.range(15).forEach(i => {
      // iterate through all polygons
      this.polygons.forEach(poly => {
        // get all neighbors + current cell (skipping land neighbors for lakes)
        let aggregate = poly.n
          .map(idx => this.polygons[idx])
          .filter(
            n => !(poly.h < this.coast && poly.landmark !== 1) || n.h < this.coast
          )
          .map(n => n.rain)
        aggregate.push(poly.rain)
        // compute and set the mean precipitation
        const mean = d3.mean(aggregate)
        poly.rain = mean
        // initialize flux
        poly.flux = mean
      })
    })
    // regional average rain
    Object.values(this.regions).forEach(region => {
      region.rain = region.land.reduce((sum, p) => sum + p.rain, 0) / region.land.length
    })
  }
  // flux calculations
  flux() {
    // initialize river variables
    this.rivers = 0
    this.riverData = []
    this.riverOrder = []
    let land = this.land
    // process all land cells starting at the highest points
    land.sort(this.compareHeight)
    land.forEach(poly => {
      // find the lowest neighbor
      const heights = poly.n.map(idx => {
        return this.polygons[idx].h
      })
      const min = heights.indexOf(Math.min(...heights))
      const downward = this.polygons[poly.n[min]]
      let curr = poly.river
      // check to see if the flux is high enough to start / continue a river
      if (poly.flux > 1.2) {
        // no rivers in very tall mountains or desert or cold cells
        const validSource =
          poly.h > 0.4 &&
          poly.rain >= 0.25 &&
          poly.heat >= 0.025 &&
          window.dice.random > 0.3
        // start a source if river not present
        if (poly.river < 0 && validSource) {
          // set the river index
          poly.river = this.rivers
          curr = poly.river
          this.riverOrder.push({
            r: poly.river,
            order: window.dice.random / 1000
          })
          // add this cell to the new river path
          this.riverData.push({
            river: poly.river,
            x: poly.site[0],
            y: poly.site[1],
            cell: poly.idx,
            type: 'source'
          })
          // increment the river index for the next river
          this.rivers += 1
        }
        // if this cell is a river, continue the river to the lowest neighboring cell
        if (poly.river >= 0) {
          // continue river to lowest neighbor if it is not already a river
          if (downward.river < 0) {
            downward.river = poly.river
          } else if (downward.type !== landmarks.water) {
            // determine where to branch the river
            const low = this.riverData.filter(d => d.river === downward.river).length
            const high = this.riverData.filter(d => d.river === poly.river).length
            // mark the location as a river confluence (used in city placement)
            downward.confluence = true
            // the longer river consumes the shorter one
            if (high >= low) {
              // record new river length
              this.riverOrder[poly.river].order += high
              // mark all cells from the downward river as being part of the current river
              land.filter(r => r.river === downward.river).forEach(p => {
                p.river = poly.river
              })
            } else {
              // record new river length
              this.riverOrder[downward.river].order += low
              // mark all cells from the current river as being part of the downward river
              land.filter(r => r.river === poly.river).forEach(p => {
                p.river = downward.river
              })
            }
          }
        }
      }
      // increase flux of the downward cell to simulate rainwater flow
      downward.flux += poly.flux
      // if (poly.rain * 0.97 > downward.rain) {
      //   downward.rain = poly.rain * 0.97
      // }
      if (curr >= 0) {
        const [px, py] = downward.site
        // end rivers at the coast
        if (downward.h < this.coast) {
          const sink = { x: downward.site[0], y: downward.site[1] }
          // find edges that border the downward cell and get the midpoint
          this.diagram.cells[poly.idx].halfedges
            .map(e => this.diagram.edges[e])
            .filter(
              e =>
                e.right && e.left && [e.right.idx, e.left.idx].includes(downward.idx)
            )
            .forEach(e => {
              sink.x = (e[0][0] + e[1][0]) / 2
              sink.y = (e[0][1] + e[1][1]) / 2
            })
          this.riverData.push({
            river: curr,
            x: sink.x,
            y: sink.y,
            cell: poly.idx,
            type: 'sink'
          })
        } else {
          // add downward cell coordinates to river data
          this.riverData.push({
            river: curr,
            x: px,
            y: py,
            cell: downward.idx,
            type: 'course'
          })
        }
      }
    })
  }
  // biome calculations
  biomeClassification() {
    // set then biome of each cell based on temperature and precipitation
    this.polygons.forEach(poly => {
      if (poly.h < this.coast) {
        poly.biome = 13 // water
      } else if (poly.h > this.mountains) {
        poly.biome = 12 // mountains
      } else if (poly.heat < this.climates.arctics) {
        if (poly.rain < 0.25) {
          poly.biome = 8 // polar desert
        } else if (poly.rain >= 0.25 && poly.rain < 0.5) {
          poly.biome = 9 // tundra
        } else if (poly.rain >= 0.5 && poly.rain < 0.85) {
          poly.biome = 10 // boreal forest
        } else {
          poly.biome = 11 // boreal marsh
        }
      } else if (poly.heat >= this.climates.arctics && poly.heat <= this.climates.tropics) {
        if (poly.rain < 0.25) {
          poly.biome = 4 // cold desert
        } else if (poly.rain >= 0.25 && poly.rain < 0.5) {
          poly.biome = 5 // grassland
        } else if (poly.rain >= 0.5 && poly.rain < 0.85) {
          poly.biome = 6 // deciduous forest
        } else {
          poly.biome = 7 // deciduous swamp
        }
      } else {
        if (poly.rain < 0.25) {
          poly.biome = 0 // hot desert
        } else if (poly.rain >= 0.25 && poly.rain < 0.5) {
          poly.biome = 1 // savana
        } else if (poly.rain >= 0.5 && poly.rain < 0.85) {
          poly.biome = 2 // tropical seasonal forest
        } else {
          poly.biome = 3 // tropical rainforest
        }
      }
    })
  }
  // hazard biomes
  hazards() {
    const hazards = 0
    range(hazards).forEach(() => {
      const valid = Object.values(this.regions).filter(r => !r.hazard)
      if (valid.length > 0) {
        const chosen = window.dice.choice(valid)
        chosen.hazard = chosen.rain < 0.5 ? 14 : 15
        this.polygons[chosen.capital.cell].location = undefined
        chosen.land.concat(chosen.water).forEach(p => {
          p.hazard = 0.5
          if (p.h <= this.mountains) {
            p.biome = chosen.hazard
          }
        })
      }
    })
    // smooth hazard values
    d3.range(10).forEach(() => {
      // iterate through all polygons
      this.polygons.filter(p => p.hazard > 0).forEach(poly => {
        // get all neighbors + current cell
        let aggregate = poly.n.map(idx => this.polygons[idx].hazard)
        aggregate.push(poly.hazard)
        // compute and set the mean hazard
        const mean = d3.mean(aggregate)
        poly.hazard = mean
      })
    })
  }
  health() {
    const livable = this.land.reduce((sum, p) => sum + this.biomes[p.biome].population, 0) / this.land.length
    const sustain = mapValue([25, 70], [0.3, 1], livable)
    this.world.nTowns = this.world.nTowns.map(i => i * sustain)
    console.log('Settlements: ', sustain, this.world.nTowns)
  }
}
export default WeatherShaper
