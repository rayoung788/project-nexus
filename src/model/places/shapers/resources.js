import Shaper from './../shaper'
import {
  MineralResources,
  AlchemicalResources,
  FarmingResources,
  TextileResources,
  FurResources,
  FruitResources,
  WoodResources,
  SpiceResources,
  SaltwaterResources,
  FreshwaterResources
} from '../../items/resources/types'

class ResourceShaper extends Shaper {
  constructor(world, uniques) {
    super(world)
    this.uniques = uniques
    this.factories = {
      'Hot Desert': this.desertResources,
      Savana: this.grasslandResources,
      'Tropical Seasonal Forest': this.tropicalForestResources,
      'Subtropical Rainforest': this.tropicalForestResources,
      'Cold Desert': this.desertResources,
      Grassland: this.grasslandResources,
      'Deciduous Forest': this.temperateForestResources,
      'Deciduous Swamp': this.temperateForestResources,
      'Polar Desert': this.desertResources,
      Tundra: this.grasslandResources,
      'Boreal Forest': this.arcticForestResources,
      'Boreal Marsh': this.arcticForestResources,
      Mountains: this.mountainResources
    }
  }
  get pipeline() {
    return [{ name: 'Economic Resource Generation', action: this.resourceGeneration }]
  }
  resourceGeneration() {
    this.world.resources = {}
    this.uniques.forEach(biome => {
      const key = this.biomes[biome].name
      this.world.resources[biome] = this.factories[key]()
    })
    this.world.resources[13] = this.oceanResources()
    this.world.resources[14] = this.lakeResources()
  }
  mountainResources() {
    return {
      mining: {
        common: [MineralResources.iron(), ...MineralResources.common(4)],
        rare: MineralResources.rare(2),
        gems: MineralResources.gemstone(3)
      },
      alchemical: {
        common: AlchemicalResources.common(12),
        uncommon: AlchemicalResources.uncommon(6),
        rare: AlchemicalResources.rare(3)
      }
    }
  }
  desertResources() {
    return {
      mining: {
        common: [MineralResources.iron(), ...MineralResources.common(6)],
        rare: MineralResources.rare(4)
      },
      alchemical: {
        common: AlchemicalResources.common(12),
        uncommon: AlchemicalResources.uncommon(6),
        rare: AlchemicalResources.rare(3)
      }
    }
  }
  grasslandResources() {
    return {
      farming: {
        staple: FarmingResources.staples(),
        vegtables: FarmingResources.vegtables(3),
        livestock: FarmingResources.vegtables(),
        textiles: TextileResources.grassland()
      },
      alchemical: {
        common: AlchemicalResources.common(12),
        uncommon: AlchemicalResources.uncommon(6),
        rare: AlchemicalResources.rare(3)
      }
    }
  }
  arcticForestResources() {
    return {
      farming: {
        vegtables: FarmingResources.vegtables(3),
        fur: FurResources.forest(3),
        wood: WoodResources.softwood(5)
      },
      alchemical: {
        common: AlchemicalResources.common(12),
        uncommon: AlchemicalResources.uncommon(6),
        rare: AlchemicalResources.rare(3)
      }
    }
  }
  temperateForestResources() {
    return {
      farming: {
        vegtables: FarmingResources.vegtables(3),
        fruit: FruitResources.temperate(3),
        fur: FurResources.forest(3),
        spices: SpiceResources.temperate(3),
        textiles: TextileResources.forest(),
        wood: WoodResources.hardwood(5)
      },
      alchemical: {
        common: AlchemicalResources.common(12),
        uncommon: AlchemicalResources.uncommon(6),
        rare: AlchemicalResources.rare(3)
      }
    }
  }
  tropicalForestResources() {
    return {
      farming: {
        vegtables: FarmingResources.vegtables(3),
        fruit: FruitResources.tropical(3),
        spices: SpiceResources.tropical(3),
        textiles: TextileResources.forest(),
        wood: WoodResources.hardwood(5)
      },
      alchemical: {
        common: AlchemicalResources.common(12),
        uncommon: AlchemicalResources.uncommon(6),
        rare: AlchemicalResources.rare(3)
      }
    }
  }
  oceanResources() {
    return {
      fishing: {
        common: SaltwaterResources.common(10),
        rare: SaltwaterResources.rare(5)
      }
    }
  }
  lakeResources() {
    return {
      fishing: {
        common: FreshwaterResources.common(5),
        rare: FreshwaterResources.rare(3)
      }
    }
  }
}
export default ResourceShaper
