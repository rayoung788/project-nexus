import * as d3 from 'd3'
// import {interpolateSpectral} from 'd3-scale-chromatic'
import Shaper from './../shaper'
import { mapValue } from '../../utilities/common'
import { landmarks } from '@/model/utilities/constants'

class DisplayShaper extends Shaper {
  get pipeline() {
    return [
      { name: 'Draw Coast', action: this.drawCoast },
      { name: 'Region Borders', action: this.regionBorders },
      { name: 'Draw Icons', action: this.drawIcons },
      { name: 'Draw Rivers', action: this.drawRivers },
      { name: 'Draw Roads', action: this.drawRoads },
      { name: 'Draw Maps', action: this.drawMaps }
    ]
  }
  // line function used for rivers and roads
  get d3Line() {
    const x = d3
      .scaleLinear()
      .domain([0, this.dim.w])
      .range([0, this.dim.w])
    const y = d3
      .scaleLinear()
      .domain([0, this.dim.h])
      .range([0, this.dim.h])
    return d3
      .line()
      .x(function(d) {
        return x(d.x)
      })
      .y(function(d) {
        return y(d.y)
      })
      .curve(d3.curveCatmullRom.alpha(0.1))
  }
  // line function used for coastlines and region borders
  get d3LineCoast() {
    const x = d3
      .scaleLinear()
      .domain([0, this.dim.w])
      .range([0, this.dim.w])
    const y = d3
      .scaleLinear()
      .domain([0, this.dim.h])
      .range([0, this.dim.h])
    return d3
      .line()
      .x(function(d) {
        return x(d.x)
      })
      .y(function(d) {
        return y(d.y)
      })
      .curve(d3.curveBasis)
  }
  get elevationColors() {
    return d3
      .scaleLinear()
      .domain([0, this.coast, this.mountains + 0.1, 1])
      .range(['#75C5A7', '#75C5A7', '#F9F9B0', '#F9F9B0'])
  }
  get display() {
    return this.world.display
  }
  get riverData() {
    return this.world.riverData
  }
  get riverOrder() {
    return this.world.riverOrder
  }
  // place map icons
  drawIcons() {
    // no icons on settlement cells
    // no icons on the coast
    // no icons on roads
    // no icons on rivers
    // 20% chance for no icon placement
    let valid = m =>
      m.location === undefined &&
      m.type !== landmarks.coast &&
      window.dice.random > 0.1 &&
      m.path === 0 &&
      m.river === -1
    // mountains
    const mountains = this.polygons.filter(p => p.mountain > -1)
    mountains
      .filter(m => valid(m))
      .forEach(m => {
        if (m.h > 1.18) {
          m.icon = m.n
            .map(p => this.polygons[p])
            .every(p => p.icon !== 0 && p.icon !== 1)
            ? 0
            : -1
        } else if (m.h > 1.03) {
          m.icon = m.n
            .map(p => this.polygons[p])
            .every(p => p.icon !== 0 && p.icon !== 1)
            ? 1
            : -1
        } else if (m.h > 0.89) {
          m.icon = m.n
            .map(p => this.polygons[p])
            .every(p => p.icon !== 0 && p.icon !== 1)
            ? 2
            : 3
        } else if (m.h > 0.76) {
          m.icon = m.n
            .map(p => this.polygons[p])
            .every(p => p.icon !== 0 && p.icon !== 1)
            ? 3
            : 4
        } else if (m.h > 0.63) {
          m.icon = 4
        } else {
          m.icon = 5
        }
      })
    valid = m =>
      m.location === undefined &&
      m.type !== landmarks.coast &&
      m.path === 0 &&
      m.river === -1
    // grass
    const grassBiomes = [1, 5, 9]
    const grass = this.land.filter(p => grassBiomes.includes(p.biome))
    grass
      .filter(m => valid(m))
      .forEach(m => {
        m.icon = window.dice.choice([6])
      })
    // forest
    const forestBiomes = [6, 15]
    const forest = this.land.filter(p => forestBiomes.includes(p.biome))
    forest
      .filter(m => valid(m))
      .forEach(m => {
        m.icon = window.dice.choice([11, 12, 13])
      })
    // boreal
    const borealBiomes = [10, 11]
    const boreal = this.land.filter(p => borealBiomes.includes(p.biome))
    boreal
      .filter(m => valid(m))
      .forEach(m => {
        m.icon = window.dice.choice([14, 15, 16, 17])
      })
    // swamp
    const swampBiomes = [7]
    const swamp = this.land.filter(p => swampBiomes.includes(p.biome))
    swamp
      .filter(m => valid(m))
      .forEach(m => {
        m.icon = window.dice.choice([18, 19])
      })
    // tropical
    const tropicalBiomes = [2, 3]
    const tropical = this.land.filter(p => tropicalBiomes.includes(p.biome))
    tropical
      .filter(m => valid(m))
      .forEach(m => {
        m.icon = window.dice.choice([20, 21, 22, 23])
      })
    valid = m =>
      m.location === undefined &&
      m.type !== landmarks.coast &&
      m.path === 0 &&
      window.dice.random > 0.8 &&
      m.river === -1
    // desert
    const desertBiomes = [0, 4, 8, 14]
    const desert = this.land.filter(p => desertBiomes.includes(p.biome))
    desert
      .filter(m => valid(m))
      .forEach(m => {
        m.icon = window.dice.choice([24, 25, 26, 28])
      })
  }
  drawRivers() {
    let side = 1
    this.riverOrder.sort(this.compareOrder)
    // process rivers in order of length
    this.riverOrder.forEach(river => {
      // get the path for the river
      const points = this.riverData.filter(pt => {
        return pt.river === river.r
      })
      if (points.length > 1) {
        const finalRiver = []
        // create meandering points between each set of points
        points.forEach((pt, idx) => {
          finalRiver.push(pt)
          if (idx + 1 < points.length) {
            // get the next point
            let [ex, ey] = [points[idx + 1].x, points[idx + 1].y]
            // get the current point
            const [dx, dy] = [pt.x, pt.y]
            const dist = Math.hypot(ex - dx, ey - dy)
            // meander offset
            const meander = 0.2 + window.dice.random * 0.2 + 1 / (idx + 1)
            // get the angle of the connecting line
            const angle = Math.atan2(ey - dy, ex - dx)
            if (dist > 5) {
              // place first meander point 1/3 of the way in between
              let [sx, sy] = [(dx * 2 + ex) / 3, (dy * 2 + ey) / 3]
              // place second meander point 2/3 of the way in between
              ex = (dx + ex * 2) / 3
              ey = (dy + ey * 2) / 3
              // randomly pick meander direction
              window.dice.random > 0.5 && (side *= -1)
              // add meander offset
              sx += -Math.sin(angle) * meander * side
              sy += Math.cos(angle) * meander * side
              // randomly pick meander direction
              window.dice.random > 0.6 && (side *= -1)
              // add meander offset
              ex += Math.sin(angle) * meander * side
              ey += -Math.cos(angle) * meander * side
              // add meander to the final river
              finalRiver.push({
                x: sx,
                y: sy
              })
              finalRiver.push({
                x: ex,
                y: ey
              })
            } else if (dist > 2 || points.length < 6) {
              // only add one meander point half way between
              let [mx, my] = [(dx + ex) / 2, (dy + ey) / 2]
              // add meander offset
              mx += -Math.sin(angle) * meander * side
              my += Math.cos(angle) * meander * side
              // add meander to the final river
              finalRiver.push({
                x: mx,
                y: my
              })
            }
          }
        })
        // create the curive from the paths
        const display = this.d3Line(finalRiver)
        // extract new path points
        const path = this.parsePaths(display)
        let count = 1
        for (let i = 1; i < path.length; i++) {
          let [sx, sy] = [0, 0]
          if (i === 1) {
            sx = path[0].values[0]
            sy = path[0].values[1]
          } else {
            sx = path[i - 1].values[4]
            sy = path[i - 1].values[5]
          }
          let [ex, ey] = [path[i].values[4], path[i].values[5]]
          let [xn, yn] = [ex, ey]
          // assign river width based on length
          const riverWidth = mapValue([0, 0.8], [0.01, 1], count / 50)
          // recreate curve
          let curve = ` C${path[i].values[0]}, ${path[i].values[1]}, ${
            path[i].values[2]
          }, ${path[i].values[3]}`
          count += 1
          // finalize line segment
          const segment = `M${sx},${sy}${curve},${xn},${yn}`
          // assign shadow width based on length
          const shadowWidth = Math.max(riverWidth / 3, 0.1)
          // add river segment
          this.display.rivers.push({
            d: segment,
            river: riverWidth, // stroke-width
            shadow: shadowWidth,
            q: [
              [sx, sy],
              path[i].values.slice(0, 2),
              path[i].values.slice(2),
              [xn, yn]
            ].reduce((iq, p) => {
              iq[this.quadrant(p)] = true
              return iq
            }, {all: true})
          })
        }
      }
    })
  }
  drawRoute(route, path) {
    if (path.length > 1) {
      const points = path.map(i => this.polygons[i].site)
      this.display.routes[route].push({
        ...this.postprocess(points, 'd3Line')
      })
    }
  }
  sameEdge(e1, e2) {
    return e1[0] === e2[0] && e1[1] === e2[1]
  }
  drawCoast() {
    const coast = this.diagram.edges.filter(edge => edge.coast)
    const lakes = []
    // lakes
    d3.range(...this.features.water).forEach(i => {
      // don't consider the ocean here
      if (this.features.landmarks[i] !== landmarks.ocean) {
        // get all of the lake's edges
        const group = coast.filter(e => e.water === i)
        // pick a random edge to start
        let [current, end] = group.shift()
        const ordered = [end, current]
        // loop until we arrive at the end
        while (group.length > 0) {
          let idx = 0
          // find the next edge in the segment
          for (let i = 0; i < group.length; i++) {
            const edge = group[i]
            // the next segment shares a vertex with the current segment
            if (this.sameEdge(edge[0], current) || this.sameEdge(edge[1], current)) {
              current = this.sameEdge(edge[0], current) ? edge[1] : edge[0]
              idx = i
              break
            }
          }
          // add current vertex
          ordered.push(current)
          // don't consider already visited points
          group.splice(idx, 1)
        }
        // add ordered path to the list of lakes
        lakes.push(ordered)
      }
    })
    // land (ocean)
    const islands = []
    const lakeIsles = []
    const innerlakes = new Set()
    // iterate over each island
    d3.range(...this.features.land).forEach(i => {
      // get ocean coastline edges
      let group = coast.filter(
        e => e.land === i && this.features.landmarks[e.water] === landmarks.ocean
      )
      // its a lake island if empty
      const inner = group.length < 1
      if (group.length < 1) {
        const enclosed = coast.filter(e => e.land === i)
        // find any inner lakes
        const lakes = Array.from(new Set(enclosed.map(e => e.water)))
        const prospects = lakes
          .map(l => {
            return { l, path: enclosed.filter(e => e.water === l) }
          })
          .sort((a, b) => b.path.length - a.path.length)
        group = prospects[0].path
        // mark inner lakes
        prospects.length > 1 && prospects.slice(1).forEach(p => innerlakes.add(p.l))
        // mark the lake island
        lakeIsles.push(i)
      }
      let [current, end] = group.shift()
      // pick a random edge to start
      const ordered = [end, current]
      // loop until we arrive at the end
      while (group.length > 0) {
        let idx = 0
        // find the next edge in the segment
        for (let i = 0; i < group.length; i++) {
          const edge = group[i]
          // the next segment shares a vertex with the current segment
          if (this.sameEdge(edge[0], current) || this.sameEdge(edge[1], current)) {
            current = this.sameEdge(edge[0], current) ? edge[1] : edge[0]
            idx = i
            break
          }
        }
        // add current vertex
        ordered.push(current)
        // don't consider already visited points
        group.splice(idx, 1)
      }
      // add ordered path to the list of ocean paths
      islands.push({ path: ordered, lakeIsle: inner })
    })
    // mark lake island anomolies
    this.display.lakeIsle = lakeIsles
    this.display.innerlakes = Array.from(innerlakes)
    // create lake curves
    this.display.lakes = lakes.map(path => {
      return {
        ...this.postprocess(path)
      }
    })
    // create ocean curve
    this.display.islands = islands.map(({ path, lakeIsle }) => {
      return {
        ...this.postprocess(path),
        lakeIsle
      }
    })
  }
  // Smooth region borders
  regionBorders() {
    const regions = []
    // iterate though all regions
    Object.values(this.regions)
      .filter(r => !r.conquered)
      .forEach(r => {
        // find all borders & coastline cells
        const land = r.vassals
          .map(v => this.regions[v])
          .reduce((sum, v) => sum.concat(v.land), r.land)
        const border = land.filter(p => p.border || p.type === landmarks.coast)
        const edges = []
        // find edges that border other regions or water
        border.forEach(b => {
          const owner = this.regions[b.region].agent.idx
          this.diagram.cells[b.idx].halfedges
            .filter(e => {
              let edge = this.diagram.edges[e]
              return edge.left && edge.right
            })
            .forEach(e => {
              let edge = this.diagram.edges[e]
              let left = edge.left.index
              const n = left === b.idx ? edge.right.index : left
              const guest = this.regions[this.polygons[n].region].agent.idx
              if (guest !== owner || this.polygons[n].type === landmarks.water) {
                edges.push(edge)
              }
            })
        })
        // iterate through all edge groups
        while (edges.length > 0) {
          let [current, end] = edges.shift()
          // pick a random edge to start
          const ordered = [end, current]
          // loop until we arrive at the end
          while (!this.sameEdge(end, current)) {
            let idx = 0
            // find the next edge in the segment
            for (let i = 0; i < edges.length; i++) {
              const [e1, e2] = edges[i]
              // the next segment shares a vertex with the current segment
              if (this.sameEdge(e1, current) || this.sameEdge(e2, current)) {
                current = this.sameEdge(e1, current) ? e2 : e1
                idx = i
                break
              }
            }
            // add current vertex
            ordered.push(current)
            // don't consider already visited points
            edges.splice(idx, 1)
          }
          // add ordered path to the list of region borders
          regions.push({ path: ordered, color: r.color, r: r.idx })
        }
      })
    // create border curve
    this.display.regions = regions.map(({ path, color, r }) => {
      return {
        ...this.postprocess(path),
        color,
        r
      }
    })
  }
  drawRoads() {
    // used road paths
    const used = {}
    // iterate through each road type
    Object.entries(this.routes).forEach(([route, paths]) => {
      this.display.routes[route] = []
      // draw each road segment
      paths.forEach(path => {
        let [i, k] = [0, 1]
        for (let j = 0; k < path.length; j++, k++) {
          const [src, dst] = [path[j], path[k]]
          // make sure each segment is only drawn once
          if (used[[src, dst]]) {
            this.drawRoute(route, path.slice(i, k))
            i = k
          } else {
            used[[src, dst]] = true
            used[[dst, src]] = true
          }
        }
        this.drawRoute(route, path.slice(i, k))
        // this.drawRoute(route, path.slice(i, k))
      })
    })
  }
  drawMaps() {
    // create rain buckets based on temperature
    const { s, w, e } = this.world.climate_extremes
    const coldest = (s + w) / 2
    const buffer = (e - coldest) * 0.0333
    const tropics = this.climates.tropics + buffer
    const arctics = this.climates.arctics - buffer
    const shift = (tropics - arctics) / 4
    const divisons = [coldest, arctics, arctics + shift, tropics - shift, tropics, e]
    const type = 'primary'
    const wetter = d3
      .scaleLinear()
      .domain(divisons)
      .range([
        this.biomes[11][type],
        this.biomes[11][type],
        this.biomes[7][type],
        this.biomes[7][type],
        this.biomes[3][type],
        this.biomes[3][type]
      ])
    const wet = d3
      .scaleLinear()
      .domain(divisons)
      .range([
        this.biomes[10][type],
        this.biomes[10][type],
        this.biomes[6][type],
        this.biomes[6][type],
        this.biomes[2][type],
        this.biomes[2][type]
      ])
    const dry = d3
      .scaleLinear()
      .domain(divisons)
      .range([
        this.biomes[9][type],
        this.biomes[9][type],
        this.biomes[5][type],
        this.biomes[5][type],
        this.biomes[1][type],
        this.biomes[1][type]
      ])
    const dryer = d3
      .scaleLinear()
      .domain(divisons)
      .range([
        this.biomes[8][type],
        this.biomes[8][type],
        this.biomes[4][type],
        this.biomes[4][type],
        this.biomes[0][type],
        this.biomes[0][type]
      ])
    const cache = {
      heat: {},
      rain: {},
      elev: {}
    }
    const heat_cache = heat => {
      if (!cache.heat[heat]) {
        const wwc = wetter(heat)
        const wc = wet(heat)
        const dc = dry(heat)
        const ddc = dryer(heat)
        cache.heat[heat] = d3
          .scaleLinear()
          .domain([0, 0.2, 0.3, 0.4, 0.65, 0.8, 0.9, 1])
          .range([ddc, ddc, dc, dc, wc, wc, wwc, wwc])
      }
      return cache.heat[heat]
    }
    const rain_cache = (heat, rain) => {
      const key = `${rain}|${heat}`
      if (!cache.rain[key]) {
        const cc = heat_cache(heat)
        cache.rain[key] = cc(rain)
      }
      return cache.rain[key]
    }
    // create biome colors
    const ecolors = (h, r, e) => {
      const heat = Math.round(h)
      const rain = Math.round(r * 100) / 100
      const elev = Math.round(e * 100) / 100
      const key = `${rain}|${heat}|${elev}`
      if (!cache.elev[key]) {
        const rc = rain_cache(heat, rain)
        cache.elev[key] = d3
          .scaleLinear()
          .domain([0, 0.35, 0.85, 1])
          .range([rc, rc, '#e8e8da', '#e8e8da'])(elev)
      }
      return cache.elev[key]
    }
    this.display.shape = this.polygons.map(i => {
      const region = this.regions[i.region]
      i.q = this.quadrant(i.site)
      return {
        d: 'M' + i.data.map(data => data.map(d => d.toFixed(1))).join('L') + 'Z',
        biomes: ecolors(i.heat, i.rain, i.h),
        border: i.border || i.type === landmarks.coast || i.shallow,
        borders: region.agent.color,
        land: i.type === landmarks.land || i.type === landmarks.coast || i.shallow,
        lakeIsle: this.display.lakeIsle.includes(i.landmark),
        idx: i.idx
      }
    })
  }
  quadrant([x, y]) {
    const div = this.dim.h / 2
    if (x < div && y < div) {
      return 1
    } else if (x >= div && y < div) {
      return 2
    } else if (x < div && y >= div) {
      return 3
    }
    return 4
  }
  parsePaths(paths) {
    if (!paths) {
      return []
    }
    const commands = paths.split(/(?=[LMC])/)
    return commands.map(command => {
      return {
        type: command[0],
        values: command
          .slice(1, -1)
          .split(',')
          .map(n => parseFloat(n).toFixed(1))
      }
    })
  }
  postprocess(path, type = 'd3LineCoast') {
    const d = this[type](
      path.map(([x, y]) => {
        return { x: parseFloat(x.toFixed(1)), y: parseFloat(y.toFixed(1)) }
      })
    )
    const q = d
      .split(/(?=[LMC])/)
      .map(command =>
        command
          .slice(1, -1)
          .split(',')
          .map(n => parseFloat(n))
          .reduce((sum, el, i, arr) => {
            i % 2 === 0 && sum.push([el, arr[i + 1]])
            return sum
          }, [])
      )
      .reduce((quads, group) => {
        return {
          ...quads,
          ...group.reduce((iq, p) => {
            iq[this.quadrant(p)] = true
            return iq
          }, {all: true})
        }
      }, {})
    return { d, q }
  }
  compareOrder(a, b) {
    if (a.order < b.order) return 1
    if (a.order > b.order) return -1
    return 0
  }
}
export default DisplayShaper
