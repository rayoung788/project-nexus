import * as d3 from 'd3'

class CityBlueprint {
  constructor() {
    // initial voronoi points
    this.sites = []
    // voronoi diagram
    this.diagram = {}
    // voronoi polygons
    this.polygons = []
    // voronoi dimensions and resolution
    this.dim = {
      h: 400,
      w: 400,
      cells: 200
    }
    // display attributes
    this.display = {
      shape: []
    }
    // predefined district info (placeholder)
    this.blocks = d3.range(40).map(() => {
      const area = 100
      return { area, chaos: area > 100 ? 0 : window.dice.uniform(0.1, 0.3) }
    })
    // building polygons
    this.structures = []
    // edge vertices
    this.vertices = {}
    // gate and road paths
    this.paths = []
    // initiates city build
    this.build()
  }

  get minArea() {
    return window.dice.random * 30 + 10
  }

  build() {
    // creates the general layout
    this.layout()
    // gets adjacent polygon info and edge vertices
    this.adjacency()
    // designate districts
    this.districts()
    // generate buildings
    this.buildings()
    // draw main roads
    this.roads()
    // draw display
    this.draw()
  }

  layout() {
    // place points randomly on the map
    let sites = d3.range(this.dim.cells).map(d => {
      return [window.dice.random * this.dim.w, window.dice.random * this.dim.h]
    })
    // add additional points to the center
    sites = sites.concat(
      d3.range(20).map(d => {
        return [window.dice.random * 20 + 190, window.dice.random * 20 + 190]
      })
    )
    console.log('sites:', sites.length)
    // create base voronoi function
    let voronoi = d3.voronoi().extent([[0, 0], [this.dim.w, this.dim.h]])
    // repeated loyd relaxation to smooth districts
    let relaxedSites = voronoi(sites)
      .polygons()
      .map(d3.polygonCentroid)
    d3.range(10).forEach(() => {
      relaxedSites = voronoi(relaxedSites)
        .polygons()
        .map(d3.polygonCentroid)
    })
    this.sites = relaxedSites
    // finalize voronoi diagram
    this.diagram = voronoi(relaxedSites)
  }

  adjacency() {
    let diagram = this.diagram
    // get voronoi polygon data
    this.polygons = diagram.polygons().map((i, idx) => {
      return {
        idx: idx,
        data: i,
        site: diagram.cells[idx].site.slice(0, 2),
        label: 'Exterior',
        // find neighbors using cell edges
        n: diagram.cells[idx].halfedges
          .filter(e => {
            let edge = diagram.edges[e]
            return edge.left && edge.right
          })
          .map(e => {
            let edge = diagram.edges[e]
            let left = edge.left.index
            return left === idx ? edge.right.index : left
          })
      }
    })
    // find all edge vertices
    diagram.edges.forEach(edge => this.addVertices(edge))
  }

  addVertices([e1, e2]) {
    !this.vertices[e1] && (this.vertices[e1] = {})
    !this.vertices[e2] && (this.vertices[e2] = {})
    this.vertices[e1][e2] = true
    this.vertices[e2][e1] = true
  }

  districts() {
    // district layers
    // find the center cell and mark as center of the city
    const cell = this.diagram.find(0.5 * this.dim.w, 0.5 * this.dim.h).index
    const center = this.polygons[cell]
    center.label = 'Center'
    // start labeling districts surrounding the center going outward
    let current = center.n.map(n => this.polygons[n])
    // until all district slots are filled
    let district = 0
    while (district < this.blocks.length) {
      // cells in the next layer
      let next = []
      // iterate through all cells in the current layer
      for (let j = 0; j < current.length; j++) {
        let p = current[j]
        if (p.label === 'Exterior') {
          p.label = `District #${district}`
          p.district = district
          // add unlabeled neighbors to the next layer
          next = next.concat(p.n.map(n => this.polygons[n]))
          if (++district >= this.blocks.length) {
            break
          }
        }
      }
      current = next
    }
    // collect all 'city' districts
    const districts = this.polygons.filter(p => p.label !== 'Exterior')
    districts.forEach(d => {
      // find outer wall edges of each district
      this.diagram.cells[d.idx].halfedges
        .map(e => this.diagram.edges[e])
        .filter(edge => {
          return edge.left && edge.right
        })
        .forEach(edge => {
          const n = edge.right.index === d.idx ? edge.left.index : edge.right.index
          const neighbor = this.polygons[n]
          if (neighbor.label === 'Exterior') {
            edge.wall = true
            this.vertices[edge[0]].wall = true
            this.vertices[edge[1]].wall = true
          } else {
            edge.district = true
            this.vertices[edge[0]].district = true
            this.vertices[edge[1]].district = true
          }
        })
    })
  }
  sameEdge(e1, e2) {
    return e1[0] === e2[0] && e1[1] === e2[1]
  }
  subDivide(sides, chaos = 0.0) {
    // list of new polyogns
    const blocks = []
    // the longest edge
    let longest = {
      l: 0
    }
    sides.forEach(side => {
      // get the first vertex
      const [x1, y1] = side[0]
      // get the second vertex
      const [x2, y2] = side[1]
      // get the length of the edge
      side.l = Math.hypot(x1 - x2, y1 - y2)
      // replace if longer than longest edge
      if (side.l > longest.l) {
        longest = side
      }
    })
    // first vertex of longest edge
    let [x1, y1] = longest[0]
    // second vertex of longest edge
    let [x2, y2] = longest[1]
    // random offset from midpoint (creates less organized streets)
    const r = window.dice.uniform(0.5 - chaos, 0.5 + chaos)
    // compute (semi-) midpoint delta with offset
    const [dx, dy] = [(x2 - x1) * r, (y2 - y1) * r]
    // find the 'midpoints'
    let [mx, my] = [x1 + dx, y1 + dy]
    // get the slope of the two vertices
    const slope = (y2 - y1) / (x2 - x1)
    // offset the slope randomly (creates less organized streets)
    // find the negative reciprocal to get the perpendicular slope
    const a = (-1 / slope) * window.dice.uniform(1 - chaos, 1 + chaos)
    // get the y-intercept using the new slope
    const c = my - a * mx
    // find the point that meets the perpendicular slope to split the polygon
    let [ox, oy] = [0, 0]
    let otherside = {}
    sides.forEach(side => {
      // get the points on the other side
      let [x1, y1] = side[0]
      let [x2, y2] = side[1]
      // determine the slope of the other side
      const b = (y2 - y1) / (x2 - x1)
      // get the y-intercept of the other side
      const d = y1 - b * x1
      // if the lines arent parallel
      if (a !== b && side !== longest) {
        // if the intersect point is on the other side we are done
        const ix = (d - c) / (a - b)
        const iy = a * ix + c
        const case1 = (x1 <= ix && ix <= x2) || (x1 >= ix && ix >= x2)
        const case2 = (y1 <= iy && iy <= y2) || (y1 >= iy && iy >= y2)
        if (case1 || case2) {
          ;[ox, oy] = [ix, iy]
          otherside = side
        }
      }
    })
    // add the split points to the side list
    sides.push([[mx, my], longest[0]])
    sides.push([[mx, my], longest[1]])
    sides.push([[ox, oy], otherside[0]])
    sides.push([[ox, oy], otherside[1]])
    // loop through each point on the other side
    otherside.forEach(point => {
      let [end, current] = [[mx, my], point]
      const edges = [[end, [ox, oy]], [[ox, oy], current]]
      const vertices = [end, [ox, oy], current]
      const group = sides.filter(s => s !== longest && s !== otherside)
      // start from one end and find the new polygon edges
      while (!this.sameEdge(end, current)) {
        let idx = 0
        const past = current
        for (let i = 0; i < group.length; i++) {
          const edge = group[i]
          if (this.sameEdge(edge[0], current) || this.sameEdge(edge[1], current)) {
            // the next edge connects to the current edge
            current = this.sameEdge(edge[0], current) ? edge[1] : edge[0]
            idx = i
            break
          }
        }
        edges.push([past, current])
        vertices.push(current)
        group.splice(idx, 1)
      }
      blocks.push({
        edges,
        vertices
      })
    })
    return blocks
  }
  buildings() {
    this.structures = []
    // iterate through all city districts
    this.polygons.filter(p => p.label.includes('District')).forEach(p => {
      // get all the edges of the polyogn
      const idx = p.district
      const area = this.blocks[idx].area
      const chaos = this.blocks[idx].chaos
      const sides = this.diagram.cells[p.idx].halfedges
        .filter(e => {
          let edge = this.diagram.edges[e]
          return edge.left && edge.right
        })
        .map(e => this.diagram.edges[e])
      // split the polygon into smaller polygons to make buildings
      let blocks = this.subDivide(sides, chaos)
      // building area must be below some minimum
      let buildings = blocks.filter(b => Math.abs(d3.polygonArea(b.vertices)) < area)
      let prospects = blocks.filter(b => !buildings.includes(b))
      // rescursively split buildings until they are all below the min area
      while (prospects.length > 0) {
        let build = []
        prospects.forEach(block => {
          blocks = this.subDivide(block.edges, chaos)
          buildings = buildings.concat(
            blocks.filter(b => Math.abs(d3.polygonArea(b.vertices)) < area)
          )
          build = build.concat(blocks.filter(b => !buildings.includes(b)))
        })
        prospects = build
      }
      this.structures = this.structures.concat(buildings.map(b => b.vertices))
    })
  }
  // generate gates and main roads
  roads() {
    // get all district vertices that are on the wall
    const prospects = Object.entries(this.vertices)
      .filter(([k, v]) => v.wall && v.district)
      .map(([k, v]) => k)
    // minimum space between gates
    const spacing = 100
    // use a quad tree to quickly find the closest gate
    let gateTree = d3.quadtree().extent([[0, 0], [this.dim.w, this.dim.h]])
    // loop through all prospective gates
    for (let i = 0; i < prospects.length; i++) {
      const v = JSON.parse(`[${prospects[i]}]`)
      let dist = Infinity
      // find the closest gate
      if (i > 0) {
        const closest = gateTree.find(v[0], v[1])
        dist = Math.hypot(v[0] - closest[0], v[1] - closest[1])
      }
      // set gates that are not too close to other gates
      if (dist > spacing) {
        this.vertices[v].gate = true
        gateTree.add(v)
      }
    }
    // reset gate quad tree
    gateTree = d3.quadtree().extent([[0, 0], [this.dim.w, this.dim.h]])
    // all main roads meet in the center
    const center = this.polygons.filter(p => p.label === 'Center')[0].data
    const dest = center.map(c => c.toString())
    // add all center vertices to the quad tree
    gateTree.addAll(center)
    // use gate vertices as road starting points
    const gates = Object.entries(this.vertices)
      .filter(([k, v]) => v.gate)
      .map(([k, v]) => k)
    gates.forEach(gate => {
      const v = JSON.parse(`[${gate}]`)
      // use closest center vertex as the destination
      const closest = gateTree.find(v[0], v[1])
      // initialize the path
      const path = [v]
      let next = v
      // loop until we get to the destination
      while (!dest.includes(next)) {
        const prospects = Object.keys(this.vertices[next])
          .filter(k => !['wall', 'district', 'gate'].includes(k))
          .map(k => JSON.parse(`[${k}]`))
        // greedily choose the path that gets closest to the center
        next = prospects.reduce(
          (nearest, prospect) => {
            const dist = Math.hypot(
              prospect[0] - closest[0],
              prospect[1] - closest[1]
            )
            return nearest.dist < dist ? nearest : { dist, vertex: prospect }
          },
          { dist: Infinity }
        )
        path.push(next.vertex)
        next = next.vertex.toString()
      }
      this.paths.push(path)
    })
    console.log(this.paths)
  }

  draw() {
    // draw districts
    this.display.shape = this.polygons.map((i, idx) => {
      return {
        d: 'M' + i.data.map(data => data.map(d => d.toFixed(1))).join('L') + 'Z',
        fill: i.label === 'Exterior' ? '#caccbb' : 'white'
      }
    })
    // draw buildings
    this.display.structures = this.structures.map((i, idx) => {
      const [x, y] = d3.polygonCentroid(i)
      const fill = '#caccbb'
      return {
        d: 'M' + i.join('L') + 'Z',
        fill,
        stroke: fill === 'white' ? 'white' : 'grey',
        t: `translate(${x},${y}) scale(0.9) translate(${-x},${-y})`
      }
    })
    // draw main roads
    this.display.roads = this.paths.map((i, idx) => {
      return {
        d: 'M' + i.join('L')
      }
    })
    // draw walls
    const walls = this.diagram.edges.filter(edge => edge.wall)
    this.display.walls = walls.map((i, idx) => {
      return {
        d: 'M' + i.join('L') + 'Z',
        v1: `translate(${[i[0][0], i[0][1]]})`,
        v2: `translate(${[i[1][0], i[1][1]]})`
      }
    })
    // draw gates
    const gates = Object.entries(this.vertices)
      .filter(([k, v]) => v.gate)
      .map(([k, v]) => k)
    const [h, w] = [3, 3]
    this.display.gates = gates.map((i, idx) => {
      const v = JSON.parse(`[${i}]`)
      return {
        t: `translate(${[v[0] - w / 2, v[1] - h / 2]})`
      }
    })
  }
}

export default CityBlueprint
