import { mapValue } from '../utilities/common'

class Star {
  constructor({ earth }) {
    // the amount of heat energy that the planet recieves
    this.heat = earth ? 1 : window.dice.uniform(1.1, 0.53)
    // earth-like preference
    const range = window.dice.random > 0.75 ? [0.6, 0.8] : [0, 1]
    const mass = -Math.log(1 - window.dice.uniform(...range)) / Math.log(1.35)
    this.mass = earth ? 1 : mass // mass of the sun (solar masses)
    this.solarMass = 1.9891e30 // mass of earth sun
    this.au = 1.496e8 // distance from sun to earth
    this.g = 6.674e-20 // gravitational constant
  }
  // brightness (bolometric)
  get luminosity() {
    return this.mass ** 3.5
  }
  get diameter() {
    return this.mass ** 0.74
  }
  get radius() {
    return this.diameter / 2
  }
  // orbital period (days)
  get orbital() {
    return (
      (2 * Math.PI * ((this.au * this.distance) ** 3 / (this.g * (this.solarMass * this.mass))) ** 0.5) /
      60 /
      60 /
      24
    )
  }
  // distance to planet (AU)
  get distance() {
    return (this.luminosity / this.heat) ** 0.5
  }
  get temperature() {
    return this.mass ** 0.505
  }
  get classification() {
    const mass = this.mass
    let cls = 'M'
    let subcls = [0, 0.45]
    if (mass >= 16) {
      cls = 'O'
      subcls = [16, 80]
    } else if (mass >= 2.1) {
      cls = 'B'
      subcls = [2.1, 16]
    } else if (mass >= 1.4) {
      cls = 'A'
      subcls = [1.4, 2.1]
    } else if (mass >= 1.04) {
      cls = 'F'
      subcls = [1.04, 1.4]
    } else if (mass >= 0.8) {
      cls = 'G'
      subcls = [0.8, 1.04]
    } else if (mass >= 0.8) {
      cls = 'K'
      subcls = [0.45, 0.8]
    }
    return `${cls}${Math.round(mapValue(subcls, [0, 9], mass))}`
  }
}

export default Star
