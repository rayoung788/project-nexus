import { settlements } from '../utilities/constants'
// data structure to propagate attribute changes upward
const propagator = (ctx, attribute, influence) =>
  new Proxy(
    {},
    {
      get: (target, name) => (name in target ? target[name] : 0),
      set: (target, name, value) => {
        const old = target[name] || 0
        const diff = value - old
        ctx.ref && (ctx.ref[attribute][name] += diff * ctx.relative(influence))
        target[name] = value
        return true
      }
    }
  )
// base cell object used to share common context variables between cells
class Cell {
  constructor(ref = undefined) {
    // default cell name
    this.name = this.constructor.name.split(/(?=[A-Z])/).join(' ')
    // reference to parent cell
    this.ref = ref
    // culture & biome distributions
    this.distributions = {
      culture: {},
      origin: {},
      majority: {}
    }
    this.cultureDist = propagator(this, 'cultureDist', 'population')
    this.biomeDist = propagator(this, 'biomeDist', 'size')
    // entity population
    this._population = 0
    // cell value
    this._value = 0
    // land cells for the particular context
    this.land = []
    // water cells for the particular context
    this.water = []
  }
  get size() {
    return this.land.length
  }
  // total population
  get population() {
    return this._population
  }
  set population(value) {
    const diff = value - this._population
    this.ref && (this.ref.population += diff)
    this._population = value
  }
  // total value
  get value() {
    return this._value
  }
  set value(value) {
    const diff = value - this._value
    this.ref && (this.ref.value += diff)
    this._value = value
  }
  // map dimensions
  get dim() {
    return this.ref.dim
  }
  // biome mapping (idx -> biome information)
  get biomes() {
    return this.ref.biomes
  }
  // location array (idx -> location information)
  get locations() {
    return this.ref.locations
  }
  // returns all settlement class locations
  get settlements() {
    return this.locations.filter(l => Object.values(settlements).includes(l.type))
  }
  // culture mapping (idx -> culture information)
  get cultures() {
    return this.ref.cultures
  }
  // npc generation factory
  get npcgen() {
    return this.ref.npcgen
  }
  // unique places name list
  get places() {
    return this.ref.places
  }
  // landscape features (continents, mountains, rivers, lakes, etc.)
  get features() {
    return this.ref.features
  }
  // voronoi polygon list
  get polygons() {
    return this.ref.polygons
  }
  // region list
  get regions() {
    return this.ref.regions
  }
  // base culture distribution
  get normal() {
    return this.ref.normal
  }
  // minority culture distribtuion
  get minority() {
    return this.ref.minority
  }
  // majority culture distribtuion
  get majority() {
    return this.ref.majority
  }
  // gets the origin regional culture distribution
  get origin() {
    return this.ref.origin
  }
  // gets world context
  get world() {
    return this.ref.world
  }
  relative(trait) {
    return this[trait] / this.ref[trait]
  }
  // selects a culture from a given distribtuion
  pickCulture(dist = this.normal) {
    const culture = window.dice.weightedChoice(
      Object.keys(dist),
      Object.values(dist)
    )
    return this.cultures[culture]
  }
  // generates a unique place name
  namePlace(key, ...args) {
    const culture = this.culture || this.pickCulture(this.majority)
    let name = culture.genName(key, ...args)
    // recursively run until a unqiue name is found
    this.places.has(name) && (name = this.namePlace(key, ...args))
    this.places.add(name)
    return name
  }
  toString() {
    return this.name
  }
}

export default Cell
