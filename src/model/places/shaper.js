import { profile } from '../utilities/common'
import * as d3 from 'd3'
import { settlements } from '@/model/utilities/constants'

class Shaper {
  constructor(world) {
    // world reference to be built
    this.world = world
    this.climates = {
      tropics: 20,
      arctics: 3
    }
  }
  // world voronoi diagram
  get diagram() {
    return this.world.diagram
  }
  set diagram(value) {
    this.world.diagram = value
  }
  // world voronoi polygons
  get polygons() {
    return this.world.polygons
  }
  set polygons(value) {
    this.world.polygons = value
  }
  // world dimensions
  get dim() {
    return this.world.dim
  }
  // world land polygons
  get land() {
    return this.world.land
  }
  // world sea-level cutoff
  get coast() {
    return this.world.coast
  }
  set coast(value) {
    this.world.coast = value
  }
  // world mountain cutoff
  get mountains() {
    return this.world.mountains
  }
  // world landscape features
  get features() {
    return this.world.features
  }
  // world location list
  get locations() {
    return this.world.locations
  }
  // world region list
  get regions() {
    return this.world.regions
  }
  // world culture list
  get cultures() {
    return this.world.cultures
  }
  // world biome list
  get biomes() {
    return this.world.biomes
  }
  // world roads
  get routes() {
    return this.world.routes
  }
  // world river index
  get rivers() {
    return this.world.rivers
  }
  set rivers(value) {
    this.world.rivers = value
  }
  // world spacing (cities & roads)
  get spacing() {
    return this.world.spacing
  }
  // pipeline for shaper actions
  get pipeline() {
    return []
  }
  build() {
    // iterate and run each action in the pipeline with profiling
    this.pipeline.forEach(({ name, action }) => {
      const bound = action.bind(this)
      profile(name, () => {
        bound()
      })
    })
  }
  // make sure all land cells have at least one neighbor that is lower elevation (used for rivers)
  resolveDepressions() {
    let polygons = this.polygons
    let land = this.polygons.filter(p => p.h >= this.coast)
    let depression = 1
    while (depression > 0) {
      depression = 0
      // process highest points first
      land.sort(this.compareHeight)
      for (var i = 0; i < land.length; i++) {
        // find the lowest neighbor
        const { idx } = land[i].n
          .map(e => polygons[e])
          .reduce((lowest, e) => (e.h < lowest.h ? e : lowest), { h: Infinity })
        // check if the current cell is lower than its lowest neighbor
        if (land[i].h <= polygons[idx].h) {
          // mark the depression
          depression += 1
          // make it slightly higher than its lowest neighbor
          land[i].h = polygons[idx].h + 0.01
        }
      }
    }
  }
  // place cities on the map given spacing constraints
  placeCities(count, type, spacing) {
    // save the old city length
    const old = this.locations.length
    // create a quad tree to quickly find the nearest city
    const cityTree = d3.quadtree().extent([[0, 0], [this.dim.w, this.dim.h]])
    // add existing locations to the quad tree
    this.locations.forEach(city => {
      cityTree.add([city.x, city.y])
    })
    // sort land polygons by score (and remove edge polygons from consideration)
    let land = this.land
      .sort(this.compareScore)
      .filter(
        poly =>
          poly.site[0] > spacing() &&
          poly.site[0] < this.dim.w - spacing() &&
          poly.site[1] > spacing() &&
          poly.site[1] < this.dim.h - spacing() &&
          poly.location === undefined &&
          !(settlements.capital === type && poly.h >= this.mountains)
      )
    // place cities by itertating through the best locations first
    for (let i = 0; i < land.length && this.locations.length < count + old; i++) {
      const poly = land[i]
      let dist = Infinity
      if (i > 0) {
        // figure out if the location is too close to a nearby city
        const closest = cityTree.find(poly.site[0], poly.site[1])
        dist = Math.hypot(poly.site[0] - closest[0], poly.site[1] - closest[1])
      }
      if (dist >= spacing()) {
        // if its valid, spawn the city
        let city = {
          x: poly.site[0],
          y: poly.site[1],
          region: poly.region,
          type: type,
          cell: poly.idx,
          idx: this.locations.length,
          name: window.dice.generateId(6),
          trade: { sea: [], land: [] },
          paths: { sea: {}, land: {} }
        }
        // check if the city should have a port
        if (poly.harbor === 1) {
          city.port = true
          // move city to the coast
          const { x, y } = window.dice.choice(poly.coastal)
          city.x = x
          city.y = y
        }
        // check if the city is on a river polygon
        if (poly.river >= 0) {
          // move city away from river
          const shift = Math.max(Math.min(~~(poly.flux * 0.2), 1), 0.2)
          city.x += shift - window.dice.random
          city.y += shift - window.dice.random
        }
        // add city to location list
        this.locations.push(city)
        // add city to quad tree
        cityTree.add([city.x, city.y])
        // set the polygon location to the city's location index
        poly.location = this.locations.length - 1
        // set the polygon site to the city location (if moved)
        poly.site = [city.x, city.y]
      }
    }
    // report if it was unable to place all the cities
    if (this.locations.length < count + old) {
      console.log(
        `placement failure detected: ${type}; placed: ${this.locations.length}`
      )
    }
  }
  compareHeight(a, b) {
    if (a.h < b.h) return 1
    if (a.h > b.h) return -1
    return 0
  }
  compareScore(a, b) {
    if (a.score < b.score) return 1
    if (a.score > b.score) return -1
    return 0
  }
}

export default Shaper
