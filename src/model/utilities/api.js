import axios from 'axios'
const host = 'http://192.168.1.74:5555/' // 'http://172.0.113.77:5555/'
// const im = 'http://localhost:5556/'

const spawn = params => {
  return axios.post(`${host}create`).then(result => {
    return result.data
  })
}

export default {
  spawn,
  host
}
