import m0 from '@/assets/mountains/0.png'
import m1 from '@/assets/mountains/1.png'
import m2 from '@/assets/mountains/2.png'
import m3 from '@/assets/mountains/3.png'
import m4 from '@/assets/mountains/4.png'
import m5 from '@/assets/mountains/5.png'
import g0 from '@/assets/plants/g0.png'
import g1 from '@/assets/plants/g1.png'
import g2 from '@/assets/plants/g2.png'
import g3 from '@/assets/plants/g3.png'
import t2 from '@/assets/plants/t2.png'
import t3 from '@/assets/plants/t3.png'
import t4 from '@/assets/plants/t4.png'
import t5 from '@/assets/plants/t5.png'
import t6 from '@/assets/plants/t6.png'
import t7 from '@/assets/plants/t7.png'
import t8 from '@/assets/plants/t8.png'
import t9 from '@/assets/plants/t9.png'
// import t11 from '@/assets/plants/t11.png'
// import t12 from '@/assets/plants/t12.png'
// import t13 from '@/assets/plants/t13.png'
// import t14 from '@/assets/plants/t14.png'
import t15 from '@/assets/plants/t15.png'
import t16 from '@/assets/plants/t16.png'
import s0 from '@/assets/plants/s0.png'
import d0 from '@/assets/desert/d0.png'
import d1 from '@/assets/desert/d1.png'
import d2 from '@/assets/desert/d2.png'
import d4 from '@/assets/desert/d4.png'
import pine1 from '@/assets/plants/pine1.png'

const landmarks = {
  water: 'Water',
  land: 'Land',
  coast: 'Coast',
  ocean: 'The Great Sea'
}

const settlements = {
  city: 'City',
  town: 'Town',
  capital: 'Capital'
}

const icons = {
  // mountains
  0: { w: 127 / 10, h: 65 / 10, d: m0 },
  1: { w: 96 / 12, h: 61 / 12, d: m1 },
  2: { w: 93 / 19, h: 49 / 19, d: m2 },
  3: { w: 5.46, h: 4.69, d: m3 },
  4: { w: 4.21, h: 2.79, d: m4 },
  5: { w: 3, h: 1.73, d: m5 },
  // grass
  6: { w: 96 / 15, h: 109 / 20, d: g0 },
  7: { w: 90 / 20, h: 100 / 20, d: g1 },
  8: { w: 100 / 20, h: 97 / 20, d: g2 },
  9: { w: 93 / 20, h: 102 / 20, d: g3 },
  // forest
  10: { w: 45 / 20, h: 40 / 20, d: t6 },
  11: { w: 93 / 20, h: 95 / 20, d: t7 },
  12: { w: 85 / 20, h: 98 / 20, d: t8 },
  13: { w: 90 / 20, h: 91 / 20, d: t9 },
  // boreal
  14: { w: 89 / 20, h: 95 / 20, d: pine1 },
  15: { w: 97 / 20, h: 103 / 20, d: pine1 },
  16: { w: 87 / 20, h: 102 / 20, d: pine1 },
  17: { w: 94 / 20, h: 104 / 20, d: pine1 },
  // swamp
  18: { w: 95 / 20, h: 111 / 20, d: t15 },
  19: { w: 93 / 20, h: 95 / 20, d: t16 },
  // tropical
  20: { w: 93 / 20, h: 94 / 20, d: t2 },
  21: { w: 93 / 20, h: 95 / 20, d: t3 },
  22: { w: 85 / 20, h: 97 / 20, d: t4 },
  23: { w: 90 / 20, h: 94 / 20, d: t5 },
  // desert
  24: { w: 92 / 20, h: 30 / 20, d: d0 },
  25: { w: 84 / 20, h: 39 / 20, d: d1 },
  26: { w: 78 / 20, h: 34 / 20, d: d2 },
  28: { w: 59 / 20, h: 21 / 20, d: d4 },
  // single
  29: { w: 17 / 10, h: 20 / 10, d: s0 }
}

export { landmarks, settlements, icons }
