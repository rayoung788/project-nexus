import SimplexNoise from 'simplex-noise'
import { mapValue } from '@/model/utilities/common'

class DiamondSquare {
  constructor(res, f) {
    this.size = res
    this.roughness = f
    this.elev = Array(this.size + 1)
      .fill()
      .map(() => Array(this.size + 1).fill(0))
  }

  distort(value) {
    const max = value / (this.size * 2) * this.roughness
    return (window.dice.random - 0.5) * max
  }

  normalize(value) {
    return value > 1 ? 1 : value < -1 ? -1 : value
  }

  finalize(x, y, step) {
    const distortion = this.elev[x][y] + this.distort(step)
    this.elev[x][y] = this.normalize(distortion)
  }

  init() {
    let step = this.size
    this.elev[0][0] = window.dice.random * 2 - 1
    this.elev[0][step] = window.dice.random * 2 - 1
    this.elev[step][step] = window.dice.random * 2 - 1
    this.elev[step][0] = window.dice.random * 2 - 1
  }

  gen() {
    this.init()
    let step = this.size
    while (step > 1) {
      for (let i = 0; i < this.size; i += step) {
        for (let j = 0; j < this.size; j += step) {
          const [x, y] = [i + step / 2, j + step / 2]
          const tl = this.elev[i][j]
          const tr = this.elev[i][j + step]
          const br = this.elev[i + step][j + step]
          const bl = this.elev[i + step][j]
          this.elev[x][y] = (tr + tl + br + bl) / 4
          this.finalize(x, y, step)

          const c = this.elev[x][y]
          if (x - step >= 0) {
            this.elev[x - step / 2][y] = (tl + tr + c + this.elev[x - step][y]) / 4
          } else {
            this.elev[x - step / 2][y] = (tl + tr + c) / 3
          }
          this.finalize(x - step / 2, y, step)

          if (y + step <= this.size) {
            this.elev[x][y + step / 2] = (tr + br + c + this.elev[x][y + step]) / 4
          } else {
            this.elev[x][y + step / 2] = (tr + br + c) / 3
          }
          this.finalize(x, y + step / 2, step)

          if (x + step <= this.size) {
            this.elev[x + step / 2][y] = (bl + br + c + this.elev[x + step][y]) / 4
          } else {
            this.elev[x + step / 2][y] = (bl + br + c) / 3
          }
          this.finalize(x + step / 2, y, step)

          if (y - step >= 0) {
            this.elev[x][y - step / 2] = (bl + tl + c + this.elev[x][y - step]) / 4
          } else {
            this.elev[x][y - step / 2] = (bl + tl + c) / 3
          }
          this.finalize(x, y - step / 2, step)
        }
      }
      step /= 2
    }
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.size; j++) {
        this.elev[i][j] = Math.abs(this.elev[i][j]) * 0.8
      }
    }
    return this.elev
  }
}

class Simplex {
  constructor(res, params) {
    this.res = res
    this.params = params
  }
  noise(i, j, { octaves, frequency, persistence, n }) {
    let [amp, f, maxValue, res] = [1, frequency, 0, 0]
    for (let k = 1; k <= octaves; k++) {
      res += n.noise2D(i * f, j * f) * amp
      maxValue += amp
      f *= 2
      amp *= persistence
    }
    return res / maxValue
  }
  gen(simplex) {
    const size = this.res
    const data = Array(size + 1)
      .fill()
      .map(() => Array(size + 1).fill(0))
    let [high, low] = [-Infinity, Infinity]
    for (let i = 0; i <= size; i++) {
      for (let j = 0; j <= size; j++) {
        data[i][j] = this.noise(i, j, simplex)
        data[i][j] = this.noise(i + data[i][j], j + data[i][j], simplex)
        high = data[i][j] > high ? data[i][j] : high
        low = data[i][j] < low ? data[i][j] : low
      }
    }
    for (let i = 0; i <= size; i++) {
      for (let j = 0; j <= size; j++) {
        data[i][j] = simplex.mod(mapValue([low, high], [-1, 1], data[i][j]))
      }
    }
    return data
  }
  billow(seed) {
    const template = {
      n: new SimplexNoise(seed),
      mod: (d) => Math.abs(d),
      ...this.params
    }
    return this.gen(template)
  }
}

class NoiseGenerator {
  static diamondSquare(res, f) {
    return new DiamondSquare(res, f).gen()
  }
  static simplex(res, f, seed) {
    return new Simplex(res, f).billow(seed)
  }
}

export default NoiseGenerator
