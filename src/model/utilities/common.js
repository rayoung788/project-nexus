const variants = (a, b, sep = ' ') => {
  return a.reduce((combined, left) => {
    return [...combined, ...b.map(right => `${left}${sep}${right}`)]
  }, [])
}

const bootstrap = (keys, values) => {
  return keys.reduce((result, key, i) => {
    result[key] = values[i]
    return result
  }, {})
}

const title = str => {
  return str.replace(/[^\s-]+/g, txt => {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}

const flatten = (a, b) => [].concat(...a.map(d => b.map(e => [].concat(d, e))))
const cartesian = (a, b, ...c) => (b ? cartesian(flatten(a, b), ...c) : a)

const sum = (arr, init = 0) => {
  return arr.reduce((sum, v) => sum + v, init)
}

const normalize = data => {
  const [keys, values] = [Object.keys(data), Object.values(data)]
  const total = sum(values, 0)
  return bootstrap(keys, values.map(v => v / total))
}

const piePrep = ([labels, data, colors]) => {
  return {
    labels: labels,
    datasets: [
      {
        data: data,
        backgroundColor: colors
      }
    ]
  }
}

const range = n => {
  !Number.isInteger(n) && (n = Math.round(n))
  return [...Array(Math.max(n, 0)).keys()]
}

const distance = ([x1, y1], [x2, y2], [sx = 1, sy = 1]) => {
  return Math.hypot((x1 - x2) * sx, (y1 - y2) * sy)
}

const findIndices = (str, c) => {
  var indices = []
  for (var i = 0; i < str.length; i++) {
    if (str[i] === c) indices.push(i)
  }
  return indices
}

const mapValue = ([omin, omax], [nmin, nmax], v) => {
  const ospan = omax - omin
  const nspan = nmax - nmin
  return (v - omin) * nspan / ospan + nmin
}

const romanize = num => {
  const lookup = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1
  }
  let roman = ''
  let i
  for (i in lookup) {
    while (num >= lookup[i]) {
      roman += i
      num -= lookup[i]
    }
  }
  return roman
}

const profile = (name, f) => {
  console.time(name)
  let result = f()
  console.timeEnd(name)
  return result
}

function* combinations(head, ...tail) {
  let remainder = tail.length ? combinations(...tail) : [[]]
  for (let r of remainder) for (let h of head) yield [h, ...r]
}

class DefaultDict {
  constructor(defaultVal) {
    return new Proxy({}, {
      get: (target, name) => (name in target ? target[name] : defaultVal)
    })
  }
}

const mapTier = ([min, max]) => {
  return tier => {
    return mapValue([0, 6], [min, max], tier)
  }
}

const radians = d => d * Math.PI / 180
const degrees = r => r * 180 / Math.PI

export {
  variants,
  bootstrap,
  title,
  normalize,
  sum,
  piePrep,
  range,
  distance,
  findIndices,
  romanize,
  mapTier,
  mapValue,
  profile,
  combinations,
  cartesian,
  DefaultDict,
  degrees,
  radians
}
