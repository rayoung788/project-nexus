import serialijse from 'serialijse'

import World from '@/model/places/cells/vor'
import Region from '@/model/places/cells/region'
import ForeignDistrict from '@/model/places/cells/locations/districts/foreign'
import MagicDistrict from '@/model/places/cells/locations/districts/magic'
import MilitaryDistrict from '@/model/places/cells/locations/districts/military'
import TempleDistrict from '@/model/places/cells/locations/districts/temple'
import { SlumDistrict, MixedSlumDistrict } from '@/model/places/cells/locations/districts/slum'
import HarborDistrict from '@/model/places/cells/locations/districts/harbor'
import { TradeDistrict } from '@/model/places/cells/locations/districts/trade'
import {
  ResidentialDistrict,
  MixedResidentialDistrict
} from '@/model/places/cells/locations/districts/residential'
import {
  NobleDistrict,
  LordsDistrict,
  RoyalDistrict
} from '@/model/places/cells/locations/districts/noble'
import { Inn, ForeignInn, NobleInn } from '@/model/places/cells/locations/dwellings/inn'
import { Shop, ForeignShop } from '@/model/places/cells/locations/dwellings/shop'
import {
  Embassy,
  RoyalCourt,
  HighCourt,
  Court,
  TownCourt
} from '@/model/places/cells/locations/dwellings/government'
import { GuardBarracks, ArmyBarracks } from '@/model/places/cells/locations/dwellings/barracks'
import {
  MageGuild,
  TemplarGuild,
  EmeraldCircle,
  MercenaryGuild,
  Syndicate
} from '@/model/places/cells/locations/dwellings/guild'
import {
  CommonHouse,
  ForeignCommonHouse,
  NobleHouse
} from '@/model/places/cells/locations/dwellings/residential'
import Ship from '@/model/places/cells/locations/dwellings/ship'
import { Temple, ForeignTemple } from '@/model/places/cells/locations/dwellings/temple'
import NPC from '@/model/entities/npc'
import NPCRegistry from '@/model/entities/registry'
import Ancestry from '@/model/entities/attributes/ancestry'
import Diagram from 'd3-voronoi/src/Diagram'
import Inventory from './../entities/attributes/Inventory'

// serialijse line 160
// if (propertyName === '_meta') {
//   return false;
// }

const serialize = () => {
  serialijse.declarePersistable(Set)
  serialijse.declarePersistable(Diagram)

  serialijse.declarePersistable(NPC)
  serialijse.declarePersistable(Inventory)

  serialijse.declarePersistable(World)
  serialijse.declarePersistable(Region)

  serialijse.declarePersistable(ForeignDistrict)
  serialijse.declarePersistable(MagicDistrict)
  serialijse.declarePersistable(MilitaryDistrict)
  serialijse.declarePersistable(TempleDistrict)
  serialijse.declarePersistable(SlumDistrict)
  serialijse.declarePersistable(MixedSlumDistrict)
  serialijse.declarePersistable(HarborDistrict)
  serialijse.declarePersistable(TradeDistrict)
  serialijse.declarePersistable(ResidentialDistrict)
  serialijse.declarePersistable(MixedResidentialDistrict)
  serialijse.declarePersistable(NobleDistrict)
  serialijse.declarePersistable(LordsDistrict)
  serialijse.declarePersistable(RoyalDistrict)

  serialijse.declarePersistable(Inn)
  serialijse.declarePersistable(ForeignInn)
  serialijse.declarePersistable(NobleInn)
  serialijse.declarePersistable(Shop)
  serialijse.declarePersistable(ForeignShop)
  serialijse.declarePersistable(Embassy)
  serialijse.declarePersistable(Court)
  serialijse.declarePersistable(RoyalCourt)
  serialijse.declarePersistable(HighCourt)
  serialijse.declarePersistable(TownCourt)
  serialijse.declarePersistable(ArmyBarracks)
  serialijse.declarePersistable(GuardBarracks)
  serialijse.declarePersistable(MageGuild)
  serialijse.declarePersistable(TemplarGuild)
  serialijse.declarePersistable(EmeraldCircle)
  serialijse.declarePersistable(MercenaryGuild)
  serialijse.declarePersistable(Syndicate)
  serialijse.declarePersistable(CommonHouse)
  serialijse.declarePersistable(ForeignCommonHouse)
  serialijse.declarePersistable(NobleHouse)
  serialijse.declarePersistable(Ship)
  serialijse.declarePersistable(Temple)
  serialijse.declarePersistable(ForeignTemple)

  serialijse.declarePersistable(NPCRegistry)
  serialijse.declarePersistable(Ancestry)
  return serialijse
}

export default serialize
