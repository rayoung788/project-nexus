const generateId = () => {
  return Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString(36)
}

class Dice {
  constructor(seed) {
    this.seed = parseInt(seed, 36)
  }
  // https://gist.github.com/blixt/f17b47c62508be59987b
  get random() {
    this.seed = Math.imul(16807, this.seed) | 0 % 2147483647
    return (this.seed & 2147483647) / 2147483648
  }
  // excluding max
  uniform(min = 0, max = 1) {
    return this.random * (max - min) + min
  }
  // including max
  randint(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(this.random * (max - min + 1)) + min
  }
  // https://en.wikipedia.org/wiki/Marsaglia_polar_method
  norm(mean, dev) {
    if (this.spare) {
      const s = this.spare
      delete this.spare
      return s * dev + mean
    }
    let s, u, v
    do {
      u = this.uniform(-1, 1)
      v = this.uniform(-1, 1)
      s = u * u + v * v
    } while (s >= 1)
    s = Math.sqrt((-2 * Math.log(s)) / s)
    this.spare = s * v
    return s * u * dev + mean
  }

  choice(arr) {
    return arr[~~(this.random * arr.length)]
  }

  weightedChoice(arr, weights) {
    const rng = this.random
    // normalized between 0 and 1
    const total = weights.reduce((sum, e) => sum + e)
    const normed = weights.map(e => e / total)
    let sum = 0
    for (let i = 0; i < normed.length; i++) {
      sum += normed[i]
      if (rng <= sum) return arr[i]
    }
  }

  shuffle(arr) {
    return arr
      .map(a => [this.random, a])
      .sort((a, b) => a[0] - b[0])
      .map(a => a[1])
  }

  sample(arr, cnt) {
    return this.shuffle(arr).slice(0, cnt)
  }
  // random string a-z0-9
  generateId() {
    return Math.floor(this.random * Number.MAX_SAFE_INTEGER).toString(36)
  }

  colorCascade() {
    const color = this.randint(0, 360)
    const saturation = this.randint(30, 60)
    const lum = this.randint(20, 80)
    return {
      start: `hsl(${color}, ${saturation}%, ${lum}%)`,
      mid: `hsl(${color}, ${saturation}%, 80%)`,
      end: `hsl(${color}, ${saturation}%, 80%)`
    }
  }

  randomColors() {
    const light = [
      this.uniform(153, 250),
      this.uniform(153, 250),
      this.uniform(153, 250)
    ].map(c => ~~c)
    const dark = [
      this.uniform(0, 153),
      this.uniform(0, 153),
      this.uniform(0, 153)
    ].map(c => ~~c)
    return [
      `rgb(${light[0]}, ${light[1]}, ${light[2]})`,
      `rgb(${dark[0]}, ${dark[1]}, ${dark[2]})`
    ]
  }
}

export { generateId, Dice }
