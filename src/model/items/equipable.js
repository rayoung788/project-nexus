import Item from './item'

import { title } from '@/model/utilities/common'

class Equipable extends Item {
  constructor({ tier = 1 }) {
    super()
    this.tier = Math.max(0, tier + window.dice.norm(0, 0.2))
    this.subtype = ''
    this.slot = ''
    this.stats = {}
  }
  get description() {
    const core = ['recovery', 'health', 'intensity']
    return Object.entries(this.stats).map(([attr, val]) => {
      const percentage = !core.includes(attr)
      percentage && (val *= 100)
      const units = percentage ? '%' : 'points'
      return `${title(attr)}: ${val.toFixed(2)} ${units}`
    })
  }
  equip(inventory) {
    this.source = inventory.owner
    this.source.drop(this)
    inventory.equipment[this.type][this.slot].name &&
      inventory.equipment[this.type][this.slot].unequip(inventory)
    inventory.equipment[this.type][this.slot] = this
    const attributes = this.source.attributes.stats
    Object.entries(this.stats).forEach(([attribute, value]) => {
      attributes[attribute] += value
    })
  }
  unequip(inventory) {
    const attributes = this.source.attributes.stats
    Object.entries(this.stats).forEach(([attribute, value]) => {
      attributes[attribute] -= value
    })
    inventory.equipment[this.type][this.slot] = { tier: 0 }
    this.source.add(this)
    this.source = {}
  }
}

export default Equipable
