class Item {
  constructor() {
    this._name = this.constructor.name.split(/(?=[A-Z])/).join(' ')
    this.id = window.dice.generateId()
    this.type = 'other'
    this.source = {}
  }
  get value() {
    return 0
  }
  get weight() {
    return 0
  }
  get name() {
    return this._name
  }
  get identifier() {
    return `${this.name}: ${this.id}`
  }
  get description() {
    return ['wip']
  }
  addTo(inventory) {
    const result = { ...inventory.stash[this.type] }
    result[this.identifier] = this
    inventory.stash[this.type] = result
  }
  removeFrom(inventory) {
    const result = { ...inventory.stash[this.type] }
    delete result[this.identifier]
    inventory.stash[this.type] = result
    return this
  }
  toString() {
    return this.name
  }
}

export default Item
