import { romanize } from '@/model/utilities/common'
import { affix } from '../../affixes'
import Equipable from '../../equipable'

class Skill extends Equipable {
  constructor({ tier, subtype }) {
    super({ tier, subtype })
    this.type = 'skills'
    this.subtype = subtype
    this.flavor = {}
    this.stats = Object.entries(this.traits).reduce((stats, [k, w]) => {
      stats[k] = affix[k].scale(this.tier) * w
      return stats
    }, {})
  }
  get name() {
    return `${this._name} (${this.rank})`
  }
  get rank() {
    return `Rank ${romanize(Math.max(1, ~~this.tier + 1))}`
  }
  get traits() {
    return {}
  }
  get value() {
    return (this.tier * 5) ** 3
  }
  equip(inventory) {
    super.equip(inventory)
    Object.entries(this.flavor).forEach(([k, v]) => {
      this.source.attributes.flavor[k].push(v)
    })
  }
  unequip(inventory) {
    Object.entries(this.flavor).forEach(([k, v]) => {
      this.source.attributes.flavor[k] = this.source.attributes.flavor[k].filter(
        f => f !== v
      )
    })
    super.unequip(inventory)
  }
}

export { Skill }
