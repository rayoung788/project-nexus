import { range } from '@/model/utilities/common'
import { choice } from '@/model/utilities/random'
import { Artisan } from './artisan'
import { WeaponFactory } from '@/model/equpiables/wearables/factories/weapon'
import { PlateArmorFactory } from '@/model/equpiables/wearables/factories/armor'

class Blacksmithing extends Artisan {
  craft() {
    const resources = this.resources
    const weapons = ['sword', 'axe', 'mace', 'dagger', 'greatsword', 'greataxe', 'greatmace']
    const armors = ['helmet', 'armor', 'gloves', 'boots']
    const goods = []
    const split = ~~((Math.random() * 0.4 + 0.3) * resources)
    range(split).forEach(() => {
      const weapon = choice(weapons)
      goods.push(WeaponFactory[weapon](this.tier))
    })
    range(resources - split).forEach(() => {
      const armor = choice(armors)
      goods.push(PlateArmorFactory[armor](this.tier))
    })
    return goods
  }
}

export { Blacksmithing }
