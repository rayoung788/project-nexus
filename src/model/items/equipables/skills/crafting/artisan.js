import { randint } from '@/model/utilities/random'
import { Skill } from '@/model/items/equipables/skill'

class Artisan extends Skill {
  constructor({ resources }) {
    super()
    this.resources = resources || randint(5, 10)
  }
  craft() {}
  stock() {
    this.craft().forEach(item => this.source.add(item))
    this.resources = 0
  }
  equip(entity) {
    super.equip(entity)
    this.source.carryWeight += 10000
    this.source.merchant = true
    this.stock()
  }
  unequip() {
    this.source.carryWeight -= 10000
    this.source.merchant = false
    this.source = false
  }
}

export { Artisan }
