import Equipable from './../equipable'
import { mapTier } from '../../utilities/common'

const qualities = ['Poor', 'Standard', 'Fine', 'Superior', 'Exquisite', 'Mastercraft']

const value = mapTier([5, 80])

class Wearable extends Equipable {
  constructor({ name, tier, subtype }) {
    super({ tier })
    this._name = name
    this.subtype = subtype
    this.stats = {}
  }
  get name() {
    return `${this._name} (${this.quality})`
  }
  get quality() {
    return `${qualities[Math.max(0, ~~this.tier)]}`
  }
  get value() {
    return (value(this.tier) ** 2 + this.weight * 5) * this.mod
  }
  get mod() {
    return 0
  }
}

export default Wearable
