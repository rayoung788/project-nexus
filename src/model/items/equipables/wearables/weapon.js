import Wearable from './../wearable'

import { mapValue } from '@/model/utilities/common'

class Weapon extends Wearable {
  constructor({ tier, subtype, name, weight, recovery }) {
    super({ name: name(), tier, subtype })
    this.type = 'weapons'
    this.slot = 'mainhand'
    this._weight = weight
    this.stats.recovery = window.dice.uniform(...recovery)
    this.stats.intensity = mapValue([6, 30], [25, 125], this.stats.recovery)
    this.stats.accuracy = mapValue([0, 6], [0, 1], this.tier) * this.mod
  }
  get mod() {
    return 1
  }
  get weight() {
    return this._weight(this.tier)
  }
}

class TwoHanded extends Weapon {
  get description() {
    return ['Two-handed Weapon', ...super.description]
  }
}

class OneHanded extends Weapon {
  constructor({ tier, subtype, name, weight, recovery }) {
    super({ tier, subtype, name, weight, recovery })
    this._slot = this.slot
  }
  get mod() {
    return 0.5
  }
  equip(inventory) {
    const { equipment, training } = inventory
    if (
      equipment[this.type][this.slot].name &&
      training[this.type].offhand.includes(this.subtype)
    ) {
      this.slot = 'offhand'
    }
    super.equip(inventory)
  }
  unequip(inventory) {
    super.unequip(inventory)
    this.slot = this._slot
  }
}

class OffHand extends OneHanded {
  constructor({ tier, subtype, name, weight, recovery }) {
    super({ tier, subtype, name, weight, recovery })
    this.slot = 'offhand'
  }
}

export { OneHanded, TwoHanded, OffHand }
