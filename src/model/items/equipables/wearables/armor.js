import Wearable from './../wearable'
import { mapValue, mapTier } from '@/model/utilities/common'

const materials = {
  cloth: {
    weight: mapTier([3, 6]),
    recovery: [15, 20]
  },
  padded: {
    weight: mapTier([6, 10]),
    recovery: [20, 25]
  },
  leather: {
    weight: mapTier([10, 15]),
    recovery: [25, 30]
  },
  mail: {
    weight: mapTier([15, 25]),
    recovery: [30, 35]
  },
  plate: {
    weight: mapTier([25, 40]),
    recovery: [35, 40]
  }
}

class Armor extends Wearable {
  constructor({ name, tier, subtype }) {
    super({ name, tier, subtype })
    this.type = 'armor'
    const recovery = window.dice.uniform(...materials[this.subtype].recovery)
    this.stats.recovery = recovery * this.mod
    // this should be armor but >> its easier this way
    this.stats.health = mapValue([15, 40], [50, 250], recovery) * this.mod
    this.stats.evasion = mapValue([0, 6], [0, 1], this.tier) * this.mod
    this.slot = 'armor'
  }
  get weight() {
    return materials[this.subtype].weight(this.tier) * this.mod
  }
  get mod() {
    return 0.4
  }
}

class Boots extends Armor {
  constructor({ name, tier, subtype }) {
    super({ name, tier, subtype })
    this.slot = 'boots'
  }
  get mod() {
    return 0.2
  }
}

class Gloves extends Armor {
  constructor({ name, tier, subtype }) {
    super({ name, tier, subtype })
    this.slot = 'gloves'
  }
  get mod() {
    return 0.2
  }
}

class Helmet extends Armor {
  constructor({ name, tier, subtype }) {
    super({ name, tier, subtype })
    this.slot = 'helmet'
  }
  get mod() {
    return 0.2
  }
}

class Shield extends Armor {
  constructor({ name, tier, subtype }) {
    super({ name, tier, subtype })
    this.type = 'weapons'
    this.slot = 'offhand'
  }
  get mod() {
    return 0.4
  }
}

export { Armor, Boots, Gloves, Helmet, Shield }
