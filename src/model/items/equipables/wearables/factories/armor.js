import { Armor, Boots, Gloves, Helmet } from '../armor'

// const hooded = weight => window.dice.weightedChoice(['Hooded ', ''], [weight, 1 - weight])
const robes = () => window.dice.choice(['Robes', 'Coat', 'Raiment', 'Vestment'])
const leather = () => window.dice.choice(['Leather', 'Hide', 'Fur', 'Scale'])
const plate = () => window.dice.choice(['Plate', 'Chainmail'])

const clothsub = 'cloth'
class ClothArmorFactory {
  static helmet(tier) {
    return new Helmet({
      name: `Cloth ${window.dice.choice(['Hood', 'Cowl'])}`,
      subtype: clothsub,
      tier: tier
    })
  }
  static armor(tier) {
    return new Armor({
      name: `Cloth ${robes()}`,
      subtype: clothsub,
      tier: tier
    })
  }
  static gloves(tier) {
    return new Gloves({
      name: `Cloth ${window.dice.choice(['Gloves', 'Handwraps'])}`,
      subtype: clothsub,
      tier: tier
    })
  }
  static boots(tier) {
    return new Boots({
      name: `Cloth Boots`,
      subtype: clothsub,
      tier: tier
    })
  }
  static full(tier) {
    return [
      ClothArmorFactory.helmet(tier),
      ClothArmorFactory.armor(tier),
      ClothArmorFactory.gloves(tier),
      ClothArmorFactory.boots(tier)
    ]
  }
}

const leathersub = 'leather'
class LeatherArmorFactory {
  static helmet(tier) {
    return new Helmet({
      name: `${leather()} ${window.dice.choice(['Helm', 'Hood'])}`,
      subtype: leathersub,
      tier: tier
    })
  }
  static armor(tier) {
    return new Armor({
      name: `${leather()} Armor`,
      subtype: leathersub,
      tier: tier
    })
  }
  static gloves(tier) {
    return new Gloves({
      name: `${leather()} ${window.dice.choice(['Gloves', 'Bracers'])}`,
      subtype: leathersub,
      tier: tier
    })
  }
  static boots(tier) {
    return new Boots({
      name: `${leather()} Boots`,
      subtype: leathersub,
      tier: tier
    })
  }
  static full(tier) {
    return [
      LeatherArmorFactory.helmet(tier),
      LeatherArmorFactory.armor(tier),
      LeatherArmorFactory.gloves(tier),
      LeatherArmorFactory.boots(tier)
    ]
  }
}

const platesub = 'plate'
class PlateArmorFactory {
  static helmet(tier) {
    return new Helmet({
      name: window.dice.choice([`Steel Plate Helm`]),
      subtype: platesub,
      tier: tier
    })
  }
  static armor(tier) {
    return new Armor({
      name: `Steel ${plate()} Armor`,
      subtype: platesub,
      tier: tier
    })
  }
  static gloves(tier) {
    return new Gloves({
      name: `Steel Plate Gauntlets`,
      subtype: platesub,
      tier: tier
    })
  }
  static boots(tier) {
    return new Boots({
      name: `Steel Plate Boots`,
      subtype: platesub,
      tier: tier
    })
  }
  static full(tier) {
    return [
      PlateArmorFactory.helmet(tier),
      PlateArmorFactory.armor(tier),
      PlateArmorFactory.gloves(tier),
      PlateArmorFactory.boots(tier)
    ]
  }
}

export default {
  [clothsub]: ClothArmorFactory,
  [leathersub]: LeatherArmorFactory,
  [platesub]: PlateArmorFactory
}
