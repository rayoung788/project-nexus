import { OneHanded, TwoHanded, OffHand } from '../weapon'
import { Shield } from '../armor'

import { mapTier } from '@/model/utilities/common'

const magic = () => window.dice.choice(['Infused', 'Ornate', 'Mystic'])

class WeaponFactory {
  // one-handed
  static dagger(tier) {
    return new OneHanded({
      subtype: 'dagger',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Dagger', 'Stiletto'])}`,
      weight: mapTier([2, 5]),
      recovery: [6, 8]
    })
  }
  static sword(tier) {
    return new OneHanded({
      subtype: 'sword',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Dagger', 'Stiletto'])}`,
      weight: mapTier([2, 5]),
      recovery: [6, 8]
    })
  }
  static axe(tier) {
    return new OneHanded({
      subtype: 'axe',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Waraxe', 'Hatchet', 'Cleaver', 'Scythe'])}`,
      weight: mapTier([11, 15]),
      recovery: [10, 12]
    })
  }
  static mace(tier) {
    return new OneHanded({
      subtype: 'mace',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Mace', 'Hammer', 'Flail', 'Maul'])}`,
      weight: mapTier([12, 16]),
      recovery: [12, 14]
    })
  }
  // two-handed
  static greatsword(tier) {
    return new TwoHanded({
      subtype: 'greatsword',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Greatsword', 'Estoc', 'Nodachi', 'Claymore'])}`,
      weight: mapTier([18, 23]),
      recovery: [20, 24]
    })
  }
  static greataxe(tier) {
    return new TwoHanded({
      subtype: 'greataxe',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Greataxe', 'Battleaxe', 'Halberd', 'Glaive'])}`,
      weight: mapTier([20, 25]),
      recovery: [24, 28]
    })
  }
  static greatmace(tier) {
    return new TwoHanded({
      subtype: 'greatmace',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Warhammer', 'Morning Star', 'Longmace'])}`,
      weight: mapTier([24, 30]),
      recovery: [28, 32]
    })
  }
  // ranged
  static bow(tier) {
    return new TwoHanded({
      subtype: 'bow',
      tier: tier,
      name: () => `${window.dice.choice(['Hunting', 'War'])} Bow`,
      weight: mapTier([6, 12]),
      recovery: [16, 20]
    })
  }
  static crossbow(tier) {
    return new TwoHanded({
      subtype: 'crossbow',
      tier: tier,
      name: () => `Steel ${window.dice.choice(['Crossbow', 'Arbalest'])}`,
      weight: mapTier([12, 16]),
      recovery: [20, 25]
    })
  }
  static firearm(tier) {
    return new TwoHanded({
      subtype: 'firearm',
      tier: tier,
      name: () => `${window.dice.choice(['Blunderbuss', 'Arquebus'])}`,
      weight: mapTier([14, 18]),
      recovery: [25, 30]
    })
  }
  static pistol(tier) {
    return new OneHanded({
      subtype: 'pistol',
      tier: tier,
      name: () => `Pistol`,
      weight: mapTier([3, 6]),
      recovery: [12, 14]
    })
  }
  // magic
  static staff(tier) {
    return new TwoHanded({
      subtype: 'staff',
      tier: tier,
      name: () => `${magic()} ${window.dice.choice(['Staff', 'Rod'])}`,
      weight: mapTier([10, 20]),
      recovery: [20, 30]
    })
  }
  static implement(tier) {
    return new OneHanded({
      subtype: 'implement',
      tier: tier,
      name: () => `${magic()} ${window.dice.choice(['Wand', 'Scepter'])}`,
      weight: mapTier([3, 6]),
      recovery: [10, 15]
    })
  }
  static bound(tier) {
    return new OneHanded({
      subtype: 'bound',
      tier: tier,
      name: () => `${window.dice.choice(['Bound', 'Ethereal'])} ${window.dice.choice(['Sword', 'Blade'])}`,
      weight: mapTier([2, 4]),
      recovery: [10, 15]
    })
  }
  // offhand
  static magic(tier) {
    return new OffHand({
      subtype: 'magic',
      tier: tier,
      name: () =>
        `${magic()} ${window.dice.choice(['Grimoire', 'Tome', 'Orb', 'Lantern', 'Lamp'])}`,
      weight: mapTier([2, 4]),
      recovery: [10, 15]
    })
  }
  static divine(tier) {
    return new OffHand({
      subtype: 'divine',
      tier: tier,
      name: () => `${window.dice.choice(['Holy', 'Divine'])} ${window.dice.choice(['Tome', 'Symbol'])}`,
      weight: mapTier([2, 4]),
      recovery: [10, 15]
    })
  }
  static shield(tier) {
    return new Shield({
      name: `Steel Shield`,
      subtype: 'shield',
      tier: tier
    })
  }
}

export default WeaponFactory
