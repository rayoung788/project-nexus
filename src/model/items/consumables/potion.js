import Item from './../item'
import { title } from '@/model/utilities/common'

const qualities = ['Potion', 'Draught', 'Solution', 'Philter', 'Elixir', 'Flask']
const effects = ['Strength', 'Intellect', 'Dexterity', 'Fortitude', 'Healing']

class Potion extends Item {
  constructor({ tier, attribute }) {
    super()
    this.attribute = attribute || window.dice.choice(effects)
    this.tier = (tier || 1) + window.dice.norm(0, 0.2)
  }

  get name() {
    return `${this.quality} of ${title(this.attribute)}`
  }

  get quality() {
    return `${qualities[Math.max(0, ~~this.tier)]}`
  }
}

export default Potion
