import Item from './../item'

import { romanize, title } from '@/model/utilities/common'

const effects = {
  weapon: {
    ice: (name) => `${name} of ${window.dice.choice(['Frost', 'Ice', 'Freezing', 'Blizzards', 'Winter'])}`,
    fire: (name) => `${name} of ${window.dice.choice(['Scorching', 'Burning', 'the Inferno', 'the Blaze'])}`,
    lightning: (name) => `${name} of ${window.dice.choice(['Arcing', 'Shocks', 'Storms'])}`,
    blood: (name) => `${name} of ${window.dice.choice(['Consuming', 'Harvesting', 'Devouring', 'Leeching'])}`,
    fear: (name) => `${name} of ${window.dice.choice(['Fear', 'Dread', 'Despair', 'Terror'])}`,
    holy: (name) => `${name} of ${window.dice.choice(['Blessed', 'Sanctified', 'Reverent', 'Hallowed', 'Virtuous'])}`
  },
  armor: {
    intellect: (name) => `${name} of ${window.dice.choice(['Intellect'])}`,
    dexterity: (name) => `${name} of ${window.dice.choice(['Dexterity'])}`,
    strength: (name) => `${name} of ${window.dice.choice(['Strength'])}`
  }
}

class Rune extends Item {
  constructor({ tier, type, attribute }) {
    super()
    this.placeholder = '<weapon>'
    this.type = type || window.dice.random > 0.5 ? 'armor' : 'weapon'
    this.attribute = attribute || window.dice.choice(Object.keys(effects[this.type]))
    this._name = `${title(this.attribute)} Rune`
    this.tier = (tier || 1) + window.dice.norm(0, 0.2)
    this._affix = effects[this.type][this.attribute](this.placeholder)
  }

  get name() {
    return `${this._name}: Rank ${this.rank})`
  }

  get rank() {
    return `Rank ${romanize(Math.max(1, Math.round(this.tier)))}`
  }

  affix(name) {
    return this._affix.replace(this.placeholder, name)
  }
}

export default Rune
