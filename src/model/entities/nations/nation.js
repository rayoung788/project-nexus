import { range } from '../../utilities/common'

export default class {
  constructor(subject) {
    this.type = 'Monarchy'
    this.subject = subject
    // default cultural titles
    this.religions = {
      druid: {
        lesser: 'Druid',
        greater: 'Archdruid'
      },
      shaman: {
        lesser: 'Shaman',
        greater: 'Far Seer'
      },
      witch: {
        lesser: 'Witch Doctor',
        greater: 'High Priest'
      },
      priest: {
        lesser: 'Priest',
        greater: 'Bishop'
      }
    }
    this.titles = {
      king: {
        male: 'King',
        female: 'Queen'
      },
      archduke: {
        male: 'Archduke',
        female: 'Archduchess'
      },
      duke: {
        male: 'Duke',
        female: 'Duchess'
      },
      count: {
        male: 'Count',
        female: 'Countess'
      },
      baron: {
        male: 'Baron',
        female: 'Baroness'
      },
      house: 'House',
      diplomat: window.dice.choice([
        'Diplomat',
        'Ambassador',
        'Consul',
        'Envoy',
        'Emissary'
      ]),
      'royal guards': `${window.dice.choice(['Royal', 'Honor', 'Palace'])} Guard`,
      guard: window.dice.choice([
        'Guard',
        'Guard',
        'Guard',
        'Sentinal',
        'PeaceKeeper'
      ]),
      district: window.dice.choice(['District', 'Quarter', 'Canton']),
      village: {
        title: '% Village',
        cutoff: 1500
      }
    }
    this.tribes = false
    this.spec = {
      mage: 0,
      warrior: 0,
      rogue: 0,
      faith: 0
    }
  }
  get apply() {
    this.religion = this.religion_type
    this.subject.classes = this.classes
    this.subject.spec = this.spec
    this.subject.gov = this.type
    this.titles.priest = this.religions[this.religion].lesser
    this.titles.bishop = this.religions[this.religion].greater
    this.subject.titles = this.titles
    this.subject.tribal = this.tribes
  }
  // hero classes
  get classes() {
    const used = new Proxy(
      {},
      {
        get: (target, name) => (name in target ? target[name] : 0)
      }
    )
    used.mercenary += 1
    const mages = !this.tribes && window.dice.random > 0.8 ? 0 : 0
    used.mage += mages
    const religion = this.religion === 'priest' ? 'priest' : 'naturalist'
    used[religion] += 1
    const specialists = 4 - mages
    range(specialists).forEach(() => {
      used[window.dice.choice(this.specialists(used, religion))] += 1
    })
    this.spec.mage =
      used.mage + used.warlock + used.hybrid + used.dark_hybrid + used.elemental
    this.spec.dark = used.warlock + used.dark_hybrid
    this.spec.warrior = used.mercenary + used.knight
    this.spec.rogue = used.rogue + used.hunter + used.tribal + used.science
    this.spec.faith = used.templar + used.naturalist + used.priest
    this.spec.hybrids = used.hybrid + used.dark_hybrid
    this.spec.science = used.science
    return Object.entries(used).reduce(
      (total, [k, v]) =>
        total.concat(
          window.dice
            .sample(Object.keys(this[k]), v)
            .map(c => window.dice.choice(this[k][c]))
        ),
      []
    )
  }
  specialists(used, religion) {
    const preprocessed = [...this.basic, religion]
    used.priest > 0 && preprocessed.push('templar')
    used.mage > 0 && preprocessed.push('hybrid', 'hybrid')
    used.warlock > 0 && preprocessed.push('dark_hybrid', 'dark_hybrid')
    const specialists = preprocessed.filter(
      s => Object.keys(this[s]).length > used[s]
    )
    return specialists
  }
  get religion_type() {
    return window.dice.random > 0.8 ? 'druid' : 'priest'
  }
  get basic() {
    return ['knight', 'mage', 'rogue', 'warlock', 'hunter']
  }
  get mercenary() {
    return {
      mercenary: ['Mercenary', 'Warrior']
    }
  }
  get knight() {
    return {
      knight: ['Vanguard', 'Knight', 'Defender', 'Guardian'],
      reaver: ['Berserker', 'Reaver', 'Warbringer']
    }
  }
  get mage() {
    return {
      elemental: ['Pyromancer', 'Frostbinder', 'Stormcaller'],
      wizard: ['Wizard', 'Arcanist'],
      enchanter: ['Illusionist', 'Enchanter']
    }
  }
  get warlock() {
    return {
      warlock: ['Warlock', 'Occultist', 'Cabalist', 'Sorceror'],
      necromancer: ['Necromancer', 'Deathspeaker']
    }
  }
  get rogue() {
    return {
      thief: ['Assassin', 'Thief', 'Rogue'],
      wanderer: ['Wanderer', 'Marauder', 'Bounty Hunter'],
      monk: ['Monk', 'Brewmaster', 'Kensai']
    }
  }
  get hunter() {
    return {
      hunter: ['Ranger', 'Marksman']
    }
  }
  get hybrid() {
    return {
      // archer: ['Arcane Archer'],
      knight: ['Arcane Knight', 'Spellblade', 'Battle Mage', 'Theurgist'],
      rogue: ['Trickster', 'Mesmer', 'Cipher']
    }
  }
  get dark_hybrid() {
    return {
      // archer: ['Dark Ranger', 'Shadow Hunter'],
      knight: ['Blood Knight', 'Deathbringer', 'Dreadblade'],
      rogue: ['Nightblade', 'Shadowdancer']
    }
  }
  get priest() {
    return {
      priest: ['Cleric', 'Priest', 'Oracle']
    }
  }
  get templar() {
    return {
      archer: ['Demon Hunter', 'Witch Hunter'],
      knight: ['Inquisitor', 'Justicar', 'Paladin', 'Templar']
    }
  }
  get naturalist() {
    return {
      druid: ['Druid', 'Shapeshifter', 'Naturalist']
    }
  }
  get science() {
    return {
      alchemist: ['Alchemist', 'Apothacary'],
      engineer: ['Engineer', 'Tinkerer', 'Saboteur']
    }
  }
}
