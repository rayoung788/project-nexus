import Nation from './nation'

export default class extends Nation {
  constructor(subject) {
    super(subject)
    this.type = 'Trading Company'
    const director = {
      male: 'Director',
      female: 'Director'
    }
    this.titles.king = director
    this.titles.archduke = director
    const governor = {
      male: 'Governor',
      female: 'Governor'
    }
    this.titles.duke = governor
    this.titles.count = governor
    this.titles.baron = governor
  }
  get knight() {
    return {
      knight: super.knight.knight
    }
  }
}
