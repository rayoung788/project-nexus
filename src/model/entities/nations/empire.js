import Dynasty from './dynasty'

export default class extends Dynasty {
  constructor(subject) {
    super(subject)
    this.type = 'Empire'
  }
  get knight() {
    return {
      knight: ['Centurion', 'Praetorian']
    }
  }
}
