import Nation from './nation'

export default class extends Nation {
  constructor(subject) {
    super(subject)
    this.type = 'Dynasty'
    this.titles.king = {
      male: 'Emperor',
      female: 'Empress'
    }
    this.titles.archduke = {
      male: 'Viceroy',
      female: 'Viceroy'
    }
  }
  get knight() {
    return {
      knight: super.knight.knight
    }
  }
}
