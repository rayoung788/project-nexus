import Nation from './nation'

export default class extends Nation {
  constructor(subject) {
    super(subject)
    this.type = 'Republic'
    this.titles.councilor = window.dice.choice(['Councilor', 'Senator'])
    this.titles.king = {
      male: 'High Consul',
      female: 'High Consul'
    }
    this.titles.baron = {
      male: 'Mayor',
      female: 'Mayor'
    }
  }
  get knight() {
    return {
      knight: super.knight.knight
    }
  }
  get basic() {
    return super.basic.filter(s => s !== 'warlock')
  }
}
