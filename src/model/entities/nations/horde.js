import Tribal from './tribal'

export default class extends Tribal {
  constructor(subject) {
    super(subject)
    this.type = 'Horde'
    this.titles.king = {
      male: 'Warchief',
      female: 'Warchief'
    }
    const high_warlord = {
      male: 'High Warlord',
      female: 'High Warlord'
    }
    this.titles.archduke = high_warlord
    this.titles.duke = high_warlord
    const warlord = {
      male: 'Warlord',
      female: 'Warlord'
    }
    this.titles.count = warlord
    this.titles.baron = warlord
  }
  get basic() {
    return ['knight', 'rogue', 'warlock', 'tribal', 'hunter', 'elemental']
  }
  get knight() {
    return {
      knight: ['Vanguard', 'Defender', 'Guardian'],
      reaver: super.knight.reaver
    }
  }
}
