import Nation from './nation'

export default class extends Nation {
  constructor(subject) {
    super(subject)
    this.type = 'Tribal'
    this.titles.king = {
      male: 'High Chieften',
      female: 'High Chieften'
    }
    const chief = {
      male: 'Chieften',
      female: 'Chieften'
    }
    this.titles.archduke = chief
    this.titles.duke = chief
    this.titles.count = chief
    this.titles.baron = chief
    this.titles.house = 'Clan'
    this.titles.village = {
      title: window.dice.choice(['% Camp', 'Camp %']),
      cutoff: 6000
    }
    this.tribes = true
  }
  get classes() {
    const classes = super.classes
    this.religion = classes.some(c => this.naturalist.shaman.includes(c))
      ? 'shaman'
      : 'witch'
    return classes
  }
  get religion_type() {
    return window.dice.random > 0.8 ? 'shaman' : 'witch'
  }
  get basic() {
    return ['knight', 'rogue', 'hunter', 'tribal', 'elemental', 'druid']
  }
  get knight() {
    return {
      reaver: super.knight.reaver.concat(['Spirit Warrior'])
    }
  }
  get rogue() {
    return {
      thief: super.rogue.thief,
      wanderer: super.rogue.wanderer
    }
  }
  get tribal() {
    return {
      axethrower: ['Axethrower', 'Headhunter'],
      exotic: ['Blood Drinker', 'Beastmaster', 'Shadow Hunter']
    }
  }
  get elemental() {
    return {
      mage: this.mage.elemental
    }
  }
  get naturalist() {
    return {
      shaman: ['Shaman', 'Watershaper', 'Mistweaver', 'Primalist', 'Elementalist'],
      witch: ['Witch Doctor', 'Soulpriest', 'Spiritualist']
    }
  }
  get druid() {
    return {
      druid: ['Druid', 'Shapeshifter', 'Naturalist']
    }
  }
}
