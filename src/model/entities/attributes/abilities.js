import { variants } from '@/model/utilities/common'

const undead = [
  ...variants(
    ['Bone', 'Spectral', 'Undead', 'Fallen'],
    ['Soldier', 'Archer', 'Knight', 'Champion', 'Guard', 'Wizard', 'Hound']
  ),
  ...variants(
    ['Undead'],
    [
      'Ghoul',
      'Abomination',
      'Husk',
      'Horror',
      'Zombie',
      'Spectre',
      'Shade',
      'Revenant',
      'Wraith'
    ]
  )
]
const demons = [
  ...variants(
    ['Demon of'],
    ['Despair', 'Envy', 'Pride', 'Lust', 'Wrath', 'Terror', 'Spite']
  ),
  ...variants(
    ['Fel', 'Abyssal', 'Dread', 'Void', 'Infernal'],
    ['Guard', 'Soldier', 'Stalker', 'Beast', 'Knight', 'Hound', 'Bats']
  )
]
const frenzy = [
  ...variants(['Savage', 'Wild', 'Primal'], ['Frenzy', 'Rage', 'Fury']),
  'Berserk'
]

const beastSkills = [
  ...variants(
    ['Ferocious', 'Brutal', 'Savage', 'Wild', 'Primal'],
    ['Bite', 'Slash', 'Rake', 'Rip']
  ),
  'Ravage',
  'Maul',
  'Mangle',
  'Lacerate'
]

const shadowInfuse = variants(['Shadow', 'Unholy', 'Void'], ['Infusion'])

const zeal = variants(
  ['Divine', 'Holy'],
  ['Zeal', 'Judgement', 'Wrath', 'Verdict', 'Avenger']
)

export default {
  spells: {
    ice: [
      'Ice Storm',
      'Frost Blast',
      'Ice Spikes',
      'Ice Flurry',
      'Icewind',
      'Frozen Orb',
      'Howling Blast',
      'Glacial Blast',
      'Arctic Gale',
      'Hungering Cold',
      'Blizzard',
      "Winter's Grasp"
    ],
    fire: [
      'Wild Fire',
      'Burning Orb',
      'Incinerate',
      'Immolate',
      'Fire Blast',
      'Fire Ball',
      'Combustion',
      'Chaos Fire',
      'Inferno',
      'Soul Burn',
      'Searing Flames',
      'Meteor',
      'Scorching Fire'
    ],
    arcane: [
      ...variants(
        ['Arcane', 'Nether', 'Astral'],
        ['Blast', 'Barrage', 'Tempest', 'Surge', 'Torrent', 'Nova']
      ),
      'Archon',
      'Asphyxiate'
    ],
    lightning: [
      ...variants(['Lighting', 'Storm'], ['Blast', 'Tempest', 'Surge', 'Nova']),
      'Overload',
      'Chain Lightning'
    ],
    earth: [
      ...variants(
        ['Earth', 'Rock', 'Stone'],
        ['Shatter', 'Crush', 'Slam', 'Strike']
      ),
      'Avalanche',
      'Rock Slide',
      'Earthquake',
      'Throw Boulder'
    ],
    nature: [
      ...variants(['Root', 'Bark', 'Thorn'], ['Strike']),
      ...variants(['Insect', 'Wasp', 'Spider'], ['Swarm']),
      ...variants(['Crushing', 'Strangulating'], ['Roots'])
    ],
    wind: variants(['Arcane', 'Astral'], ['Gust', 'Gale', 'Winds', 'Tornado']),
    water: variants(
      ['Water'],
      ['Blast', 'Barrage', 'Tempest', 'Surge', 'Torrent', 'Geyser']
    ),
    blood: variants(['Blood', 'Soul'], ['Drain', 'Harvest', 'Feast', 'Drinker']),
    mind: [
      ...variants(['Mind'], ['Blast', 'Sear', 'Flay', 'Spike']),
      'Phantom Pain',
      'Nightmare',
      ...variants(['Touch of'], ['Insanity', 'Madness', 'Mayhem', 'Hysteria'])
    ],
    shadow: [
      ...variants(
        ['Shadow', 'Eldrich', 'Void'],
        ['Blast', 'Barrage', 'Tempest', 'Surge']
      ),
      'Asphyxiate'
    ],
    light: [
      'Smite',
      'Penance',
      ...variants(['Blessed', 'Sanctified', 'Holy'], ['Fire', 'Flames', 'Blaze'])
    ]
  },
  stuns: {
    ice: ['Permafrost', 'Flash Freeze', 'Deep Freeze', 'Ice Tomb'],
    arcane: variants(
      ['Polymorph:', 'Hex:'],
      ['Sheep', 'Rat', 'Chicken', 'Frog', 'Toad', 'Rabbit']
    ),
    mind: ['Fear', 'Sleep', 'Terror', 'Mesmerize']
  },
  evade: {
    ice: [
      ...variants(['Ice', 'Frost'], ['Barrier', 'Armor', 'Ward']),
      'Ice Block',
      'Ice Wall'
    ],
    fire: [...variants(['Ash'], ['Shell', 'Ward', 'Shield']), 'Cauterize'],
    arcane: [
      'Teleport',
      'Invisibility',
      'Mirror Images',
      'Fade',
      'Time Warp',
      ...variants(
        ['Arcane', 'Nether', 'Void', 'Astral'],
        ['Ward', 'Shield', 'Barrier', 'Distortion']
      )
    ],
    fighter: ['Parry', 'Block'],
    light: variants(['Holy', 'Divine'], ['Shield', 'Aegis', 'Bulwark'])
  },
  counter: {
    ice: variants(['Ice', 'Frost'], ['Sigil']),
    fire: variants(['Fire', 'Flame'], ['Sigil']),
    shadow: [
      ...variants(['Shadow', 'Dark'], ['Simulacrum']),
      ...variants(['Blood', 'Soul'], ['Mirror'])
    ],
    fighter: ['Riposte', 'Revenge'],
    traps: variants(
      ['Spike', 'Bear', 'Immolation', 'Explosive', 'Arcane', 'Poison'],
      ['Trap']
    )
  },
  afflictions: {
    curses: variants(
      ['Curse of', 'Mark of'],
      [
        'Darkness',
        'Pain',
        'Decay',
        'Doom',
        'Misery',
        'Agony',
        'Anguish',
        'Ruin',
        'Languish',
        'Despair',
        'Corruption',
        'Suffering',
        'Affliction'
      ]
    ),
    diseases: [
      ...variants(['Bone', 'Corpse', 'Flesh', 'Brain'], ['Rot']),
      ...variants(
        ['Shadow', 'Blood', 'Virulent', 'Necrotic'],
        ['Plague', 'Fever', 'Parasite', 'Contagion']
      ),
      'Pestilence',
      'Necrosis',
      'Haunt',
      'Locust Swarm'
    ],
    poison: variants(
      ['Deadly', 'Malign', 'Lethal', 'Potent', 'Corrosive'],
      ['Poison', 'Venom', 'Toxin']
    )
  },
  summons: {
    undead: variants(['Reanimate:'], undead),
    demons: variants(['Summon:'], demons),
    beasts: variants(
      ['Animal Companion:'],
      ['Wolf', 'Hound', 'Spider', 'Hawk', 'Viper', 'Raven', 'Fox']
    )
  },
  healing: {
    arcane: [
      ...variants(
        ['Life', 'Arcane', 'Living'],
        [
          'Rejuvenation',
          'Regrowth',
          'Renewal',
          'Bloom',
          'Resurgence',
          'Restoration',
          'Blossom',
          'Regeneration',
          'Revival',
          'Mend'
        ]
      ),
      'Wild Growth'
    ],
    light: [
      ...variants(
        ['Prayer of', 'Hymn of', 'Blessing of'],
        [
          'Healing',
          'Mending',
          'Life',
          'Divinity',
          'Serenity',
          'Tranquility',
          'Hope',
          'Redemption',
          'Solace',
          'Salvation',
          'Light'
        ]
      ),
      'Light of Dawn',
      'Healing Light',
      'Flash Heal',
      'Holy Radiance'
    ]
  },
  melee: {
    fighter: [
      'Cleave',
      'Flurry',
      'Crushing Blow',
      'Rend',
      'Deep Wound',
      'Charge',
      'Sunder',
      'Punish',
      'Obliterate',
      'Annihilate',
      'Bull Rush',
      'Pulverize'
    ],
    knight: [
      ...variants(['Mortal', 'Heroic', "Champion's"], ['Strike']),
      'Battle Trance',
      'Paragon'
    ],
    tank: variants(['Shield'], ['Slam', 'Bash', 'Charge']),
    berserker: [
      ...frenzy,
      ...variants(['Brutal', 'Savage', 'Wild', 'Primal'], ['Strike']),
      'Bloodthirst',
      'Rampage',
      'Whirlwind',
      'Bloodlust'
    ],
    rogue: [
      'Eviscerate',
      'Mutilate',
      'Rupture',
      'Sever',
      'Fan of Knives',
      'Ambush',
      'Grievous Wounds',
      'Hemorrhage',
      'Hidden Pistol'
    ]
  },
  ranged: {
    marksman: [
      ...variants(
        [
          'Aimed',
          'Quick',
          'Precision',
          'Piercing',
          'Shatter',
          'Wounding',
          'Poisoned',
          'Viper'
        ],
        ['Shot']
      ),
      'Rapid Fire'
    ],
    elemental: variants(
      ['Elemental', 'Void', 'Arcane', 'Nether', 'Explosive', 'Immolation', 'Frost'],
      ['Shot']
    ),
    shadow: [
      ...variants(
        [
          'Shadow',
          'Scourge',
          'Entropic',
          'Necrotic',
          'Festering',
          'Unholy',
          'Void',
          'Dusk'
        ],
        ['Shot']
      ),
      ...shadowInfuse
    ],
    blood: variants(
      ['Devouring', 'Consuming', 'Hungering', 'Vampiric', 'Sanguine'],
      ['Shot']
    ).concat(['Soul Reaper']),
    light: [
      ...zeal,
      ...variants(
        ['Sanctified', 'Blessed', 'Divine', 'Righteous', 'Virtuous'],
        ['Blade', 'Hammer', 'Shot']
      )
    ],
    engineering: variants(['Frost', 'Fire', 'Cluster'], ['Grenade', 'Bomb'])
  },
  beast: {
    melee: [
      ...variants(
        ['Ferocious', 'Brutal', 'Savage', 'Wild', 'Primal'],
        ['Bite', 'Slash', 'Rake', 'Rip']
      ),
      'Ravage',
      'Maul',
      'Mangle',
      'Lacerate'
    ],
    venom: variants(
      ['Poison', 'Venomous', 'Toxic'],
      ['Bite', 'Spit', 'Strike', 'Sting', 'Fang']
    ),
    diseased: variants(
      ['Infected', 'Diseased', 'Necrotic', 'Blight', 'Plagued'],
      ['Bite', 'Slash', 'Strike']
    ),
    breath: variants(['Fire', 'Frost', 'Ice', 'Flame'], ['Breath'])
  },
  shifter: variants(['Werewolf:', 'Werebear:'], beastSkills),
  runes: {
    shadow: [
      ...variants(
        [
          'Shadow',
          'Scourge',
          'Entropic',
          'Necrotic',
          'Festering',
          'Unholy',
          'Void',
          'Dusk'
        ],
        ['Strike']
      ),
      ...shadowInfuse
    ],
    blood: variants(
      ['Devouring', 'Consuming', 'Hungering', 'Vampiric', 'Sanguine'],
      ['Strike']
    ).concat(['Soul Reaper']),
    fire: variants(['Blazing', 'Scorching', 'Burning', 'Searing'], ['Strike']),
    ice: variants(['Freezing', 'Chilling', 'Frost', 'Frozen'], ['Strike']),
    arcane: variants(['Arcane', 'Warp', 'Phase', 'Astral'], ['Strike']),
    storm: variants(['Storm', 'Lightning'], ['Strike']),
    light: [
      ...zeal,
      ...variants(
        ['Blade of', 'Hammer of'],
        [
          'Virtue',
          'Retribution',
          'Righteousness',
          'Vengeance',
          'Dawn',
          'Justice',
          'Wrath',
          'Reckoning',
          'Light',
          'Valor'
        ]
      ),
      ...variants(
        ['Sanctified', 'Blessed', 'Divine', 'Righteous', 'Virtuous'],
        ['Blade', 'Hammer']
      )
    ]
  }
}
