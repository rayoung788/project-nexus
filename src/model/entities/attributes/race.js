import { Language } from '@/model/entities/attributes/language'
import { title } from '@/model/utilities/common'

class Race {
  constructor() {
    this.name = ''
  }

  toString() {
    return this.name
  }

  culturize(entity) {}
}

class Humanoid extends Race {
  constructor(idx, origin, climate) {
    super()
    // naming language
    this.language = new Language()
    this.name = this.language.generic('culture')
    // styles
    const lang = this.language
    let styled = false
    const exotic = ['qq']
    this.exotic = lang.stop_injects > 1 || lang.phonemes.ʔ === '-' || exotic.some(g => lang._consonants.includes(g)) || ['j', 'q'].some(e => lang.phonemes.F.join('').includes(e))
    styled = this.exotic
    const gutteral = ['gg', 'gmm', 'ggr', 'xx', 'zz', 'ww', 'gx', 'gz']
    const vowels = lang._vowels.join('')
    this.gutteral = (['e', 'i'].every(e => !vowels.includes(e)) || !vowels.includes('a') || gutteral.some(g => lang._consonants.includes(g)) || ['b', 'sh', 'sk'].some(e => lang.phonemes.F.join('').includes(e))) && !styled
    styled |= this.gutteral
    this.melodic = (lang._vowels.some(v => v === 'ae') || lang.phonemes.ʔ === "'" || ['o', 'u'].every(e => !vowels.includes(e))) && !styled
    styled |= this.melodic
    this.common = !styled
    // id used to index world.cultures
    this.idx = idx
    // origin region id used to index world.regions
    this.origin = origin
    // climate designation (arctic, temperate, tropical)
    this.climate = climate
    // subrace designation
    this.subRace = this.melodic ? 'Melodic' : this.exotic ? 'Exotic' : this.gutteral ? 'Gutteral' : 'Common'
    // display colors
    this.colors = window.dice.colorCascade()
    this.display = this.colors.start
    // regional count
    this.regions = 0
    // cultural traits
    this.advantage = {
      economics: window.dice.random,
      military: window.dice.random
    }
    this.traits = {
      // ideology
      economics: window.dice.random, // management -> capitalism (merchant power)
      government: window.dice.random, // monarchy -> republic -> city states
      aggression: window.dice.random, // violent vs non-violent strategies
      liberalism: window.dice.random, // tolerance of cultural differences
      diplomacy: window.dice.random, // engagement -> neutrality
      religion: window.dice.random, // doctrines, gods, & influence
      // innovation
      magic: window.dice.random, // magic district -> mage guild -> circle -> magic outlawed
      technology: window.dice.random // advanced (firearms, cannons, explosives, medicine) -> average (crossbows, ballista, arbalests) -> low (bows, battering rams)
    }
    // default hero classes
    this.classes = ['Mercenary']
    // default cultural titles
    this.titles = {}
    // cultural attributes
    const splitHealth = 10 * window.dice.random
    const splitDefense = 0.1 * window.dice.random
    this.attributes = {
      health: splitHealth,
      evasion: splitDefense,
      intensity: 10 - splitHealth,
      accuracy: 0.1 - splitDefense
    }
    this.persona = {
      empathy: window.dice.uniform(-0.1, 0.1),
      objectivity: window.dice.uniform(-0.1, 0.1),
      ambition: window.dice.uniform(-0.1, 0.1),
      change: window.dice.uniform(-0.1, 0.1),
      charisma: window.dice.uniform(-0.1, 0.1)
    }
  }

  get details() {
    return {
      name: this.name,
      appearence: [['Climate', title(this.climate)], ['Climate', title(this.subRace)]],
      attributes: Object.entries(this.attributes).map(([stat, value]) => {
        return [title(stat), value.toFixed(2)]
      }),
      persona: Object.entries(this.persona).map(([stat, value]) => {
        return [title(stat), value.toFixed(2)]
      }),
      advantage: Object.entries(this.advantage).map(([k, v]) => [
        title(k),
        v.toFixed(2)
      ]),
      traits: Object.entries(this.traits).map(([k, v]) => [title(k), v.toFixed(2)]),
      language: this.language.details
    }
  }

  culturize(entity) {
    Object.entries(this.attributes).forEach(([stat, value]) => {
      entity.attributes.stats[stat] += value
    })
    Object.entries(this.persona).forEach(([stat, value]) => {
      entity.persona[stat] += value
    })
  }

  genName(key, ...args) {
    return this.language[key](...args)
  }
}

export { Humanoid }
