import { mapValue } from '../../utilities/common'

import stats from './stats'

class Attributes {
  constructor() {
    this.stats = {
      ...Object.entries(stats)
        .filter(([, v]) => !v.credit) // credit => derived
        .reduce((sum, [k]) => {
          sum[k] = 0
          return sum
        }, {}),
      // set base values
      health: 30, // base health
      recovery: 5, // time between attacks
      accuracy: 0.15, // chance to hit
      might: 1.5, // crit bonus
      chaos: window.dice.uniform(0.05, 0.15), // the hand of fate
      sanity: 1 // friend or foe detection
    }
    this.abilities = {
      offense: [],
      healing: [],
      thorns: [],
      evasion: [],
      stun: []
    }
    // stunned status
    this.incapacitated = false
    // magic source flag
    this.magic = false
    // friend or foe
    this._allies = []
    this._enemies = []
    // modded stats
    this.mod = {}
    this.reset()
  }

  get evasion() {
    const evasion = this.compute('evasion')
    return evasion * (1 + this.chaos)
  }

  get endurance() {
    const endurance = this.compute('endurance')
    return 1 + endurance
  }

  get vitality() {
    const vitality = this.compute('vitality')
    return 1 + vitality
  }

  get accuracy() {
    const accuracy = this.compute('accuracy')
    return accuracy * (1 + this.chaos)
  }

  get maxHealth() {
    return this.stats.health * this.endurance
  }

  get health() {
    return this.maxHealth + this.mod.health
  }

  set health(value) {
    this.mod.health = value > 0 ? 0 : value
  }

  get percentHealth() {
    return this.health / this.maxHealth
  }

  get chaos() {
    const chaos = this.compute('chaos')
    return window.dice.uniform(-chaos, chaos) + this.compute('luck')
  }

  get desc() {
    const core = ['health', 'intensity', 'sanity']
    return Object.keys(this.stats)
      .filter(name => this.stats[name] !== 0 && !core.includes(name))
      .map(name => {
        const value = this.stats[name]
        return {
          name: name,
          desc: stats[name].desc(value)
        }
      })
  }

  compute(k) {
    return this.stats[k] + this.mod[k]
  }

  armor(source) {
    let armor = this.compute('armor')
    const glory = this.compute('glory')
    armor += mapValue([0, 1], [-glory, glory], source.percentHealth)
    const victory = this.compute('victory')
    armor += mapValue([0, 1], [victory, -victory], source.percentHealth)
    const bastion = this.compute('bastion')
    armor += mapValue([0, 1], [-bastion, bastion], this.percentHealth)
    const guardian = this.compute('guardian')
    armor += mapValue([0, 1], [guardian, -guardian], this.percentHealth)
    armor += this.compute(source.magic ? 'warding' : 'fiend')
    armor -= this.compute(source.magic ? 'fiend' : 'warding')
    return armor
  }

  intensity(target) {
    let potency = this.compute('potency')
    const pride = this.compute('pride')
    potency += mapValue([0, 1], [-pride, pride], this.percentHealth)
    const vengeance = this.compute('vengeance')
    potency += mapValue([0, 1], [vengeance, -vengeance], this.percentHealth)
    const envy = this.compute('vengeance')
    potency += mapValue([0, 1], [-envy, envy], target.percentHealth)
    const havoc = this.compute('havoc')
    potency += mapValue([0, 1], [havoc, -havoc], target.percentHealth)
    potency += this.compute(target.magic ? 'lore' : 'hunter')
    potency -= this.compute(target.magic ? 'hunter' : 'lore')
    return Math.max(this.compute('intensity') * (1 + potency + this.chaos), 0)
  }

  regenerate() {
    const lifebinder = this.compute('regen') * this.maxHealth
    return this.restore(window.dice.random < 0.3 ? lifebinder : 0)
  }

  defend(damage, source) {
    const accuracy = source.accuracy
    const diff = (accuracy - this.evasion) * 100
    const crit = 100 - diff
    const hit = crit - 50
    const graze = hit - 30
    const roll = window.dice.random * 100
    if (roll > crit) {
      damage *= source.compute('might')
    } else if (roll > hit) {
      damage *= 1
    } else if (roll > graze) {
      damage *= 0.5
    } else {
      damage = 0
    }
    const incoming = damage * (1 - this.armor(source))
    const counter = this.compute('counter') > window.dice.random ? 0.5 : 1
    const finalize = incoming * counter
    this.health -= finalize
    const thorns = this.compute('thorns') * incoming
    source.health -= thorns + (1 - counter) * source.intensity(this)
    return finalize
  }

  afflict(target, cause, effect) {
    const metric = this.compute(cause)
    // takes five rounds to apply maximum effect
    let taker = metric / 5
    // cap maximum reduction at computed metric
    target.mod[effect] = Math.max(target.mod[effect] - taker, -metric)
    return taker
  }

  steal(target, cause, effect) {
    const stolen = this.afflict(target, cause, effect)
    // cap maximum bonus at computed metric
    this.mod[effect] = Math.min(this.mod[effect] + stolen, this.compute(cause))
    return stolen
  }

  afflictions(target) {
    this.afflict(target, 'attrition', 'evasion')
    this.afflict(target, 'decay', 'armor')
    this.afflict(target, 'ruin', 'endurance')
    this.afflict(target, 'wither', 'vitality')
    target.mod.sanity -= this.compute('madness')
    this.afflict(target, 'despair', 'potency')
    this.afflict(target, 'doubt', 'accuracy')
    this.afflict(target, 'dampen', 'might')
    this.afflict(target, 'sloth', 'haste')
    this.afflict(target, 'agony', 'regen')
    this.afflict(target, 'corrupt', 'siphon')
    this.afflict(target, 'omen', 'luck')
    this.steal(target, 'reaper', 'potency')
    this.steal(target, 'thief', 'accuracy')
    this.steal(target, 'harvest', 'haste')
    this.steal(target, 'augur', 'luck')
    this.steal(target, 'leech', 'evasion')
    this.steal(target, 'devour', 'endurance')
    this.steal(target, 'absorb', 'siphon')
    target.incapacitated =
      target.incapacitated || this.compute('stun') > window.dice.random
  }

  siphoning(before, after) {
    const pact = this.compute('siphon')
    // payment is taken before; credit is taken after
    const output = pact > 0 ? after : before
    const price = pact * output
    this.restore(price)
    return price
  }

  attack(target) {
    // damage before defenses
    const raw = this.intensity(target) * (1 - this.compute('wisdom') * 0.5)
    // damage after defenses
    const damage = target.defend(raw, this)
    // apply siphoning
    this.siphoning(raw, damage)
    // apply afflictions
    if (damage > 0) {
      this.afflictions(target)
    }
  }

  restore(healing) {
    const healed = healing * this.vitality
    this.health += healed
    return healed
  }

  heal(target) {
    const healing = this.intensity(target)
    const healed = target.restore(healing)
    const siphoned = Math.max(this.siphoning(healing, healed), 0)
    target.health -= siphoned
  }

  buffs() {
    this.allies.forEach(ally => {
      ally.mod.evasion += this.compute('shield')
      ally.mod.armor += this.compute('fortitude')
      ally.mod.endurance += this.compute('stamina')
      ally.mod.vitality += this.compute('bloom')
      ally.mod.regen += this.compute('spirit')
      ally.mod.potency += this.compute('intellect')
      ally.mod.accuracy += this.compute('dexterity')
      ally.mod.might += this.compute('strength')
      ally.mod.haste += this.compute('warp')
      ally.mod.luck += this.compute('fortune')
    })
  }

  reset() {
    Object.keys(this.stats).forEach(k => {
      this.mod[k] = 0
    })
    this.incapacitated = false
  }

  ai() {
    let allies = this.allies
    let enemies = this.enemies
    const sanity = this.compute('sanity')
    // below zero sanity allies and enemies are swapped
    if (sanity < 0) {
      enemies = this.allies
      allies = this.enemies
      // below half sanity allies and enemies are ambiguous
    } else if (sanity < 0.5) {
      enemies = this.enemies.concat(this.allies)
      allies = this.enemies.concat(this.allies)
    }
    // skip action if dead or stunned
    if (!this.incapacitated && this.health > 0) {
      // heal most injured allies first
      const injured = allies
        .filter(a => a.health > 0 && a.health < a.maxHealth)
        .sort((a, b) => a.health - b.health)
      // TODO: better targeting AI
      enemies = window.dice.shuffle(enemies.filter(a => a.health > 0))
      // start with a single target
      let aoe = 1
      // add multiple targets if aoe is triggered
      if (window.dice.random > this.compute('cleave')) {
        aoe = window.dice.randint(2, 6)
        const spread = mapValue([2, 6], [0.4, 0.7], aoe)
        this.mod.intensity = -this.stats.intensity * spread
      }
      // take action on each target
      // TODO: add stalemates
      if (injured.length > 0 && this.compute('wisdom') > window.dice.random) {
        injured.slice(0, aoe).forEach(ally => this.heal(ally))
      } else {
        enemies.slice(0, aoe).forEach(enemy => this.damage(enemy))
      }
      // reset aoe malus
      this.mod.intensity = 0
      // regenerate health if possible
      this.regenerate()
    } else {
      // stuns only last one round
      this.incapacitated = false
      // if dead, attempt to ressurect
      if (this.health <= 0 && this.compute('lives') > 0) {
        this.health = this.maxHealth * this.compute('rebirth')
        this.mod.lives -= 1
      }
    }
    // add recover time to set your place in the queue
    this.mod.recovery += this.stats.recovery * (1 - this.compute('haste'))
  }
}

export default Attributes
