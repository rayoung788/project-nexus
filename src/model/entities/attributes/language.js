import { settlements } from '@/model/utilities/constants'
import { range, title } from '@/model/utilities/common'

class Cluster {
  constructor({src, key, len, ending, restrict = 1000}) {
    this.restrict = restrict
    this.src = src
    this.key = key
    this._len = len
    this.name = ['male', 'female'].includes(key)
    this.patterns = {
      V: window.dice.choice(['VC', 'VCV']),
      C: window.dice.choice(['CV', 'CVC'])
    }
    this.ending = ending
    this.morphemes = {}
    this.cache = []
    this.used = []
  }

  get len() {
    return this._len || this.src.len
  }

  get stop_chance() {
    return this.src.stop_chance
  }

  get stop_melodic() {
    return this.src.stop_melodic
  }

  get stop_injects() {
    return this.src.stop_injects
  }

  get mapping() {
    return this.src.phonemes
  }

  get uniques() {
    return this.src.uniques
  }

  get rules() {
    return this.src.rules
  }

  get details() {
    const groups = {
      prefix: [],
      suffix: []
    }
    Object.entries(this.morphemes).forEach(([k, v]) => {
      const group = 'AB'.includes(k[0]) ? 'prefix' : 'suffix'
      v.filter(m => m !== '*').forEach(m => groups[group].push(m.join('')))
    })
    return `${Object.entries(groups)
      .map(([k, v]) => `${k}: ${[...new Set(v)].sort().join(', ')}`)
      .join(' | ')} [${this.patterns.C.length + this.patterns.V.length}]`
  }

  recordUniques(chosen) {
    if (this.uniques[chosen]) {
      this.used = this.uniques[chosen].reduce(
        (sum, r) => sum.concat(this.rules[r]),
        this.used
      )
    }
  }

  reset() {
    this.cache = []
    this.used = []
  }

  hasMorph(template, morph) {
    return this.morphemes[template]
      .filter(m => m !== '*')
      .some(m => m.join('') === morph)
  }

  syllable(template) {
    // create a new syllable from template
    return template.split('').map(c => {
      // no action for stop characters (they can only appear once)
      if (c === 'ʔ') {
        return this.mapping.ʔ
      }
      // get the valid character set (non-unique or non-used characters)
      const valid = this.mapping[c].filter(m => !this.used.includes(m))
      // pick a random character from the valid set
      const idx = ~~(
        window.dice.random *
        (valid.length > 0 ? valid.length : this.mapping[c].length)
      )
      // use all characters if there are none left
      const chosen = (valid.length > 0 ? valid : this.mapping[c])[idx]
      // add unqiue characters to the used list
      this.recordUniques(chosen)
      return chosen
    })
  }

  morpheme(template) {
    // create morpheme distribution for pattern template if one doesn't already exist
    if (!this.morphemes[template]) {
      // each '*' is a chance to create a new morpheme
      this.morphemes[template] = ['*']
      // greater chance to create new morphemes for start sequences
      if ('AB'.includes(template[0])) {
        this.morphemes[template] = range(6).map(() => '*')
      }
    }
    // valid morphemes havent been already used and don't include used unqiue characters
    const valid = this.morphemes[template].filter(m => {
      return !this.cache.includes(m) && this.used.every(u => !m.includes(u))
    })
    const idx = ~~(window.dice.random * valid.length)
    let prospect = valid[idx] || '*'
    // create a new morpheme if there are no valids or '*' is chosen
    if (prospect === '*') {
      // create new syllable
      prospect = this.syllable(template)
      // add syllable to morpheme pattern list
      !this.hasMorph(template, prospect.join('')) &&
        this.morphemes[template].push(prospect)
      // restrict endings after a threshold is reached
      if (
        !'AB'.includes(template[0]) &&
        this.morphemes[template].length >= this.restrict
      ) {
        this.morphemes[template] = this.morphemes[template].filter(m => m !== '*')
      }
    } else {
      // record unique characters for chosen morpheme
      prospect.forEach(chosen => this.recordUniques(chosen))
    }
    // add morpheme to used morpheme list
    this.cache.push(prospect)
    return prospect
  }
  // create a new word
  word() {
    // pick a pattern from the selected group
    const next = {
      V: 'C',
      C: 'V'
    }
    const len = this.len
    let prev = window.dice.choice('VC')
    let pattern = [...Array(len).keys()].map(() => {
      prev = this.patterns[next[prev.slice(-1)]]
      return prev
    })
    const e = pattern.length - 1
    let ending = pattern[e].slice(-1)
    // append proper ending char
    if (this.ending && this.ending !== ending) {
      pattern[e] += this.ending
    }
    // replace with proper start|end consonant
    ending = pattern[e].slice(-1)
    if (ending === 'C') {
      pattern[e] = `${pattern[e].slice(0, -1)}F`
    }
    let start = pattern[0][0]
    if (start === 'V') {
      pattern[0] = window.dice.random > 0.15 ? `B${pattern[0]}` : `A${pattern[0].slice(1)}`
    }
    start = pattern[0][0]
    if (start === 'C') {
      pattern[0] = `B${pattern[0].slice(1)}`
    }
    // special stop character injection
    const injects = window.dice.random < this.stop_chance ? this.stop_injects : this.stop_injects - 1
    // only apply to first names
    if (this.name && injects > 0) {
      const inject = this.len < 3 ? [1] : injects > 1 ? [1, 2] : [window.dice.choice([1, 2])]
      let offset = 0
      inject.forEach(idx => {
        const i = idx + offset
        pattern.splice(i, 0, 'ʔ')
        // make sure stops split consonants if desired
        if (this.stop_melodic) {
          if (pattern[i - 1].slice(-1)[0] !== 'C') {
            pattern[i - 1] = `${pattern[i - 1]}C`
          }
          if (pattern[i + 1][0] !== 'C') {
            pattern[i + 1] = `C${pattern[i + 1]}`
          }
        }
        offset += 1
      })
    }
    // sub in middle vowels
    pattern = pattern.map((p, i) => i > 0 && i < pattern.length - 1 ? p.replace(/V/g, 'E') : p)
    pattern[0] = pattern[0].replace(/VCV/g, 'VCE').replace(/ACV/g, 'ACE')
    pattern[pattern.length - 1] = pattern[pattern.length - 1].replace(/VCV/g, 'ECV')
    // create the word from cluster defined morphemes (reverse order favors double vowels at the end)
    const word = pattern
      .reverse()
      .map(template => this.morpheme(template))
      .reverse()
      .reduce((sum, morph) => sum.concat(morph), [])
    // reset cluster for next word
    this.reset()
    // finalize word
    return title(word.join(''))
  }
}

class Language {
  constructor() {
    // written letters for each 'sound'
    const orthography = {
      ŋ: 'ng',
      ʃ: 'sh',
      ʧ: 'ch',
      ð: 'th',
      ...window.dice.choice([
        {A: window.dice.choice(['ä', 'å']), E: 'ë', I: 'ï', O: window.dice.choice(['ø', 'ö']), U: 'ü'},
        {A: 'á', E: 'é', I: 'í', O: 'ó', U: 'ú'},
        {A: 'â', E: 'ê', I: 'î', O: 'ô', U: 'û'}
      ])
    }
    // number of syllables per word (default)
    this.len = window.dice.choice([2, 2, 3])
    // chance to pick patterns with stop letters (only applies to first names)
    this.stop_chance = window.dice.choice([0, 0, 0, 0, 0.3, 0.6, 0.9])
    // chance to break melodic pattern (with stop sequences)
    this.stop_melodic = this.stop_chance > 0 && window.dice.random > 0.5
    // number of stop characters
    this.stop_injects = this.len < 3 || this.stop_chance === 0 ? 1 : window.dice.choice([1, 1, 1, 1, 2])
    // decide phonemes
    const basic = window.dice.sample('llmmnnnrrrsssŋ'.split(''), 3)
    const consonants = window.dice.sample('bcdfghjklmnpqrstvwxzcdghklmnrstvzʃʧðð'.split(''), 10)
    const all = consonants.concat(basic)
    const start = [
      'bh', 'bj', 'bl', 'br', 'br',
      'ch', 'ch', 'chr', 'cv', 'cz',
      'dh', 'dj', 'dr', 'dr', 'dz',
      'fr',
      'gh', 'gj', 'gn', 'gr', 'gw', 'gx',
      'hj', 'hl', 'hr', 'hv', 'h', 'h',
      'kh', 'kj', 'kn', 'kr',
      'mh', 'm', 'm', 'n', 'n',
      'ph', 'pr',
      'qh', 'qn', 'qr',
      'rh', 'r', 'r', 's', 's',
      'sh', 'sh', 'sh', 'shn', 'sk', 'skj', 'st', 'sv',
      'th', 'th', 'th', 'th', 'thr', 'tr', 'ts', 'tz',
      'vh', 'vl', 'vl', 'vr', 'v', 'v',
      'wr',
      'xh', 'x',
      'zh', 'zh', 'zr', 'zv', 'z'
    ].filter(s => s.split('').every(c => all.includes(c)))
    const mid = [
      'br',
      'ch', 'chr', 'cc',
      'dh', 'dl', 'dj', 'dr', 'dz', 'dd', 'ddr', 'd',
      'fg', 'fj', 'ff',
      'gg', 'gh', 'ghr', 'gj', 'gm', 'gmm', 'gn', 'ggr', 'gr', 'gw', 'gv', 'gx', 'gz',
      'hj', 'hrj', 'hk', 'hl', 'hr', 'hm', 'h', 'h', 'j', 'jj', 'jr',
      'kh', 'kj', 'kk', 'kl', 'kn', 'kr', 'ks', 'kw', 'k',
      'ld', 'lf', 'lh', 'll', 'll', 'ln', 'lm', 'ls', 'lt', 'lth', 'lv', 'lv', 'lx', 'lz',
      'mm', 'mm', 'mh', 'mph', 'mphr', 'mz', 'mr', 'm', 'm',
      'nn', 'nn', 'nj', 'nh', 'nm', 'nsk', 'nk', 'nc', 'nx', 'nd', 'nd', 'ng', 'ng', 'nph', 'nv', 'nz', 'nt', 'nth', 'nr',
      'n', 'n', 'ph',
      'qh', 'qq', 'qr', 'qn',
      'rc', 'rk', 'rkh', 'rq', 'rh', 'rd', 'rg', 'rl', 'rm', 'rn', 'rr', 'rv', 'rz', 'rx', 'rj', 'rsh', 'rsk', 'rth', 'r', 'r', 's', 's',
      'sh', 'shr', 'shn', 'shk', 'shj', 'sk', 'ss', 'ss', 'sr', 'st', 'str', 'sch',
      'th', 'th', 'thl', 'thr', 'tr', 'tt', 'tz', 'ts',
      'vh', 'vn', 'vv', 'vr', 'v', 'v',
      'wr', 'ww',
      'xr', 'xx', 'xh', 'x',
      'zh', 'zm', 'zs', 'zr', 'zz', 'zsh', 'zj', 'z'
    ].filter(s => s.split('').every(c => all.includes(c)))
    const vowels = window.dice.sample('aaeiou'.split(''), window.dice.randint(3, 5))
    const complex = window.dice.sample([
    'aa', 'ae', 'ai', 'ae', 'au', 'a', 'A', 'A', 'A', 'A', 'Ae', 'aA',
    'ea', 'ee', 'ei', 'eo', 'eu', 'e', 'E', 'E', 'E', 'E', 'aE', 'eA',
    'ia', 'ie', 'ii', 'io', 'iu', 'i', 'I', 'I', 'I', 'I',
    'oa', 'oo', 'ou', 'o', 'y', 'y', 'O', 'O', 'O', 'O',
    'ua', 'ue', 'ui', 'uo', 'uu', 'u', 'U', 'U', 'U', 'U',
    'aa', 'ae', 'ai', 'ae', 'au', 'a', 'A', 'A', 'A', 'A',
    'ea', 'ee', 'ei', 'eo', 'eu', 'e', 'E', 'E', 'E', 'E',
    'ia', 'ie', 'ii', 'io', 'iu', 'i', 'I', 'I', 'I', 'I',
    'oa', 'oo', 'ou', 'o', 'y', 'y', 'O', 'O', 'O', 'O',
    'ua', 'ue', 'ui', 'uo', 'uu', 'u', 'U', 'U', 'U', 'U']
        .filter(v => v
            .toLowerCase()
            .split('')
            .some(c => vowels.includes(c) || c === 'y'))
        .concat(['ya', 'yi', 'ye', 'yo', 'yu'].filter(v =>
            vowels.includes(v[1])
          )), window.dice.randint(0, 5))
    const all_vowels = vowels.concat(complex)
    const dv = ['aa', 'ee', 'ii', 'oo', 'uu']
    this.phonemes = {
      // start consonants
      B: consonants.concat(window.dice.sample(start, window.dice.randint(0, 4))),
      // middle consonants
      C: consonants.concat(window.dice.sample(mid, window.dice.randint(0, 4))).concat(basic),
      // end consonants
      F: basic.concat(window.dice.sample(consonants, window.dice.randint(0, 4))).concat(window.dice.sample(mid, window.dice.randint(0, 2))),
      // standard vowels
      V: all_vowels,
      // middle vowels
      E: all_vowels.filter(v => v.length < 2),
      // start vowels
      A: all_vowels.filter(v => !dv.includes(v)),
      // stop character
      ʔ: this.stop_chance > 0 ? window.dice.weightedChoice(['\'', '-'], [0.75, 0.25]) : '$'
    }
    // decide article / title joiner
    this.joiner = this.phonemes.ʔ === '$' ? ' ' : this.phonemes.ʔ
    // replace placeholder orthography
    this._vowels = this.phonemes.V.map(v => v.toLowerCase())
    Object.keys(this.phonemes).filter(k => k !== 'ʔ').forEach(k => {
      this.phonemes[k] = this.phonemes[k].map(c => c.split('').map(l => orthography[l] || l).join(''))
    })
    this._consonants = this.phonemes.B.concat(this.phonemes.C).concat(this.phonemes.F)
    // unique names
    this.names = {
      common: [],
      noble: [],
      'great house': [],
      locations: []
    }
    // generate unqiueness rules
    this.uniqueness()
    // word clusters: each cluster has similar words
    this._clusters = {
      settlement: new Cluster({src: this, key: 'settlement', ending: 'C', restrict: 3}),
      region: new Cluster({src: this, key: 'region', ending: 'C'}),
      male: new Cluster({src: this, key: 'male', ending: 'C'}),
      female: new Cluster({src: this, key: 'female', ending: 'V'}),
      last: new Cluster({src: this, key: 'last'}),
      the: new Cluster({src: this, key: 'the', len: 1}),
      title: new Cluster({src: this, key: 'title', len: 1})
    }
    // similar first names
    this._clusters.female.patterns = this._clusters.male.patterns
    // patronymic surrnames
    this.patronymic = window.dice.random > 0.85
    if (this.patronymic) {
      this.append = window.dice.random > 0.65
      this._clusters.title.patterns = {
        V: 'VCʔV',
        C: 'CVʔC'
      }
      const suffix = this.word('title').split(this.phonemes.ʔ)
      const base = this.append ? suffix[0] : suffix[0].toLowerCase()
      const end = suffix[1].toLowerCase()
      this.suffix = {
        male: `${base}${this.phonemes.C.includes(end) ? end : ''}`,
        female: `${base}${this.phonemes.V.includes(end) ? end : ''}`
      }
    }
    // chance to add a title to last names
    this.title_chance = this.patronymic ? 0 : window.dice.choice([0, window.dice.uniform(0, 0.2)])
    // chance to add an article to settlement names
    this.article_chance = window.dice.random > 0.9
    this.article_type = window.dice.choice([settlements.city, settlements.town])
    // create title & article words
    this.predefined = {
      the: this.word('the'),
      title: this.word('title')
    }
    // allow chance for one-letter titles
    const valid = 'DGJKLMNQRSTVXYZ'.split('').filter(c => all.includes(c.toLowerCase()))
    if (this.phonemes.ʔ === '\'' && valid.length > 0 && window.dice.random > 0.8) {
      this.predefined.title = window.dice.choice(valid)
    }
  }

  get details() {
    const c = this.phonemes.C.sort()
    const v = this.phonemes.V.sort()
    const b = this.phonemes.B.sort()
    const f = this.phonemes.F.sort()
    return {
      sounds: [
        `Consonants (B): ${b.join(', ')}`,
        `Consonants (C): ${c.join(', ')}`,
        `Consonants (F): ${f.join(', ')}`,
        `Vowels (V): ${v.join(', ')}`
      ],
      morphemes: Object.entries(this._clusters).map(([key, cluster]) => `${title(key)}: ${cluster.details}`),
      statistics: [
        `Syllables: ${this.len}`,
        `Title chance: ${(this.title_chance * 100).toFixed(2)}%`,
        `Stop chance: ${(this.stop_chance * 100).toFixed(2)}% (${this.stop_injects})`,
        `Articles: ${this.article_chance}`,
        `Patronymic: ${this.patronymic}`
      ]
    }
  }

  uniqueness() {
    // generate uniqueness rules
    const consonants = Array.from(new Set([...this.phonemes.B, ...this.phonemes.C, ...this.phonemes.F]))
    const dv = Array.from(new Set(this.phonemes.V)).filter(m => m.length > 1)
    const standard = 'aeiou' + (window.dice.random > 0.6 ? 'y' : '')
    const sv = Array.from(new Set(this.phonemes.V)).filter(m => m.length < 2 && !standard.includes(m))
    const dc = consonants.filter(m => m.length > 1)
    const basic = 'khzwgtmv'.split('').concat(dc)
    const inclusive = [
      'c', 'b', 'd', 'f', 'g', 'l', 'j', 'm', 'q', 'p', 'r',
      'x', 'y', 'ph', 'th', 'ch', 'sh', 'sk', 'gg'
    ]
    this.rules = {
      ...basic.reduce((sum, c) => {
        return { ...sum, [c]: [c] }
      }, {}),
      ...inclusive.reduce((sum, c) => {
        return {
          ...sum,
          [c]: consonants.filter(m => m.includes(c))
        }
      }, {}),
      ...sv.reduce((sum, c) => {
        return { ...sum, [c]: [c] }
      }, {}),
      _vv_: dv
    }
    this.uniques = basic.reduce((sum, c) => {
      return { ...sum, [c]: [c] }
    }, {})
    inclusive.forEach(c => {
      consonants.filter(m => m.includes(c)).forEach(s => {
        !this.uniques[s] && (this.uniques[s] = [])
        !this.uniques[s].includes(c) && this.uniques[s].push(c)
      })
    })
    dv.forEach(s => {
      !this.uniques[s] && (this.uniques[s] = [])
      this.uniques[s].push('_vv_')
    })
    sv.forEach(s => {
      !this.uniques[s] && (this.uniques[s] = [])
      this.uniques[s].push(s)
    })
  }

  culturize(entity) {
    if (this.patronymic) {
      entity._last = entity.last
      entity.last = this.append
        ? `${this.suffix[entity.gender]}${this.joiner}${entity.parent.first}`
        : `${entity.parent.first}${this.suffix[entity.gender]}`
    }
  }

  // create a new word
  word(key) {
    // create a new cluster if one doesnt already exist
    !this._clusters[key] && (this._clusters[key] = new Cluster({ src: this, key }))
    return this._clusters[key].word()
  }

  isUnique(name) {
    return !Object.values(this.names).some(names => names.some(n => n === name))
  }

  applyTitle(name) {
    if (this.predefined.title.length > 1) {
      if (this.joiner === '-') name = name.toLowerCase()
      return `${this.predefined.title}${this.joiner}${name}`
    }
    return `${this.predefined.title}'${name.toLowerCase()}`
  }

  male() {
    return this.word('male')
  }

  female() {
    return this.word('female')
  }

  first(key) {
    return key === 'male' ? this.male() : this.female()
  }

  last(type = 'common') {
    // duplication chances for different name sets
    const duplicate = type === 'common' ? 0.6 : type === 'noble' ? 0.2 : 0.05
    // pick a already made name if duplication is high enough
    if (window.dice.random < duplicate && this.names[type].length > 0) {
      return window.dice.choice(this.names[type])
    }
    // otherwise create a new name
    let name = this.word('last')
    // give titles higher chance for 'noble' names
    const title = type !== 'common' ? this.title_chance * 2 : this.title_chance
    if (window.dice.random < title) {
      name = this.applyTitle(name)
    }
    // repeat recursively until a unqiue name is found
    if (!this.isUnique(name)) {
      // console.log(`duplicate detected: ${type} lastname, ${name}`)
      return this.last(type)
    }
    // add name to the unique name list
    this.names[type].push(name)
    return name
  }

  city(name, subject) {
    if (this.article_chance || name.split(this.joiner).length > 1) {
      return name
    }
    if (
      subject.type === settlements.town &&
      window.dice.random > 0.95 &&
      subject.port
    ) {
      // small chance to append port to the title
      return `Port ${name}`
    }
    const culture = subject.culture
    if (subject.type === settlements.town && subject.urban < culture.titles.village.cutoff) {
      // small chance to append village to the title
      return culture.titles.village.title.replace('%', name)
    }
    if (!culture.tribal) {
      if (subject.type === settlements.city && window.dice.random > 0.9) {
        // small chance to append city to the title
        return `${name} City`
      }
      if (subject.type === settlements.capital && window.dice.random > 0.95 && !subject.region.divided) {
        // small chance to name city after the region
        return `${subject.region.name} City`
      }
    }
    return name
  }

  appyArticle(type, words) {
    type = type === settlements.capital ? settlements.city : type
    // add an article (on chance)
    if (this.article_chance && window.dice.random < 0.5 && type === this.article_type) {
      words.splice(0, 0, this.predefined.the)
      if (this.joiner === "'") {
        words[1] = words[1].toLowerCase()
      }
    }
    // join article and word with joiner
    return words.join(this.joiner)
  }

  settlement(subject) {
    // create the base settlement name
    let words = [this.word('settlement')]
    // add an article (on chance)
    let word = this.appyArticle(subject.type, words)
    // repeat recursively until a unqiue name is found
    if (!this.isUnique(word)) {
      // console.log(`duplicate detected: Settlement, ${word}`)
      return this.settlement(subject)
    }
    // add name to the unique name list
    this.names.locations.push(word)
    return this.city(word, subject)
  }

  generic(key) {
    let word = this.word(key)
    if (!this.isUnique(word)) {
      // console.log(`duplicate detected: ${key}, ${word}`)
      return this.generic(key)
    }
    this.names.locations.push(word)
    return word
  }

  region() {
    return this.generic('region')
  }

  island(size) {
    const name = this.generic('island')
    let template = `Continent of ${name}`
    // its an island if below a certain threshold
    if (size < 0.05) {
      const term = window.dice.random < 0.3 ? 'Isle' : 'Island'
      template = window.dice.random < 0.3 ? `${term} of ${name}` : `${name} ${term}`
    }
    return template
  }

  lake() {
    const name = this.generic('lake')
    return `${name} Lake`
  }

  river() {
    const name = this.generic('river')
    return `${name} River`
  }

  mountain(size) {
    const name = this.generic('mountain')
    // singular if theres only one
    const desc = size < 2 ? 'Mountain' : 'Mountains'
    return `${name} ${desc}`
  }
}

export { Language, Cluster }
