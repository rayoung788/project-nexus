import { mapTier } from '@/model/utilities/common'

const low = [0.05, 0.25]
const high = low.map(v => v * 2)
const chance = [0.1, 0.3]
const vitality = [0.2, 0.4]
const might = [0.5, 1]
const buffs = low

export default {
  // defensive
  health: {
    desc: x => `Base life: ${x.toFixed(2)}`
  },
  evasion: {
    // debt: { potency: 1 },
    scale: mapTier(low),
    desc: x => `Chance to avoid damage: ${(x * 100).toFixed(2)}%`
  },
  endurance: {
    // debt: { potency: 1 },
    scale: mapTier(low),
    desc: x => `Increases max health by ${(x * 100).toFixed(2)}%`
  },
  armor: {
    // debt: { potency: 1 },
    scale: mapTier(low),
    desc: x => `Decreases damage taken by ${(x * 100).toFixed(2)}%`
  },
  stun: {
    // debt: { potency: 1 },
    scale: mapTier(chance),
    desc: x => `Chance to stun: ${(x * 100).toFixed(2)}%`
  },
  siphon: {
    // debt: { potency: 0.25, armor: 0.25 },
    scale: mapTier(vitality),
    desc: x => `Steals life on hit: ${(x * 100).toFixed(2)}%`
  },
  thorns: {
    // debt: { potency: 0.5 },
    scale: mapTier(chance),
    desc: x => `Damage dealt on hit: ${(x * 100).toFixed(2)}%`
  },
  // healing
  wisdom: {
    // debt: { armor: 0.5 },
    scale: mapTier([0.4, 0.6]),
    desc: x => `Chance to heal: ${(x * 100).toFixed(2)}%`
  },
  regen: {
    // debt: { potency: 1 },
    scale: mapTier(low),
    desc: x => `30% chance to restore ${(x * 100).toFixed(2)}% health`
  },
  vitality: {
    // debt: { potency: 0.25 },
    scale: mapTier(vitality),
    desc: x => `Increases how much you are healed by ${(x * 100).toFixed(2)}%`
  },
  rebirth: {
    // debt: { potency: 0.5 },
    scale: mapTier(chance),
    desc: x => `Health restored on ressurect: ${(x * 100).toFixed(2)}%`
  },
  lives: {
    scale: mapTier([1, 1]),
    desc: x => `Number of ressurections: ${x.toFixed(2)}`
  },
  // defensive - situational
  glory: {
    scale: mapTier(high),
    desc: x =>
      `Decreases damage taken when target is at high health by up to ${(x * 100).toFixed(2)}%`
  },
  victory: {
    scale: mapTier(high),
    desc: x =>
      `Decreases damage taken when target is at low health by up to ${(x * 100).toFixed(2)}%`
  },
  bastion: {
    scale: mapTier(high),
    desc: x =>
      `Decreases damage taken when you are at high health by up to ${(x * 100).toFixed(2)}%`
  },
  guardian: {
    scale: mapTier(high),
    desc: x => `Decreases damage taken when you are at low health by up to ${(x * 100).toFixed(2)}%`
  },
  warding: {
    scale: mapTier(high),
    desc: x => `Decreases damage taken against magic sources by ${(x * 100).toFixed(2)}%`
  },
  fiend: {
    scale: mapTier(high),
    desc: x => `Decreases damage taken against non-magic sources by ${(x * 100).toFixed(2)}%`
  },
  // offensive
  recovery: {
    desc: x => `Recover time before the next action: ${x.toFixed(2)}`
  },
  cleave: {
    debt: { potency: 0.5 },
    scale: mapTier(chance),
    desc: x => `Chance to aoe: ${(x * 100).toFixed(2)}%`
  },
  intensity: {
    desc: x => `Base damage: ${x.toFixed(2)}`
  },
  potency: {
    scale: mapTier(low),
    desc: x => `Increases damage by ${(x * 100).toFixed(2)}%`
  },
  accuracy: {
    // debt: { armor: 1 },
    scale: mapTier(low),
    desc: x => `Chance to hit: ${(x * 100).toFixed(2)}%`
  },
  might: {
    // debt: { armor: 0.25 },
    scale: mapTier(might),
    desc: x => `Critical hit damage: ${(x * 100).toFixed(2)}%`
  },
  haste: {
    // debt: { armor: 1 },
    scale: mapTier(low),
    desc: x => `Decreases recovery speed by ${(x * 100).toFixed(2)}%`
  },
  counter: {
    // debt: { armor: 0.5 },
    scale: mapTier(chance),
    desc: x => `Chance to redirect damage: ${(x * 100).toFixed(2)}%`
  },
  // offensive - sacrifice
  pact: {
    debt: { siphon: 0.5 },
    credit: { potency: 1 },
    scale: mapTier(high),
    desc: x => `Increases damage dealt by ${(x * 100).toFixed(2)}% at the debt of health`
  },
  vessel: {
    debt: { armor: 1 },
    credit: { potency: 1 },
    scale: mapTier(high),
    desc: x => `Increases damage dealt and damage taken by ${(x * 100).toFixed(2)}%`
  },
  glass: {
    debt: { endurance: 1 },
    credit: { potency: 1 },
    scale: mapTier(high),
    desc: x => `Increases damage by ${(x * 100).toFixed(2)}% at the debt of endurance`
  },
  // offensive - situational
  envy: {
    scale: mapTier(high),
    desc: x => `Increases damage when target is at high health by up to ${(x * 100).toFixed(2)}%`
  },
  havoc: {
    scale: mapTier(high),
    desc: x => `Increases damage when target is at low health by up to ${(x * 100).toFixed(2)}%`
  },
  pride: {
    scale: mapTier(high),
    desc: x => `Increases damage when you are at high health by up to ${(x * 100).toFixed(2)}%`
  },
  vengeance: {
    scale: mapTier(high),
    desc: x => `Increases damage when you are at low health by up to ${(x * 100).toFixed(2)}%`
  },
  lore: {
    scale: mapTier(high),
    desc: x => `Increases damage against magic targets by ${(x * 100).toFixed(2)}%`
  },
  hunter: {
    scale: mapTier(high),
    desc: x => `Increases damage against non-magic targets by ${(x * 100).toFixed(2)}%`
  },
  // afflictions - offensive
  attrition: {
    // debt: { potency: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases evasion on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  decay: {
    // debt: { potency: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases armor on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  ruin: {
    // debt: { potency: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases endurance on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  wither: {
    // debt: { potency: 0.25 },
    scale: mapTier(vitality.map(v => v * 2)),
    desc: x => `Decreases vitality on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  madness: {
    // debt: { potency: 1 },
    scale: mapTier([0.15, 0.3]),
    desc: x => `Decreases sanity on hit by ${(x * 100).toFixed(2)}%`
  },
  // afflictions - defensive
  despair: {
    // debt: { armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases potency on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  doubt: {
    // debt: { armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases accuracy on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  dampen: {
    // debt: { armor: 0.25 },
    scale: mapTier(might.map(v => v * 2)),
    desc: x => `Decreases might on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  sloth: {
    // debt: { armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases haste on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  corrupt: {
    // debt: { armor: 0.5 },
    scale: mapTier(vitality.map(v => v * 2)),
    desc: x => `Decreases siphon on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  agony: {
    // debt: { armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases regen on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  omen: {
    // debt: { armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Decreases luck on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  // reaping
  reaper: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(low),
    desc: x => `Steals potency on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  thief: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(low),
    desc: x => `Steals accuracy on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  harvest: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(low),
    desc: x => `Steals armor on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  augur: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(low),
    desc: x => `Steals luck on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  leech: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(low),
    desc: x => `Steals evasion on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  devour: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(low),
    desc: x => `Steals endurance on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  absorb: {
    // debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Steals siphon on hit: stacks up to ${(x * 100).toFixed(2)}%`
  },
  // blessings - offensive
  intellect: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party potency by ${(x * 100).toFixed(2)}%`
  },
  dexterity: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party accuracy by ${(x * 100).toFixed(2)}%`
  },
  strength: {
    debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Increases party might by ${(x * 100).toFixed(2)}%`
  },
  warp: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party haste by ${(x * 100).toFixed(2)}%`
  },
  fortune: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party luck by ${(x * 100).toFixed(2)}%`
  },
  // blessings - defensive
  shield: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party evasion by ${(x * 100).toFixed(2)}%`
  },
  fortitude: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party armor by ${(x * 100).toFixed(2)}%`
  },
  stamina: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party endurance by ${(x * 100).toFixed(2)}%`
  },
  bloom: {
    debt: { potency: 0.5, armor: 0.5 },
    scale: mapTier(high),
    desc: x => `Increases party vitality by ${(x * 100).toFixed(2)}%`
  },
  spirit: {
    debt: { potency: 1, armor: 1 },
    scale: mapTier(buffs),
    desc: x => `Increases party regen by ${(x * 100).toFixed(2)}%`
  },
  // misc
  chaos: {
    desc: x => `The hand of fate: ${(x * 100).toFixed(2)}%`
  },
  luck: {
    desc: x => `Influences fate: ${(x * 100).toFixed(2)}%`
  },
  sanity: {
    desc: x => `Friend or foe detection: ${(x * 100).toFixed(2)}%`
  }
}
