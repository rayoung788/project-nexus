import * as d3 from 'd3'

// placeholder ancestor node
class UnknownAncestor {
  constructor(idx, last, race) {
    // parent node
    this._parent = false
    // mark ancestor as unknown
    this.unknown = true
    // default age is 0
    this.age = 0
    // start with no child nodes
    this.children = []
    // index is a combo id
    this.idx = `Unknown Ancestor #${idx}`
    // default lastname
    this.last = last
    // default race
    this.race = race
    // planned gender
    this.gender = window.dice.choice(['male', 'female'])
    // planned firstname
    this.first = race.genName('first', this.gender)
  }
  get name() {
    return `${this.first} ${this.last}?`
  }
  get parent() {
    return this._parent
  }
  set parent(node) {
    this._parent = node
    // add this node as the child of the new parent
    if (this._parent) {
      this._parent.addChild(this)
    }
  }
  get siblings() {
    // siblings are the parent's children not including this node
    return this.parent.children.filter(s => s.idx !== this.idx)
  }
  addChild(node) {
    this.children.push(node)
  }
}

class Ancestry {
  constructor(last, culture) {
    // max number of children an entity can have (not including spouse additions)
    this.maxChildren = 3
    // lineage colors
    this.colors = window.dice.randomColors()
    // unqiue id for unknown ancestors
    this.caidx = 0
    // unkown ancestor list
    this.unknowns = {}
    // default culture for unkown ancestors
    this.culture = culture
    // default surrname for unkown ancestors
    this.surrname = last
  }

  get nodes() {
    // get all nodes in this tree
    const nodes = []
    // start with root nodes
    Object.values(this.unknowns)
      .filter(n => !n.parent)
      .forEach(n => {
        // set parent as root
        n.p = 'Root'
        // recursively traverse children
        this.traverse(n, nodes)
      })
    return nodes
  }

  get unknownNodes() {
    return Object.values(this.unknowns)
  }

  traverse(entity, nodes) {
    // add entity to nodes list
    nodes.push(entity)
    entity.children.forEach(c => {
      // set parent as the parent entity (needed because spouses make parent nodes ambiguous)
      c.p = entity.idx
      // recursively traverse children
      this.traverse(c, nodes)
    })
  }
  // replace a (unknown) entity
  fill(old, entity) {
    // give the old entity's parent to the new entity
    entity.parent = old.parent
    // give the old entity's age to the new entity
    entity.age = old.age
    // give the old entity's age to the new entity
    entity.first = old.first
    // have the old entity's children point to the new entity
    old.children.forEach(c => {
      c.parent = entity
    })
    // remove old entity from parent's child list if applicable
    if (old.parent) {
      old.parent.children = old.parent.children.filter(n => n !== old)
    }
    // remove unknown ancestor
    if (old.idx.includes('Unknown Ancestor')) {
      delete this.unknowns[old.idx]
    }
  }
  newUnknownAncestor(entity) {
    // create a new unknown ancestor
    const ca = new UnknownAncestor(this.caidx, this.surrname, this.culture)
    // give the ancestor an age to make it a valid parent of the child entity
    ca.age = entity.age + window.dice.randint(18, 26)
    // add it to the list of unknowns
    this.unknowns[ca.idx] = ca
    // increment unknown index for next unknown node
    this.caidx += 1
    return ca
  }

  validParent(parent, child) {
    // valid parents are between 18-30 years older than children and have empty child slots
    return (
      parent.age >= child.age + 18 &&
      parent.age < child.age + 30 &&
      parent.children.length < this.maxChildren
    )
  }

  addEntity(entity) {
    const age = entity.age
    const nodes = this.nodes
    // get prospective unknown ancestors that can be filled
    const unknowns = this.unknownNodes.filter(
      n =>
        n.gender === entity.gender &&
        n.age <= 60 &&
        age >= n.age - 3 &&
        age <= n.age + 3 &&
        n.children.every(c => c.age + 18 <= age)
    )
    // get valid parent nodes
    let prospects = nodes.filter(n => this.validParent(n, entity))
    if (unknowns.length > 0) {
      // first fill unknown ancestors if applicable
      const chosen = window.dice.choice(unknowns)
      this.fill(chosen, entity)
    } else if (prospects.length > 0) {
      // next add as a child node to a valid parent
      entity.parent = window.dice.choice(prospects)
      this.fixAge(entity)
    } else {
      // otherwise, this node's parent is unknown
      entity.parent = this.newUnknownAncestor(entity)
      let curr = entity.parent
      let reuse = false
      // recursively create unknown ancestors until we find a valid parent or we hit the root (age <= 60)
      while (curr.age <= 60 && !reuse) {
        prospects = nodes.filter(n => this.validParent(n, curr))
        reuse = prospects.length > 0
        curr.parent = reuse
          ? window.dice.choice(prospects)
          : this.newUnknownAncestor(curr)
        reuse && this.fixAge(curr)
        curr = curr.parent
      }
    }
  }

  fixAge(node) {
    // make sure children are not the same age (no twins)
    const siblings = node.siblings.map(n => n.age)
    // if a twin is found, find a new age that doesn't conflict
    if (siblings.includes(node.age)) {
      const bound = node.parent.age - 18
      const ages = d3.range(bound - 3, bound + 1).filter(i => !siblings.includes(i))
      node.age = window.dice.choice(ages)
    }
  }
}

export default Ancestry
