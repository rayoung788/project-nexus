const empty = { tier: 0 }
class Inventory {
  constructor(owner) {
    this.owner = owner
    this.stash = {
      armor: {},
      weapons: {},
      runes: {},
      alchemy: {},
      other: {}
    }
    this.equipment = {
      armor: {
        helmet: empty,
        armor: empty,
        gloves: empty,
        boots: empty
      },
      weapons: {
        mainhand: empty,
        offhand: empty
      },
      skills: {
        0: { tier: 0 },
        1: { tier: 0 },
        2: { tier: 0 }
      }
    }
    this.training = {
      // maps slots => item subtypes
      armor: {},
      weapons: {},
      skills: {}
    }
  }
  get weight() {
    const stashWeight = Object.values(this.stash).reduce((total, group) => {
      return (
        total +
        Object.values(group).reduce((weight, item) => weight + (item.weight || 0), 0)
      )
    }, 0)
    const equipedWeight = Object.entries(this.equipment).reduce(
      (total, [, group]) => {
        return (
          total +
          Object.values(group).reduce(
            (sum, equiped) => sum + (equiped.weight || 0),
            0
          )
        )
      },
      0
    )
    return stashWeight + equipedWeight
  }
  get itemLevel() {
    return (this.groupItemLevel('weapons') + this.groupItemLevel('armor')) / 2
  }
  get carryWeight() {
    return this.owner.carryWeight
  }
  groupItemLevel(group) {
    const egroup = Object.values(this.equipment[group]).filter(item => item.mod)
    return egroup.reduce((sum, equiped) => {
      return sum + equiped.tier * equiped.mod
    }, 0)
  }
  carry(item) {
    const current = this.weight
    return current + item.weight <= this.carryWeight
  }
  equip(item) {
    const valid = [item.slot, 'offhand'].some(slot =>
      this.training[item.type][slot].includes(item.subtype)
    )
    valid && item.equip(this)
  }
  unequip(item) {
    item.unequip(this)
  }
  equipBest() {
    Object.entries(this.equipment).forEach(([type, group]) => {
      Object.entries(group).forEach(([slot, equiped]) => {
        const best = Object.values(this.stash[type])
          .filter(
            item =>
              this.training[type][slot].includes(item.subtype) &&
              (item.slot === slot || slot === 'offhand')
          )
          .reduce((max, item) => {
            return item.tier > max.tier ? item : max
          }, equiped)
        if (equiped !== best) {
          equiped.name && this.unequip(equiped)
          best.name && this.equip(best)
        }
      })
    })
  }
}

export default Inventory
