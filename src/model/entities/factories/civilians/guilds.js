import NPCFactory from '@/model/entities/factories/factory'

class Guilds extends NPCFactory {
  // guilds
  get spawns() {
    return ['master', 'veteren']
  }
  master(args) {
    const ages = [40, 60]
    return this.spawn({ ...args, ages, status: [0.99, 0.8] })
  }
  veteren(args) {
    const ages = [30, 60]
    return this.spawn({ ...args, ages, status: [1, 0.8] })
  }
  apprentice(args) {
    const ages = [18, 30]
    return this.spawn({ ...args, ages, status: [1, 0.95] })
  }
}

export default Guilds
