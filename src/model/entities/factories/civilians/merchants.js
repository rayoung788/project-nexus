import NPCFactory from '@/model/entities/factories/factory'

const commerdist = [1, 0.95]
const nobledist = [0.99, 0.1]

class Merchants extends NPCFactory {
  get spawns() {
    return ['blacksmith', 'owner', 'apprentice', 'artisan', 'patron', 'noblePatron']
  }
  // shops
  blacksmith(args) {
    const ages = [30, 60]
    return this.spawn({ ...args, ages, status: commerdist })
  }
  owner(args) {
    const ages = [30, 60]
    return this.spawn({ ...args, ages, status: commerdist })
  }
  apprentice(args) {
    const ages = [18, 30]
    return this.spawn({ ...args, ages, status: commerdist })
  }
  artisan(args) {
    const ages = [25, 50]
    return this.spawn({ ...args, ages, status: commerdist })
  }
  patron(args) {
    const ages = [18, 60]
    return this.spawn({ ...args, ages, status: commerdist })
  }
  noblePatron(args) {
    const ages = [18, 60]
    return this.spawn({ ...args, ages, status: nobledist })
  }
}

export default Merchants
