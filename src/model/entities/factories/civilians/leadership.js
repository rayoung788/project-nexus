import NPCFactory from '@/model/entities/factories/factory'
import { romanize } from '@/model/utilities/common'

const noble = [1, 0]
const greatHouse = [0, 1]

class Leadership extends NPCFactory {
  get spawns() {
    return [
      'leader',
      'king',
      'advisor',
      'diplomat',
      'generic',
      'greatHouse',
      'noble',
      'child',
      'spouse'
    ]
  }
  // leadership
  leader(args) {
    const gender = window.dice.choice(['male', 'female'])
    const { location } = args
    // pick leadership title based on location population
    let profession = location.culture.titles.baron[gender]
    if (location.urban > 32000) {
      profession = location.culture.titles.archduke[gender]
    } else if (location.urban > 16000) {
      profession = location.culture.titles.duke[gender]
    } else if (location.urban > 8000) {
      profession = location.culture.titles.count[gender]
    }
    const ages = [40, 60]
    return this.spawn({ ...args, profession, gender, ages, status: greatHouse })
  }
  king(args) {
    const gender = window.dice.choice(['male', 'female'])
    const { location } = args
    let profession = location.culture.titles.king[gender]
    const ages = [40, 60]
    const idx = this.spawn({ ...args, profession, gender, ages, status: greatHouse })
    // TODO: propagate same name to ancestors
    const numeral = window.dice.random > 0.75 ? ` ${romanize(window.dice.randint(2, 10))}` : ''
    this.registry[idx].first = `${this.registry[idx].first}${numeral}`
    return idx
  }
  advisor(args) {
    const ages = [40, 60]
    return this.spawn({ ...args, ages, status: noble })
  }
  diplomat(args) {
    const ages = [40, 60]
    const { region, cultures } = args
    const culture = region.pickCulture(cultures)
    return this.spawn({ ...args, culture, ages, status: noble })
  }
}

export default Leadership
