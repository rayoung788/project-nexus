import NPCFactory from '@/model/entities/factories/factory'

class Military extends NPCFactory {
  get spawns() {
    return ['soldier', 'captain', 'general']
  }
  // military
  soldier(args) {
    const ages = [18, 40]
    return this.spawn({ ...args, ages, status: [1, 0.95] })
  }
  captain(args) {
    const ages = [35, 50]
    return this.spawn({ ...args, ages, status: [0.99, 0.9] })
  }
  general(args) {
    const ages = [40, 60]
    return this.spawn({ ...args, ages, status: [0.9, 0.2] })
  }
}

export default Military
