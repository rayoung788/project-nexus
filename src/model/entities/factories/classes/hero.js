import NPCFactory from '@/model/entities/factories/factory'

import ArmorFactory from '@/model/items/equipables/wearables/factories/armor'
import WeaponFactory from '@/model/items/equipables/wearables/factories/weapon'
import stats from '@/model/entities/attributes/stats'

class HeroFactory extends NPCFactory {
  constructor(registry) {
    super(registry)
    this.slots = ['helmet', 'armor', 'gloves', 'boots']
    this.armor = ''
    this.magic = false
  }
  // override
  get abilities() {
    return {}
  }
  // override
  get stats() {
    return {}
  }
  get training() {
    return {
      armor: this.slots.reduce((sum, slot) => {
        sum[slot] = [this.armor]
        return sum
      }, {})
    }
  }
  register() {
    this.spawns.forEach(spawn => {
      this.registry[spawn] = args =>
        this.generate.bind(this)({ ...args, profession: spawn })
    })
  }
  generate(args) {
    const tier = 0.5
    const ages = [20, 30]
    const idx = this.spawn({ ...args, ages })
    const npc = this.registry[idx]
    npc.attributes.magic = this.magic
    npc.inventory.training = this.training
    // abilities
    Object.entries(this.abilities).forEach(([k, v]) => {
      npc.attributes.abilities[k].push(...v)
    })
    // stats
    Object.entries(this.stats).forEach(([a, v]) => {
      const value = window.dice.uniform(...v)
      if (stats[a].debt) {
        Object.entries(stats[a].debt).forEach(([k, w]) => {
          npc.attributes.stats[k] -= value * w
        })
      }
      if (stats[a].credit) {
        Object.entries(stats[a].credit).forEach(([k, w]) => {
          npc.attributes.stats[k] += value * w
        })
      } else {
        npc.attributes.stats[a] += value
      }
    })
    const { armor, weapons } = npc.inventory.training
    // generate armor & weapons
    Object.entries(armor).forEach(([slot, types]) => {
      const type = window.dice.choice(types)
      npc.add(ArmorFactory[type][slot](tier))
    })
    Object.values(weapons).forEach(types => {
      const type = window.dice.choice(types)
      npc.add(WeaponFactory[type](tier))
    })
    // equip everything
    npc.inventory.equipBest()
    return idx
  }
}

export default HeroFactory
