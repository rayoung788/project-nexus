import HeroFactory from '@/model/entities/factories/classes/hero'

import { skills, afflictions } from '@/model/entities/attributes/abilities'

class Rogue extends HeroFactory {
  constructor(registry) {
    super(registry)
    this.armor = 'leather'
  }
  get training() {
    return {
      ...super.training(),
      weapons: {
        mainhand: ['dagger'],
        offhand: ['dagger']
      }
    }
  }
}

class Agent extends Rogue {
  get stats() {
    return {
      envy: [0.1, 0.4],
      pride: [0.1, 0.4],
      decay: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: [
        ...window.dice.sample(skills.rogue, 1),
        ...window.dice.sample(afflictions.poison, 1),
        '_weapon_'
      ]
    }
  }
  get spawns() {
    return ['Agent']
  }
}

export default [Agent]
