import HeroFactory from '@/model/entities/factories/classes/hero'

import abilities from '@/model/entities/attributes/abilities'

const { spells, evade, stuns, afflictions, summons, healing } = abilities

class Mage extends HeroFactory {
  constructor(registry) {
    super(registry)
    this.armor = 'cloth'
    this.magic = true
  }
  get training() {
    return {
      ...super.training,
      weapons: window.dice.choice([
        {
          mainhand: ['staff']
        },
        {
          mainhand: ['implement'],
          offhand: ['magic']
        }
      ])
    }
  }
}

class Pyromancer extends Mage {
  get stats() {
    return {
      [window.dice.choice(['attrition', 'evasion'])]: [0.1, 0.4],
      glass: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: window.dice.sample(spells.fire, 3),
      evasion: window.dice.sample(evade.fire, 1)
    }
  }
  get spawns() {
    return ['Pyromancer']
  }
}

class Frostbinder extends Mage {
  get stats() {
    return {
      [window.dice.choice(['stun', 'evasion'])]: [0.1, 0.4],
      glass: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: window.dice.sample(spells.ice, 3),
      evasion: window.dice.sample(evade.ice, 1),
      stun: window.dice.sample(stuns.ice, 1)
    }
  }
  get spawns() {
    return ['Frostbinder']
  }
}

class Arcanist extends Mage {
  get stats() {
    return {
      [window.dice.choice(['stun', 'evasion'])]: [0.1, 0.4],
      glass: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: window.dice.sample(spells.arcane, 3),
      evasion: window.dice.sample(evade.arcane, 1),
      stun: window.dice.sample(stuns.arcane, 1)
    }
  }
  get spawns() {
    return ['Arcanist', 'Spellbinder']
  }
}

class Warlock extends Mage {
  get stats() {
    return {
      [window.dice.choice(['stun', 'attrition', 'fiend', 'madness'])]: [0.1, 0.4],
      havoc: [0.2, 0.4],
      pact: [0.4, 0.6]
    }
  }
  get abilities() {
    return {
      offense: [
        ...window.dice.sample(spells.fire, 1),
        ...window.dice.sample(afflictions.curses, 1),
        ...window.dice.sample(summons.demons, 1)
      ],
      evasion: window.dice.sample(evade.arcane, 1),
      stun: window.dice.sample(stuns.mind, 1)
    }
  }
  get spawns() {
    return ['Warlock', 'Occultist']
  }
}

class Necromancer extends Mage {
  get stats() {
    return {
      [window.dice.choice(['stun', 'attrition', 'fiend', 'madness'])]: [0.1, 0.4],
      envy: [0.2, 0.4],
      vessel: [0.4, 0.6]
    }
  }
  get abilities() {
    return {
      offense: [
        ...window.dice.sample(spells.ice, 1),
        ...window.dice.sample(afflictions.diseases, 1),
        ...window.dice.sample(summons.undead, 1)
      ],
      evasion: window.dice.sample(evade.arcane, 1),
      stun: window.dice.sample(stuns.mind, 1)
    }
  }
  get spawns() {
    return ['Necromancer']
  }
}

class Illusionist extends Mage {
  get stats() {
    return {
      [window.dice.choice(['stun', 'evasion'])]: [0.2, 0.4],
      madness: [0.3, 0.5],
      glass: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: [...window.dice.sample(spells.mind, 2), ...window.dice.sample(afflictions.curses, 1)],
      evasion: window.dice.sample(evade.arcane, 1),
      stun: window.dice.sample(stuns.mind, 1)
    }
  }
  get spawns() {
    return ['Illusionist', 'Enchanter']
  }
}

class Sorcerer extends Mage {
  get stats() {
    return {
      [window.dice.choice(['stun', 'evasion'])]: [0.2, 0.4],
      siphon: [0.4, 0.6],
      devour: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: [...window.dice.sample(spells.arcane, 1), ...window.dice.sample(spells.blood, 2)],
      evasion: window.dice.sample(evade.arcane, 1),
      stun: window.dice.sample(stuns.mind, 1)
    }
  }
  get spawns() {
    return ['Sorcerer', 'Soulbinder']
  }
}

class Cleric extends Mage {
  get stats() {
    return {
      wisdom: [0.4, 0.6],
      [window.dice.choice(['lore', 'rebirth'])]: [0.1, 0.3]
    }
  }
  get abilities() {
    return {
      offense: window.dice.sample(spells.light, 2),
      healing: window.dice.sample(healing.light, 2)
    }
  }
  get training() {
    return {
      ...super.training(),
      weapons: window.dice.choice([
        {
          mainhand: ['staff']
        },
        {
          mainhand: ['mace'],
          offhand: ['divine']
        }
      ])
    }
  }
  get spawns() {
    return ['Cleric']
  }
}

export default [
  Pyromancer,
  Frostbinder,
  Arcanist,
  Warlock,
  Necromancer,
  Illusionist,
  Sorcerer,
  Cleric
]
