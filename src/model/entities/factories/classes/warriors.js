import HeroFactory from '@/model/entities/factories/classes/hero'

import { skills } from '@/model/entities/attributes/abilities'

class Warrior extends HeroFactory {
  constructor(registry) {
    super(registry)
    this.armor = 'plate'
  }
  get training() {
    return {
      ...super.training(),
      weapons: {
        mainhand: ['sword'],
        offhand: ['shield']
      }
    }
  }
}

class Knight extends Warrior {
  get stats() {
    return {
      pride: [0.1, 0.4],
      guardian: [0.1, 0.4]
    }
  }
  get abilities() {
    return {
      offense: [...window.dice.sample(skills.knight, 1), ...window.dice.sample(skills.tank, 1), '_weapon_']
    }
  }
  get spawns() {
    return ['Knight', 'Vanguard', 'Sentinel']
  }
}

export default [Knight]
