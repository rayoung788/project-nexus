import NPC from '../npc'

import Ancestry from '@/model/entities/attributes/ancestry'

class NPCFactory {
  constructor(registry) {
    this.registry = registry
    // ancestry mapping (surrname -> ancestry)
    this.ancestry = registry.ancestry
    // gender opposites
    this.opposite = {
      male: 'female',
      female: 'male'
    }
    this.register()
  }
  // proxy functions on the top level factory
  register() {
    this.spawns.forEach(spawn => {
      this.registry[spawn] = args => this[spawn].bind(this)(args)
    })
  }
  // the allthing spawner
  spawn({
    cultures,
    culture,
    profession,
    gender = window.dice.choice(['male', 'female']),
    first,
    last,
    location,
    ages = [18, 35],
    ancestry = true,
    status = [1, 1],
    relation
  }) {
    // run relationship logic if applicable
    if (relation) {
      const { relative, type } = relation
      return this[type]({ idx: relative, profession, location })
    }
    // determine culture
    const _culture = culture || location.pickCulture(cultures)
    // get the current unique id
    const idx = this.ancestry._idx
    const [x, y] = ages
    // get a random age from the predefined range
    let age = window.dice.randint(x, y)
    // generate a new (common) last name by default
    const lastName = last || this.status(...status, location, _culture)
    // try to fill unknown ancestors if able
    if (this.ancestry[lastName]) {
      const ca = this.ancestry[lastName].unknownNodes
        .filter(u => u.age >= x && u.age <= y)
        .map(u => u.age)
      ca.length > 0 && (age = window.dice.choice(ca))
    }
    // create the new npc
    const npc = new NPC({
      idx: idx,
      culture: _culture,
      gender: gender,
      profession: profession,
      age: age,
      name: {
        first: first || _culture.genName('first', gender),
        last: lastName
      },
      location: location.idx
    })
    // add them to the general mapping
    this.registry[idx] = npc
    // create a new ancestry object if one doesnt already exist for this surrname
    !this.ancestry[npc.last] &&
      (this.ancestry[npc.last] = new Ancestry(npc.last, _culture))
    // add the new npc to the ancestry tree unless stated otherwise
    ancestry && this.ancestry[npc.last].addEntity(npc)
    // culture based naming conventions that require ancestry info
    ancestry && _culture.language.culturize(npc)
    // increment unqiue id for next npc
    this.ancestry._idx += 1
    return idx
  }

  status(great, noble, location, culture) {
    // selects a random social class given noble & great house boundaries
    const status = window.dice.random
    if (status > great) {
      return location.city.greatHouse
    } else {
      const type = status > noble ? 'noble' : 'common'
      return culture.genName('last', type)
    }
  }

  // relations
  child({ idx, profession, location }) {
    const npc = this.registry[idx]
    const cidx = this.spawn({
      location: location || { idx: npc.loc },
      culture: npc.race,
      profession: profession || npc.profession,
      last: npc.last,
      ages: [Math.max(0, npc.age - 26), npc.age - 18],
      ancestry: false
    })
    const child = this.registry[cidx]
    if (child.age < 18) {
      child.profession = 'Child'
    }
    // assign parent to child
    child.parent = npc
    child.race.language.culturize(child)
    return cidx
  }
  spouse({ idx, profession, location }) {
    const npc = this.registry[idx]
    const sidx = this.spawn({
      location: location || { idx: npc.loc },
      culture: npc.race,
      gender: this.opposite[npc.gender],
      profession: profession || npc.profession,
      last: npc.last,
      ages: [npc.age - 2, npc.age + 2],
      ancestry: false
    })
    // TODO: actually give spouses parents
    npc.spouse = this.registry[sidx]
    return sidx
  }
  parent({ idx, location, profession }) {
    const npc = this.registry[idx]
    const old = npc.parent
    if (old.idx.includes('Unknown Ancestor') && old.age <= 60) {
      return this.fill(old, location, profession)
    }
    return false
  }
  fill(old, location, profession) {
    const pidx = this.spawn({
      location: location,
      culture: old.race,
      profession: profession,
      last: old.last,
      first: old.first,
      gender: old.gender,
      ancestry: false
    })
    const entity = this.registry[pidx]
    this.ancestry[entity.last].fill(old, entity)
    return pidx
  }
}

export default NPCFactory
