class Unit {
  constructor(...npcs) {
    this.npcs = npcs
  }

  get health() {
    return this.npcs.reduce((total, npc) => {
        return total + npc.health
    }, 0)
  }
}

export default Unit
