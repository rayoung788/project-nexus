import Empire from './nations/empire'
import Dynasty from './nations/dynasty'
import Kingdom from './nations/nation'
import Tribal from './nations/tribal'
import Republic from './nations/republic'
import Horde from './nations/horde'
import Colonial from './nations/colonial'

const Nations = {
  Empire,
  Dynasty,
  Republic,
  Horde,
  Kingdom,
  Tribal,
  Colonial
}

export default (type, culture) => {
  return new Nations[type](culture).apply
}
