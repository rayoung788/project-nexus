
import Leadership from './factories/civilians/leadership'
import Military from './factories/civilians/military'
import Guilds from './factories/civilians/guilds'
import Merchants from './factories/civilians/merchants'
import Mages from './factories/classes/mages'

class NPCRegistry {
  constructor() {
    // ancestry mapping (surrname -> ancestry)
    this.ancestry = { _idx: 0 }
    // proxy factories
    const factories = [Leadership, Military, Guilds, Merchants, ...Mages]
    factories.forEach(Factory => new Factory(this))
  }
  matchmaker(npcs, cultures) {
    // get all npc details
    const details = npcs.map(npc => this[npc])
    let matched = 0
    // iterate through each culture
    cultures.forEach(culture => {
      // get all npcs of the culture
      const group = details.filter(npc => npc.race === culture)
      // pick 30% of the group
      const percent = ~~(0.3 * group.length)
      // match oldest first
      group
        .slice(0, percent)
        .sort((a, b) => b.age - a.age)
        .forEach(npc => {
          if (!npc.spouse) {
            // valid spouses must not be more than 3 years apart
            // valid spouses aren't already matched
            // valid spouses cannot have same gender/surrname (will add non-traditional matches later based on culture)
            const prospects = group.filter(
              other =>
                other.last !== npc.last &&
                !other.spouse &&
                other.gender !== npc.gender &&
                other.age <= npc.age + 3 &&
                other.age >= npc.age - 3
            )
            if (prospects.length > 0) {
              npc.spouse = window.dice.choice(prospects)
              matched += 1
            }
          }
        })
    })
    console.log(`Matched: ${matched}`)
  }
}

export default NPCRegistry
