
import Inventory from '@/model/entities/attributes/Inventory'
import Attributes from './attributes/attributes'

// const ranks = ['Novice', 'Apprentice', 'Artisan', 'Veteran', 'Master']

class NPC {
  constructor(kwargs) {
    if (kwargs) {
      const { culture, gender, profession, name, age, idx, location, tier } = kwargs
      // npc location (used to index world.locations)
      this.location = location
      // npc id (used to index world.npcgen)
      this.idx = idx
      this.gender = gender
      this.profession = profession || 'NPC'
      this.race = culture
      this.first = name.first
      this.last = name.last
      // attributes
      this.tier = tier || -1
      this.speed = 6
      this.fatigue = 100
      this.gold = 1000
      this.carryWeight = 150
      this.inventory = new Inventory(this)
      this.attributes = new Attributes()
      // weapon / armor / spell proficiencies
      this.training = {}
      // interactions
      this.merchant = false
      // persona
      this.persona = {
        empathy: window.dice.uniform(0.1, 0.9),
        objectivity: window.dice.uniform(0.1, 0.9),
        ambition: window.dice.uniform(0.1, 0.9),
        change: window.dice.uniform(0.1, 0.9),
        charisma: window.dice.uniform(0.1, 0.9)
      }
      // relations
      this.age = age
      this._parent = false
      this._spouse = false
      this.children = []
      // finalize
      this.race.culturize(this)
    }
  }

  get name() {
    return `${this.first} ${this.last}`
  }

  // ***  Start Relations Behavior *** //
  get parent() {
    return this._parent
  }

  set parent(node) {
    this._parent = node
    if (this._parent) {
      this._parent.addChild(this)
    }
  }

  get spouse() {
    return this._spouse
  }

  set spouse(node) {
    this._spouse = node
    this._spouse._spouse = this
    // combine node and spouses children
    this.children = this.children.concat(this._spouse.children)
    this._spouse.children = this.children
    // // inherit male last names
    // if (this.gender === 'male') {
    //   this._spouse.last = this.last
    // } else {
    //   this.last = this._spouse.last
    // }
    // // propagate last name changes to children
    // // TODO: recursive propagation
    // this._spouse.children.forEach(child => {
    //   child.last = this.last
    // })
  }

  get parents() {
    return [this.parent, this.parent.spouse].filter(p => p)
  }

  get siblings() {
    return this.parent.children.filter(s => s.idx !== this.idx)
  }

  addChild(node) {
    // spouses point to the same list
    this.children.push(node)
  }
  // ***  End Relations Behavior *** //

  // ***  Start Inventory Behavior *** //
  get stash() {
    return this.inventory.stash
  }
  get equipment() {
    return this.inventory.equipment
  }
  add(item) {
    if (this.inventory.carry(item)) {
      const result = { ...this.stash[item.type] }
      result[item.identifier] = item
      this.stash[item.type] = result
    }
  }
  drop(item) {
    const result = { ...this.stash[item.type] }
    delete result[item.identifier]
    this.stash[item.type] = result
    return item
  }
  // ***  End Inventory Behavior *** //
}

export default NPC
