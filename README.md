# Nexus

> A Vue.js ES6 World Generation project

Demo here: https://nexus-maps.herokuapp.com/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Inspirations
1. http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/
2. https://azgaar.wordpress.com/
3. https://mewo2.com/notes/naming-language/
4. https://heredragonsabound.blogspot.com/


