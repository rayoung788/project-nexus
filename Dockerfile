FROM node:8.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
RUN npm install http-server -g
COPY ./dist .
CMD http-server -p $PORT